package org.ithirahad.resourcesresourced.gui;

import api.listener.fastevents.GameMapDrawListener;
import com.bulletphysics.linearmath.Transform;
import org.apache.commons.lang3.text.WordUtils;
import org.ithirahad.resourcesresourced.RRUtils.SerializableVector3i;
import org.ithirahad.resourcesresourced.RRUtils.SpaceGridMap;
import org.ithirahad.resourcesresourced.universe.gasplanet.GasGiantSheet;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemSheet;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.gamemap.entry.SelectableMapEntry;
import org.schema.game.client.view.effects.ConstantIndication;
import org.schema.game.client.view.gamemap.GameMapDrawer;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.PositionableSubColorSprite;
import org.schema.schine.graphicsengine.forms.SelectableSprite;
import org.schema.schine.graphicsengine.forms.Sprite;

import javax.vecmath.Color4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.ArrayList;
import java.util.List;

import static org.ithirahad.resourcesresourced.RRSConfiguration.MAP_ZONE_COLOUR_ALPHA;
import static org.ithirahad.resourcesresourced.RRSConfiguration.USE_MAP_FOW;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.*;

public class OldMapDrawer implements GameMapDrawListener {
    private static final Vector3f nudge = new Vector3f(-64,-64,-64);
    private static final Vector3f miniNudge = new Vector3f(0.5f,0.5f,0.5f);
    private static final Vector4f greyColour = new Vector4f(0.6f,0.6f,0.6f, MAP_ZONE_COLOUR_ALPHA);
    private static final List<MapLine> MapLines = new ArrayList<>();
    private static final List<ConstantIndication> zoneLabels = new ArrayList<>();
    boolean selectedSomething;
    private static String labelText = "";
    private final Transform labelPosition = new Transform();
    private ConstantIndication label = new ConstantIndication(labelPosition, labelText);

    public OldMapDrawer(){
        label.setColor(new Vector4f(1,1,1,1));
        labelPosition.setIdentity();
    }

    @Override
    public void system_PreDraw(GameMapDrawer gameMapDrawer, Vector3i vector3i, boolean explored) {
        if(MapLines.size() == 0)
            generateLinesAndLabels(explored && USE_MAP_FOW);
    }

    ArrayList<Vector3i> exploredSystemsToDrawGiantsIn = new ArrayList<>();

    @Override
    public void system_PostDraw(GameMapDrawer drawer, Vector3i vector3i, boolean explored) {
        if(explored && !exploredSystemsToDrawGiantsIn.contains(vector3i)) {
            exploredSystemsToDrawGiantsIn.add(vector3i);
        }
    }

    @Override
    public void galaxy_PreDraw(GameMapDrawer gameMapDrawer) {

    }

    @Override
    public void galaxy_PostDraw(GameMapDrawer gameMapDrawer) {
    }

    @Override
    public void galaxy_DrawLines(GameMapDrawer gameMapDrawer) {
        for(MapLine m : MapLines)
            DrawUtils.drawFTLLine(m.start, m.end, m.colour, m.endColour);
    }

    @Override
    public void galaxy_DrawSprites(GameMapDrawer drawer) {
        for (Vector3i system : exploredSystemsToDrawGiantsIn) {
            SystemSheet sheet = getSystemSheet(GameClientState.instance.getCurrentGalaxy().galaxyPos, system);
            if(sheet==null) return; //TODO: default sheet to avoid this? Or just generate systemsheets for all the voids?
            SpaceGridMap<GasGiantSheet> giants = sheet.getGiants(new SerializableVector3i(system));

            Sprite backSprite = gasGiantMapBackLayerSprite;
            backSprite.setPositionCenter(true);
            Sprite frontSprite = gasGiantMapFrontLayerSprite;
            frontSprite.setPositionCenter(true);

            for(final Vector3i giant : giants.keySet()) {
                final Color4f color = new Color4f(giants.get(giant).brightColor);
                color.interpolate(giants.get(giant).darkColor, 0.5f); //some bright colors are white or nearly so

                DrawUtils.drawSprite(drawer, backSprite, new PositionableSubColorSprite[]{
                        new PositionableSubColorSprite() {
                            @Override
                            public Vector4f getColor() {
                                //RGBA
                                return new Vector4f(1,1,1,1);
                            }

                            @Override
                            public float getScale(long l) {
                                return 0.05F;
                            }

                            @Override
                            public int getSubSprite(Sprite sprite) {
                                return 0; //only one on the sheet anyway
                            }

                            @Override
                            public boolean canDraw() {
                                return true;
                            }

                            @Override
                            public Vector3f getPos() {
                                return posFromSector(giant,true);
                            }
                        }
                });

                DrawUtils.drawSprite(drawer, frontSprite, new PositionableSubColorSprite[]{
                        new mouseableGasBall(posFromSector(giant,true), color, giants.get(giant).type.typeName())
                });
                if(selectedSomething) drawLabelText(giant);
            }
            /*
            Sprite sprite = gasGiantMapSprite;
            sprite.setPositionCenter(true);
            for(final Vector3i giant : giants.keySet()) {
                final Color4f color = giants.get(giant).brightColor;
                DrawUtils.drawSprite(drawer, sprite, new PositionableSubColorSprite[]{
                        new PositionableSubColorSprite() {
                            @Override
                            public Vector4f getColor() {
                                //RGBA
                                return new Vector4f(color.x, color.y, color.z, 1);
                                //Assign to giant's lighter colour? probably need a more generic sprite
                                //iron: pure white or grayscale will do, will recolor perfectly
                            }

                            @Override
                            public float getScale(long l) {
                                return 0.05F;
                            }

                            @Override
                            public int getSubSprite(Sprite sprite) {
                                return 0; //only one on the sheet anyway
                            }

                            @Override
                            public boolean canDraw() {
                                return true;
                            }

                            @Override
                            public Vector3f getPos() {
                                return posFromSector(giant,true);
                            }
                        }
                });
            }
             */
        }
        exploredSystemsToDrawGiantsIn.clear();
    }

    @Override
    public void galaxy_DrawQuads(GameMapDrawer drawer) {
        /*
        //int i=0;
        for(ResourceZone zone : zoneMaps.get(GameClientState.instance.getCurrentGalaxy().galaxyPos).values()){
            Vector3i pos = new Vector3i();
            if(zone.zoneClass == ZoneClass.NONE) continue;
            else for(Vector3i systemPos : zone.systemDictionary.keySet()){
                //draw sides of cube
                pos.set(systemPos);
                Vector4f color = new Vector4f(zone.zoneClass.ZoneMapColor());
                color.w = 0.2f;
                DrawUtils.tintSystem(systemPos,color);
                //i++;
            }
        }
         */
    }

    private static class MapLine {
        public final Vector3f start;
        public final Vector3f end;
        public final Vector4f colour;
        public final Vector4f endColour;
        public MapLine(Vector3f start, Vector3f end, Vector4f colour, Vector4f endColour) {
            this.start = new Vector3f(start);
            this.end = new Vector3f(end);
            this.colour = new Vector4f(colour);
            this.endColour = new Vector4f(endColour);
        }
    }

    @Deprecated
    private void generateLinesAndLabels(boolean useFOW){ //TODO: FoW integration
        return;
    }

    //util stuff
    private static final float sectorScale = 100f/ VoidSystem.SYSTEM_SIZE;
    private static final Vector3f halfSectorOffset = new Vector3f(sectorScale/2f,sectorScale/2f,sectorScale/2f);
    /**
     * translate a sector center to map-3d-pos
     * @param sector vector3i sector
     * @param isSprite false for ftl lines (centered differently)
     * @return 3d map pos to draw stuff at
     */
    private static Vector3f posFromSector(Vector3i sector, boolean isSprite) {

        Vector3f out = sector.toVector3f();
        if (isSprite) {
            out.add(new Vector3f(-VoidSystem.SYSTEM_SIZE_HALF,-VoidSystem.SYSTEM_SIZE_HALF,-VoidSystem.SYSTEM_SIZE_HALF));
        }
        out.scale(sectorScale);
        out.add(halfSectorOffset);
        return out;
    }

    private void drawLabelText(Vector3i sector){
        drawText(posFromSector(sector,true), labelText);
    }

    public void drawText(Vector3f mapPos, String text){
        labelPosition.origin.set(mapPos);
        labelText = WordUtils.capitalize(text);
        label.setText(Lng.str(labelText));
        //label = new ConstantIndication(labelPosition, Lng.str(labelText));
        HudIndicatorOverlay.toDrawMapTexts.add(label);
    }

    private class mouseableGasBall implements PositionableSubColorSprite, SelectableSprite, SelectableMapEntry {
        String label;
        Color4f color;
        Vector3f position;

        mouseableGasBall(Vector3f pos, Color4f color, String label){
            this.label = label;
            this.color = color;
            this.position = pos;
        }

        @Override
        public Vector4f getColor() {
            //RGBA
            return new Vector4f(color.x, color.y, color.z, 1);
            //Assign to giant's lighter colour? probably need a more generic sprite
            //iron: pure white or grayscale will do, will recolor perfectly
        }

        @Override
        public float getScale(long l) {
            return 0.05F;
        }

        @Override
        public int getSubSprite(Sprite sprite) {
            return 0; //only one on the sheet anyway
        }

        @Override
        public boolean canDraw() {
            return true;
        }

        @Override
        public Vector3f getPos() {
            return position;
        }

        private boolean drawIndication;
        @Override
        public boolean isDrawIndication() {
            return drawIndication;
        }

        @Override
        public void setDrawIndication(boolean val) {
            drawIndication = val;
        }

        @Override
        public float getSelectionDepth() {
            return 0;
        }

        @Override
        public boolean isSelectable() {
            return true;
        }

        @Override
        public void onSelect(float v) {
            selectedSomething = true;
            OldMapDrawer.labelText = "Gas Giant Planet (Type: " + this.label + ")";
        }

        @Override
        public void onUnSelect() {
            selectedSomething = false;
            OldMapDrawer.labelText = "";
        }
    }
}
