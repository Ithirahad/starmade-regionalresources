package org.ithirahad.resourcesresourced.gui;

import api.DebugFile;
import api.listener.Listener;
import api.listener.events.input.MousePressEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.StarRunnable;
import libpackage.drawer.MapDrawer;
import libpackage.drawer.SpriteLoader;
import libpackage.markers.ClickableMapMarker;
import libpackage.markers.SimpleMapMarker;
import libpackage.text.MapText;
import org.apache.commons.lang3.text.WordUtils;
import org.ithirahad.resourcesresourced.RRUtils.SpaceGridMap;
import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.ithirahad.resourcesresourced.universe.galaxy.ResourceZone;
import org.ithirahad.resourcesresourced.universe.galaxy.ZoneClass;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gamemap.GameMapDrawer;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.*;

import static org.ithirahad.resourcesresourced.ResourcesReSourced.isClientWorldAccessReady;

/**
 * STARMADE MOD
 * CREATOR: Max1M
 * DATE: 20.10.2021
 * TIME: 17:34
 * drawer class specifically to draw stuff for marking out resource zones. handles its own sprite loading and the marker generation.
 * instantiate, loadsprites, generate zones. drawer does the rest.
 */
public class ZoneMapDrawer extends MapDrawer {
    private final ArrayList<SimpleMapMarker> starZoneMarkers = new ArrayList<>(); //stores the zone marker for each star
    private final Map<Vector3i, ClickableMapMarker> childToParentMarkerMap = new HashMap<>(); //TODO: per galaxy (or just make these zonemapdrawers per galaxy and swap them idk)
    private Sprite zoneSprite;
    private ClickableMapMarker hoverNow;
    private ClickableMapMarker selectedZone;
    private static GameMapDrawer currentDrawer;
    private static Vector3i currentLoc;
    public ZoneMapDrawer(StarMod mod) {
        super(mod);
        DebugFile.log("CREATED ZONE MAP DRAWER");

        new StarRunnable(){
            @Override
            public void run() {
                if (!isClientWorldAccessReady()) {
                    return;
                }
                if(generateZoneMarkers()) cancel(); //cancel upon success
            }
        }.runTimer(mod,100);

        StarLoader.registerListener(MousePressEvent.class, new Listener<MousePressEvent>() {
            @Override
            public void onEvent(MousePressEvent event) {
                if (event.getRawEvent().pressedLeftMouse()) {
                    if (selectedZone != null && selectedZone.equals(hoverNow)) {
                        selectedZone = null;
                    } else {
                        selectedZone = hoverNow;
                    }
                }
            }
        },mod);
    }

    public void loadSprite(StarMod mod) {
        SpriteLoader msw = new SpriteLoader("org/ithirahad/resourcesresourced/assets/image/map/","zoneMarker.png",1000,1000,1,1);
        if (msw.loadSprite(mod)) {
            zoneSprite = msw.getSprite();
        }
        DebugFile.log("LOADED ZONE SPRITE",mod);
    }

    /**
     * @return whether or not generation was successful
     */
    public boolean generateZoneMarkers() {
        DebugFile.log("[RRS][MAP] Generating zone markers...");
        assert ResourcesReSourced.zoneMaps != null;
        assert GameClientState.instance != null;
        Vector3i zoneCore;
        List<Vector3i> starsInZone;
        SpaceGridMap<ResourceZone> zones = ResourcesReSourced.zoneMaps.get(GameClientState.instance.getCurrentGalaxy().galaxyPos);
        if(zones == null) return false;

        Collection<ResourceZone> zoneMap = zones.values();
         for (final ResourceZone zone : zoneMap) {
            if (zone.zoneClass.equals(ZoneClass.NONE))
               continue;

            //collect all stars in this zone
            zoneCore = zone.getPrincipalStar();
            starsInZone = new LinkedList<>(zone.getStars());

            //done collecting all stars
            final Vector3i corePosIn = zoneCore;
            final ClickableMapMarker originMarker = new ClickableMapMarker(zoneSprite,
                    0,
                    getColorByType(zone.zoneClass),
                    posFromSystemPos(corePosIn)) {
                float baseScale = getScale();

                @Override
                public void preDraw(GameMapDrawer drawer) {
                    if(currentDrawer != null && corePosIn.equals(currentDrawer.getGameMapPosition().getCurrentSysPos()))
                        setScale(baseScale * 2);
                    else if(this.equals(hoverNow))
                        setScale(baseScale * 1.2f);
                    else if(zone.isMinor()){
                        setScale(baseScale/2);
                    }
                    else setScale(baseScale);
                    super.preDraw(drawer);
                }

                @Override
                public void onSelect(float v) {
                    super.onSelect(v);
                    hoverNow = this;
                }

                @Override
                public void onUnSelect() {
                    super.onUnSelect();
                    if (this.equals(hoverNow))
                        hoverNow = null;
                }
            };
            addMarker(originMarker);

            //generate help text for focused zone
            String focusedText = zone.name.toUpperCase() + "\r\n(" + (zone.isMinor() ? "Minor " : "") + WordUtils.capitalize(zone.zoneClass.getDescriptor()) + " Region)";
            MapText selectedZoneText = new MapText(GameClientState.instance,
                    this,
                    zone.name.length() * 25,
                    50,
                    posFromSystemPos(zoneCore),
                    focusedText,
                    new Vector3f(zone.name.length() * 7.5f,-100,0), //Blender Pro Heavy is not monospace so this will never be perfect
                    FontLibrary.getBlenderProHeavy20(),
                    false){
                final ClickableMapMarker parent = originMarker;
                @Override
                public void draw() {
                    boolean isViewPosition = false;
                    if(currentDrawer != null) isViewPosition = parent.equals(getOriginMarker(currentDrawer.getGameMapPosition().getCurrentSysPos()));
                    if (parent.equals(selectedZone) || isViewPosition ||  (hoverNow != null && hoverNow.equals(parent))) super.draw();
                }
            };

            /*
            String unfocusedText = zone.name.toUpperCase();
            MapText otherZoneText = new MapText(GameClientState.instance,
                    this,
                    unfocusedText.length() * 16,
                    50,
                    posFromSystemPos(zoneCore),
                    unfocusedText,
                    new Vector3f(0,-50,0),
                    FontLibrary.getBlenderProHeavy14(),
                    false){
                final ClickableMapMarker parent = originMarker;

                @Override
                public void draw() {
                    if (parent.equals(selectedZone))
                        return; //focused text would already be drawn; don't bother.
                    super.draw();
                }
            };
             */

            selectedZoneText.setBoxColor(new Vector4f(0,0,0,0));
            //otherZoneText.setBoxColor(new Vector4f(0,0,0,0));
            starsInZone.remove(zone.getPrincipalStar()); //already drawing that
            generateZone(starsInZone, zone.getVoids(), zone.zoneClass, originMarker);
            childToParentMarkerMap.put(zone.getPrincipalStar(),originMarker);
        }
        return true;
    }

    private Vector4f getColorByType(ZoneClass type) {
        return type.ZoneMapColor();
    }

    /**
     * generate markers for all stars in this zone. get slaved to the origin marker automatically
     * @param zoneClass
     */
    private void generateZone(Collection<Vector3i> systems, Collection<Vector3i> assocVoids, ZoneClass zoneClass, final ClickableMapMarker master) {
        //DebugFile.log("generating zone type " + zoneClass.name());

        for (final Vector3i star : systems) {
            SimpleMapMarker starMarker = new ClickableMapMarker(
                    zoneSprite,
                    0,
                    getColorByType(zoneClass),
                    posFromSystemPos(star)
            ){
                Vector3i galacticPos = new Vector3i(star);
                float baseScale = getScale();

                @Override
                public void preDraw(GameMapDrawer drawer) {
                    if(currentDrawer != null && galacticPos.equals(currentDrawer.getGameMapPosition().getCurrentSysPos())) setScale(baseScale * 2);
                    else if(hoverNow != null && hoverNow.equals(this)) setScale(baseScale * 1.2f);
                    else setScale(baseScale);
                    super.preDraw(drawer);
                }

                @Override
                public boolean canDraw() {
                    boolean isViewPosition = false;
                    if(currentDrawer != null) isViewPosition = master.equals(getOriginMarker(currentDrawer.getGameMapPosition().getCurrentSysPos()));
                    return (master.equals(selectedZone) || isViewPosition || (hoverNow != null && hoverNow.equals(master)));
                }
            };
            childToParentMarkerMap.put(star,master);
            addMarker(starMarker);
        }
        for(Vector3i v : assocVoids){
            childToParentMarkerMap.put(v,master);
        }
    }

    private static Vector3f posFromSystemPos(Vector3i system) {
        Vector3i in = new Vector3i(system);
        in.scale(VoidSystem.SYSTEM_SIZE);
        in.add(VoidSystem.SYSTEM_SIZE_HALF,VoidSystem.SYSTEM_SIZE_HALF,VoidSystem.SYSTEM_SIZE_HALF);
        return posFromSector(in,true);
    }

    protected ClickableMapMarker getOriginMarker(Vector3i sub){
        return childToParentMarkerMap.get(sub);
    }

    @Override
    public void galaxy_DrawSprites(GameMapDrawer gameMapDrawer) {
        super.galaxy_DrawSprites(gameMapDrawer);
        currentDrawer = gameMapDrawer;
    }
}
