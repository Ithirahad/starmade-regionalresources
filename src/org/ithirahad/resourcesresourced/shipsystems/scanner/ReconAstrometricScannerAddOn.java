package org.ithirahad.resourcesresourced.shipsystems.scanner;

import api.common.GameCommon;
import api.listener.events.systems.ReactorRecalibrateEvent;
import api.mod.StarMod;
import api.network.packets.PacketUtil;
import api.utils.addon.SimpleAddOn;
import api.utils.game.SegmentControllerUtils;
import api.utils.sound.AudioUtils;
import com.bulletphysics.linearmath.Transform;
import org.ithirahad.resourcesresourced.network.SystemScanInfo;
import org.ithirahad.resourcesresourced.vfx.ScannerPulseEffect;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorElement;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GlUtil;

import javax.vecmath.Vector3f;
import java.util.ArrayList;

import static org.ithirahad.resourcesresourced.RRSConfiguration.ASTROMETRIC_SCANNER_CHARGE_POWER_PER_MASS;
import static org.ithirahad.resourcesresourced.RRSConfiguration.ASTROMETRIC_SCANNER_CHARGE_TIME;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.*;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.ASTEROID;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.ASTEROID_MANAGED;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_WARNING;

public class ReconAstrometricScannerAddOn extends SimpleAddOn {
    public static final String UID_NAME = "AstrometricScannerAddOn";
    boolean isPlayerUsable = false;
    boolean givesSectorInformation = false;
    boolean givesRemotePlanetInformation = false;
    boolean givesMapScan = false;

    public ReconAstrometricScannerAddOn(ManagerContainer<?> container, StarMod mod) {
        super(container, elementEntries.get("Astrometric Scanner Chamber").id, mod, UID_NAME);
        //REFERENCE: this.usableId = getPlayerUsableId(elementEntries.get("Astrometric Scanner Chamber").id, mod, UID_NAME);
    }
    //TODO: fix icon

    /* REFERENCE:
    public static long getPlayerUsableId(short var0, StarMod var1, String var2) {
        long var3 = UniversalRegistry.getExistingURV(RegistryType.PLAYER_USABLE_ID, var1, var2);
        PlayerUsableInterface.ICONS.put(var3, var0);
        return var3;
    }
    */

    @Override
    public short getWeaponRowIcon() {
        return ElementKeyMap.REACTOR_CHAMBER_SCANNER;
    }

    @Override
    public boolean isPlayerUsable() {
        updateActivationState();
        return isPlayerUsable;
    }

    @Override
    public boolean executeModule() {
        return super.executeModule();
    }

    @Override
    public PowerConsumerCategory getPowerConsumerCategory() {
        return PowerConsumerCategory.SCANNER;
    }

    @Override
    public float getChargeRateFull() {
        return ASTROMETRIC_SCANNER_CHARGE_TIME;
    }

    @Override
    public double getPowerConsumedPerSecondResting() {
        return 1;
    }

    @Override
    public double getPowerConsumedPerSecondCharging() {
        return ASTROMETRIC_SCANNER_CHARGE_POWER_PER_MASS * getSegmentController().getMassWithDocks();
    }

    @Override
    public float getDuration() {
        return 0.01f; //should be instantaneous...
    }

    @Override
    public boolean onExecuteServer() {
        if (GameServerState.instance != null) {
            updateActivationState();
            if (!isPlayerUsable) return true;

            final SegmentController segmentController = this.segmentController;
            final Vector3i sector = segmentController.getSector(new Vector3i());
            Vector3f particleVelocity = GlUtil.getForwardVector(new Vector3f(), segmentController.getPhysicsObject().getWorldTransform(new Transform()).basis); //bruh, it took me forever to find this
            particleVelocity.scale(50f); //particle speed
            Vector3f inheritedVelocity = segmentController.getPhysicsObject().getLinearVelocity(new Vector3f());
            inheritedVelocity.scale(0.04f);//???????????
            particleVelocity.add(inheritedVelocity);

            ArrayList<PlayerState> rawPlayers = getAttachedPlayers();
            final ArrayList<PlayerState> playersAttached;
            if (GameCommon.isDedicatedServer()) {
                playersAttached = rawPlayers;
            } else {
                playersAttached = getServerSendables(rawPlayers);
            }
            AudioUtils.serverPlaySound("0022_gameplay - level completed", 1.0f, 1.0f, playersAttached); //TODO: custom effect "astro scanner ping" once imports are fixed
            //TODO: "0022_gameplay - notification chime 2" is better but there's a number at the end so the sound system doesn't like it

            ScannerPulseEffect.fireAstroEffectServer(segmentController.getSectorId(), sector, segmentController.getWorldTransform().origin, particleVelocity, (int) Math.max(Math.max(segmentController.getBoundingBox().sizeX(), segmentController.getBoundingBox().sizeY()), segmentController.getBoundingBox().sizeZ()));

            SystemScanInfo response = new SystemScanInfo(
                    VoidSystem.getContainingSystem(sector, new Vector3i()),
                    sector,
                    givesSectorInformation,
                    givesRemotePlanetInformation);
            for (PlayerState player : playersAttached) {
                if (player != null) {
                    if (givesMapScan) {
                        System.err.println("[MOD][RRS][SERVER][SCAN] " + this + " Scanning " + player.getCurrentSystem());
                        //BACKUP METHOD GameServer.getServerState().scanOnServer(null, player); //still broke playerstates for some reason so... guess it was asynch problems?

                        if (player.getFogOfWar().isOnServer()) {
                            Vector3i sys = new Vector3i(player.getCurrentSystem());
                            player.getFogOfWar().scan(sys); //Map scan //TODO: This is breaking playerstates
                            /*
                            @SuppressWarnings("all")
                            Object2BooleanOpenHashMap<Vector3i> cachedFogOfWar = (Object2BooleanOpenHashMap<Vector3i>) readPrivateField("cachedFogOfWar", player.getFogOfWar());
                            cachedFogOfWar.put(sys, true);
                            */
                        } else {
                            System.err.println("[MOD][RRS][CLIENT][WARNING] Attempted to do serverside scan on logical client. WTF?");
                            player.sendClientMessage(Lng.str("[MOD][RRS][CLIENT] Bruh"), MESSAGE_TYPE_WARNING);
                        }

                        //TODO: focal point mechanic + zone map FoW integration
                    }
                    System.err.println("[MOD][RRS][SERVER][SCAN] Sending astrometric scan info for sector " + player.getCurrentSector() + " in system " + player.getCurrentSystem());
                    PacketUtil.sendPacket(player, response);
                } else
                    System.err.println("[MOD][Resources ReSourced][WARNING] Found null player aboard " + segmentController.getName() + "while distributing scan results");
            }
        }
        return true;
    }

    @Override
    public boolean onExecuteClient() {
        return true; //await server response
    }

    @Override
    public void onActive() {

    }

    @Override
    public void onInactive() {

    }

    @Override
    public void onReactorRecalibrate(ReactorRecalibrateEvent event){
        updateActivationState();
    }

    @Override
    public String getName() {
        return "Astrometric Scanner";
    }

    private void updateActivationState(){
        if(segmentController.getType() == ASTEROID_MANAGED || segmentController.getType() == ASTEROID) return; //workaround for StarLoader issue

        ReactorElement baseScannerChamber = SegmentControllerUtils.getChamberFromElement(getManagerUsableSegmentController(), elementEntries.get("Astrometric Scanner Chamber"));
        ReactorElement sectorScannerChamber = SegmentControllerUtils.getChamberFromElement(getManagerUsableSegmentController(), elementEntries.get("Sector Prospecting Scanner Chamber"));
        ReactorElement planetaryScannerChamber = SegmentControllerUtils.getChamberFromElement(getManagerUsableSegmentController(), elementEntries.get("Planetary Scanner Chamber"));
        ReactorElement oldScannerChamber = SegmentControllerUtils.getChamberFromElement(getManagerUsableSegmentController(), elementEntries.get("Mapping Scanner Chamber"));

        isPlayerUsable = (baseScannerChamber != null && baseScannerChamber.isAllValid());
        givesSectorInformation = (sectorScannerChamber != null && sectorScannerChamber.isAllValid());
        givesRemotePlanetInformation = (planetaryScannerChamber != null && planetaryScannerChamber.isAllValid());
        givesMapScan = (oldScannerChamber != null && oldScannerChamber.isAllValid());
    }
}
