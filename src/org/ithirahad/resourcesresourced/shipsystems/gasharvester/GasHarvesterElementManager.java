package org.ithirahad.resourcesresourced.shipsystems.gasharvester;

import api.common.GameCommon;
import api.utils.game.SegmentControllerUtils;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.ithirahad.resourcesresourced.RRUtils.RealTimeDelayedAction;
import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import org.ithirahad.resourcesresourced.industry.ResourceSourceType;
import org.ithirahad.resourcesresourced.vfx.RamscoopEffect;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.structurecontrol.ControllerManagerGUI;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.Ship;
import org.schema.game.common.controller.elements.*;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorElement;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ShootContainer;
import org.schema.game.common.data.player.ControllerStateInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import java.io.IOException;
import java.util.*;

import static api.mod.ModStarter.justStartedServer;
import static org.ithirahad.resourcesresourced.RRSConfiguration.GAS_MINER_CHAMBER_BONUS_1;
import static org.ithirahad.resourcesresourced.RRSConfiguration.GAS_MINER_CHAMBER_BONUS_2;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.container;
import static org.ithirahad.resourcesresourced.industry.PassiveResourceSourcesContainer.getResourceColor;
import static org.schema.game.common.data.element.ElementKeyMap.STASH_ELEMENT;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class GasHarvesterElementManager extends UsableControllableFireingElementManager<GasHarvesterUnit,GasHarvesterCollectionManager,GasHarvesterElementManager> implements BlockActivationListenerInterface, IntegrityBasedInterface {
    public static final float REACTOR_POWER_CONSUMPTION_RESTING = 15f;
    public static final float REACTOR_POWER_CONSUMPTION_CHARGING = 40f;
    public static float RAMSCOOP_CYCLE_TIME_SECONDS = 15.0f;
    public static long HARVEST_SAMPLE_RATE_MS = 100;
    private final ShootContainer shootContainer = new ShootContainer();
    private static final boolean debug = false;
    List<ResourceRoutingQueueEntry> resourceRoutingQueue = new LinkedList<>();

    public GasHarvesterElementManager(SegmentController segmentController) {
        super(elementEntries.get("Gas Harvester Computer").id, elementEntries.get("Gas Harvester Module").id, segmentController);
    }

    public void claimResources(){
        for (Iterator<ResourceRoutingQueueEntry> iterator = resourceRoutingQueue.iterator(); iterator.hasNext();) {
            ResourceRoutingQueueEntry m = iterator.next();
            float chamberBonus;

            ReactorElement bonus1 = SegmentControllerUtils.getChamberFromElement((ManagedUsableSegmentController<?>) getSegmentController(),elementEntries.get("Active Siphon Bonus 1"));
            ReactorElement bonus2 = SegmentControllerUtils.getChamberFromElement((ManagedUsableSegmentController<?>) getSegmentController(),elementEntries.get("Active Siphon Bonus 2"));
            if(bonus2 != null && bonus2.isAllValid()) chamberBonus = GAS_MINER_CHAMBER_BONUS_2;
            else if(bonus1 != null && bonus1.isAllValid()) chamberBonus = GAS_MINER_CHAMBER_BONUS_1;
            else chamberBonus = 1;

            float environmentalBonus = PassiveResourceSupplier.getSituationMiningBonus(getSegmentController().getUniqueIdentifier(),m.source.resourceTypeId,m.source.type,getSegmentController().getFactionId(),getSegmentController().getSector(new Vector3i()));

            int quantity = m.source.claimActiveResources(m.harvestPower,m.destination.getCapacity() - m.destination.getVolume());
            quantity *= environmentalBonus;
            int mod = m.destination.incExistingOrNextFreeSlotWithoutException(m.source.resourceTypeId, (int)Math.floor(quantity * chamberBonus));
            m.destination.sendInventoryModification(mod);
            iterator.remove();
        }
    }

    public void doShot(GasHarvesterUnit c, GasHarvesterCollectionManager m,
                       ShootContainer shootContainer, PlayerState playerState, Timer timer) {
        if (c.canUse(timer.currentTime, false)) {
            Set<PlayerState> players = new HashSet<>(getAttachedPlayers());
            if(playerState != null) players.add(playerState); //idk if in-core firer is considered an attached player

            Inventory inv = null;
            Short2ObjectOpenHashMap<FastCopyLongOpenHashSet> linked = MiscUtils.findLinkedBlocks(getSegmentController(),m.getControllerIndex());

            if(linked.containsKey(STASH_ELEMENT)){
                LongIterator inventoryIndices = linked.get(STASH_ELEMENT).iterator();
                while(inventoryIndices.hasNext() && inv == null) {
                    inv = MiscUtils.getInventoryAt(getSegmentController(), inventoryIndices.next());
                    if(inv.isOverCapacity()) inv = null;
                }
            }
            if(inv == null && playerState != null && !playerState.getInventory().isOverCapacity())
                inv = playerState.getInventory();
            //if no inventories with space are available, inv will remain null and no further code executes here.

            if (inv != null &&  (isUsingPowerReactors() || consumePower(c.getPowerConsumption() ))) {
                Transform t = new Transform();
                t.setIdentity();
                t.origin.set(shootContainer.weapontOutputWorldPos);

                c.setShotReloading((long) (RAMSCOOP_CYCLE_TIME_SECONDS * 1000));

                Vector3f dir = new Vector3f(shootContainer.shootingDirTemp);
                dir.normalize();
                long weaponId = m.getUsableId();

                if(GameClientState.instance != null);
                if(!GameCommon.isClientConnectedToServer()) startHarvestCycle(c, inv, dir, t, weaponId, m.getColor(), players);

                handleResponse(ShootingRespose.FIRED, c, shootContainer.weapontOutputWorldPos);
            } else if (inv == null) {
                for (PlayerState player : players)
                    player.sendServerMessage(Lng.astr("No inventory to deposit harvested gases from ramscoop!"), MESSAGE_TYPE_INFO);
            } else {
                handleResponse(ShootingRespose.NO_POWER, c, shootContainer.weapontOutputWorldPos);
            }
        } else {
            handleResponse(ShootingRespose.RELOADING, c, shootContainer.weapontOutputWorldPos);
        }
    }

    private void startHarvestCycle(final GasHarvesterUnit c, final Inventory targetInv, final Vector3f dir, Transform t, long weaponId, Vector4f color, final Collection<PlayerState> players) {
        final SegmentController seg = getSegmentController();
        Vector3i sector = seg.getSector(new Vector3i());

        Vector3f pos = seg.getWorldTransform().origin;
        pos.add(c.getOutput().toVector3f());


        if(justStartedServer){
            LinkedList<PassiveResourceSupplier> wells = new LinkedList<>();

            PassiveResourceSupplier[] allSources = container.getAllAvailableSourcesInArea(seg);
            //PassiveResourceSupplier[] allSources = ResourcesReSourced.container.getPassiveSources(sector);
            for(PassiveResourceSupplier p : allSources)
                if(p.type == ResourceSourceType.SPACE) wells.add(p);

            if(wells.isEmpty()) for (PlayerState player : players)
                player.sendServerMessage(Lng.astr("No gaseous source in sector!\r\n Fly into a Gas Giant sector to use this module."), MESSAGE_TYPE_INFO); //TODO: or nebula
            else for(final PassiveResourceSupplier well : wells) {
                new RealTimeDelayedAction(HARVEST_SAMPLE_RATE_MS, -1, false){
                    float sampledHarvestPower = 0;
                    int passes = 0;
                    final long endTime = System.currentTimeMillis() + (long) (((double) RAMSCOOP_CYCLE_TIME_SECONDS) * 1000);
                    @Override
                    public void doAction() {
                        sampledHarvestPower += c.getHarvestPower();
                        passes++;

                        if(c.getPowered() <= 0.95f){
                            for (PlayerState player : players)
                                player.sendServerMessage(Lng.astr("Ramscoop is not powered!\r\n Harvested gas has been vented to avoid catastrophic containment failure."), MESSAGE_TYPE_INFO);
                            this.cancel();
                        }
                        else {
                            RamscoopEffect.FireHarvestEffectServer(seg.getSectorId(), seg.getId(), seg.getSector(new Vector3i()),c.getOutput().toVector3f(),dir,seg.getLinearVelocity(new Vector3f()), HARVEST_SAMPLE_RATE_MS, c.getStaticHarvestPower(), getResourceColor(well));
                            if (System.currentTimeMillis() >= endTime) {
                                sampledHarvestPower = sampledHarvestPower / passes; //average
                                resourceRoutingQueue.add(new ResourceRoutingQueueEntry(well, sampledHarvestPower, targetInv));
                                claimResources(); //I know this makes the queue redundant, but I figured I'd use a separate method in case it turns out to be async unsafe
                                this.cancel();
                            }
                        }
                    }
                }.startCountdown();
            }
        }
    }

    @Override
    public int onActivate(SegmentPiece piece, boolean oldActive, boolean active) {
        long absPos = piece.getAbsoluteIndex();
        for (int i = 0; i < getCollectionManagers().size(); i++) {
            for (GasHarvesterUnit d : getCollectionManagers().get(i).getElementCollections()) {
                if (d.contains(absPos)) {
                    d.setMainPiece(piece, active);

                    return active ? 1 : 0;
                }
            }
        }
        return active ? 1 : 0;
    }

    @Override
    public void updateActivationTypes(ShortOpenHashSet typesThatNeedActivation) {
        typesThatNeedActivation.add(elementEntries.get("Gas Harvester Module").id);
    }

    @Override
    protected String getTag() {
        return "mainreactor";
    }

    @Override
    public GasHarvesterCollectionManager getNewCollectionManager(SegmentPiece block, Class<GasHarvesterCollectionManager> clazz) {
        return new GasHarvesterCollectionManager(block, getSegmentController(), this);
    }

    @Override
    public String getManagerName() {
        return "Ramscoop System Collective";
    }

    @Override
    protected void playSound(GasHarvesterUnit gasHarvesterUnit, Transform transform) {

    }

    @Override
    public void handle(ControllerStateInterface unit, Timer timer) {
        //		if(!getSegmentController().isOnServer()){
        //			debug = Keyboard.isKeyDown(GLFW.GLFW_KEY_NUMPAD1);
        //		}
        if (!unit.isFlightControllerActive()) {
            if (debug) {
                System.err.println("FLIGHT CONTROLLER INACTIVE");
            }
            return;
        }
        if (getCollectionManagers().isEmpty()) {
            if (debug) {
                System.err.println("NO HARVESTER GROUPS");
            }
            //nothing to succ with
            return;
        }
        try {
            if (!convertDeligateControls(unit, shootContainer.controlledFromOrig, shootContainer.controlledFrom)) {
                if (debug) {
                    System.err.println("NO SLOT");
                }
                return;
            }
        } catch (IOException e) {
            System.err.println("[MOD][BastionInitiative] ERROR CONVERTING DELEGATE CONTROLS FOR GAS HARVESTER: ");
            e.printStackTrace();
            return;
        }
        long time = System.currentTimeMillis();
        int unpowered = 0;
        getPowerManager().sendNoPowerHitEffectIfNeeded();
        if (debug) {
            System.err.println("GAS HARVESTING CONTROLLERS: " + getState() + ", " + getCollectionManagers().size() + " FROM: " + shootContainer.controlledFrom);
        }
        for (int i = 0; i < getCollectionManagers().size(); i++) {
            GasHarvesterCollectionManager cm = getCollectionManagers().get(i);
            if (unit.isSelected(cm.getControllerElement(), shootContainer.controlledFrom)) {
                boolean controlling = shootContainer.controlledFromOrig.equals(shootContainer.controlledFrom);
                controlling |= getControlElementMap().isControlling(shootContainer.controlledFromOrig, cm.getControllerPos(), controllerId);
                if (debug) {
                    System.err.println("Controlling " + controlling + " " + getState());
                }

                if (controlling) {
                    if(!cm.allowedOnServerLimit()){
                        continue;
                    }
                    if (shootContainer.controlledFromOrig.equals(Ship.core)) {
                        unit.getControlledFrom(shootContainer.controlledFromOrig);
                    }
                    if (debug) {
                        System.err.println("Controlling " + controlling + " " + getState() + ": " + cm.getElementCollections().size());
                    }
                    for (int u = 0; u < cm.getElementCollections().size(); u++) {
                        GasHarvesterUnit c = cm.getElementCollections().get(u);

                        Vector3i v = c.getOutput();

                        shootContainer.weapontOutputWorldPos.set(
                                v.x - SegmentData.SEG_HALF,
                                v.y - SegmentData.SEG_HALF,
                                v.z - SegmentData.SEG_HALF);
                        if (debug) {
                            System.err.println("GAS HARVESTER \"OUTPUT\" ON " + getState() + " -> " + v + "");
                        }
                        if (getSegmentController().isOnServer()) {
                            getSegmentController().getWorldTransform().transform(shootContainer.weapontOutputWorldPos);
                        } else {
                            getSegmentController().getWorldTransformOnClient().transform(shootContainer.weapontOutputWorldPos);
                        }

                        shootContainer.centeralizedControlledFromPos.set(shootContainer.controlledFromOrig);
                        shootContainer.centeralizedControlledFromPos.sub(Ship.core);


                        shootContainer.camPos.set(getSegmentController().getAbsoluteElementWorldPosition(shootContainer.centeralizedControlledFromPos, shootContainer.tmpCampPos));


                        boolean focus = false;
                        boolean lead = false;
                        unit.getShootingDir(getSegmentController(), shootContainer, c.getDistanceFull(), 1, cm.getControllerPos(), focus, lead);

                        shootContainer.shootingDirTemp.normalize();

                        doShot(c, cm, shootContainer, unit.getPlayerState(), timer);

                        //							getParticleController().addProjectile(getSegmentController(), weapontOutputWorldPos, shootingDirTemp, c.getDamage(), c.getDistance());

                    }
                    if (cm.getElementCollections().isEmpty() && clientIsOwnShip()) {
                        ((GameClientState) getState()).getController().popupInfoTextMessage(Lng.str("WARNING!\n \nNo Modules connected \nto entry point"), 0);
                    }
                }
            }
        }
        if (unpowered > 0 && clientIsOwnShip()) {
            ((GameClientState) getState()).getController().popupInfoTextMessage(Lng.str("WARNING!\n \nHarvester Elements unpowered: %s",  unpowered), 0);
        }
        if (getCollectionManagers().isEmpty() && clientIsOwnShip()) {
            ((GameClientState) getState()).getController().popupInfoTextMessage(Lng.str("WARNING!\n \nNo harvester controllers."), 0);
        }

        claimResources();
    }

    @Override
    public ControllerManagerGUI getGUIUnitValues(GasHarvesterUnit firingUnit, GasHarvesterCollectionManager var2, ControlBlockElementCollectionManager<?, ?, ?> var3, ControlBlockElementCollectionManager<?, ?, ?> var4){
        return ControllerManagerGUI.create((GameClientState) getState(), Lng.str("Ramscoop Unit"), firingUnit,
                new ModuleValueEntry(Lng.str("Range (Planets)"), "2 Sectors"),
                new ModuleValueEntry(Lng.str("Range (Stars)"), "1 System"), //TODO ...and nebulae
                //TODO: Nerf range later
                new ModuleValueEntry(Lng.str("PowerConsumptionResting"), firingUnit.getPowerConsumedPerSecondResting()),
                new ModuleValueEntry(Lng.str("PowerConsumptionCharging"), firingUnit.getPowerConsumedPerSecondCharging())
        );
    }

    private class ResourceRoutingQueueEntry {
        final PassiveResourceSupplier source;
        final float harvestPower;
        final Inventory destination;

        private ResourceRoutingQueueEntry(PassiveResourceSupplier source, float harvestPower, Inventory destination) {
            this.source = source;
            this.harvestPower = harvestPower;
            this.destination = destination;
        }
    }
}
