package org.ithirahad.resourcesresourced.industry;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import static org.ithirahad.resourcesresourced.RRSConfiguration.DEBUG_LOGGING;

public class Extractor implements Serializable {
    String UID;
    float strength;
    int capacity; //amount of units that the extractor can hold.
    int faction = 0; //faction ID of containing entity
    float collected;
    Vector3i sector;
    ResourceSourceType type;
    short oreId;
    private long lastEmptied = -1;
    private int entityMiningBonus = 1; //bonus from chambers or other effectgroup appliers. Does not include faction bonus (calculated on the fly)
    private transient PassiveResourceSupplier supplier;

    private static void print(String s) {
        System.out.println(s);
    }

    public Extractor(String UID, ResourceSourceType type, Vector3i sector, short OreId) {
        this.UID = UID;
        this.type = type;
        this.sector = sector;
        this.oreId = OreId;
    }

    public void setSupplier(PassiveResourceSupplier supplier) {
        this.supplier = supplier;
    }

    protected void incCollected(float amount) {
        //collected += amount;
        collected = Math.min(capacity,collected+amount);
        if(DEBUG_LOGGING)System.out.println("[MOD][Resources ReSourced] Increased extractor "+getUID()+" collected item quantity by "+amount+" to "+collected);
    }

    /**
     * Withdraw up to max units of collected items. Withdraw amount is subtracted from collected quantity logged within object.
     * Collected gets rounded down; rounding errors remain as "collected" fractional item logged in the object.
     * @param max -1 for unlimited
     * @return quantity that is withdrawn
     */
    protected int withdrawCollected(double max) {
        int out;
        if (max!=-1)
            out = (int) Math.min(max,collected);
        else
            out = (int) collected;
        collected -= out; //keep fractions, return rounded down amount
        if(DEBUG_LOGGING) print("[MOD][Resources ReSourced] Withdrew "+out+" from extractor "+getUID());
        lastEmptied = System.currentTimeMillis();
        return out;
    }

    public void setCapacityVolume(double cargoVolume) {
        float capacityPerItem = ElementKeyMap.getInfo(oreId).getVolume();
        setCapacity((int)(cargoVolume/capacityPerItem));
    }

    public void setFaction(int factionId){
        faction = factionId;
    }

    public String getUID() {
        return UID;
    }

    public float getStrength() {
        return strength;
    }

    public int getCapacity() {
        return capacity;
    }

    public float getCollected() {
        return collected;
    }

    public Vector3i getSector() {
        return sector;
    }

    public ResourceSourceType getType() {
        return type;
    }

    public void setStrength(float strength) {
        if(DEBUG_LOGGING)System.out.println("[MOD][Resources ReSourced] Set strength of extractor " + getUID() + " from " + this.strength + " to "+ strength);
        float old = this.strength;
        this.strength = strength;

        if (strength!=old && supplier != null)
            supplier.onExtractorPowerChanged(this, old);
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setEntityMiningBonus(int miningBonus) {
        this.entityMiningBonus = miningBonus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Extractor extractor = (Extractor) o;
        return UID.equals(extractor.UID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(UID);
    }

    @Override
    public String toString() {
        String last = "NULL";
        String factionName = "ERROR: Unknown Name; Faction Manager Not Present";
        if(GameServerState.instance != null && GameServerState.instance.getFactionManager() != null) factionName = GameServerState.instance.getFactionManager().getFactionName(faction);
        if (lastEmptied > 0) {
            SimpleDateFormat sdf = new SimpleDateFormat("kk:mm - EEE, d. MMM");
            last = (lastEmptied > 0)?sdf.format(new Date(lastEmptied)):"NULL";
            long since = (System.currentTimeMillis()-lastEmptied)/1000;
            long h = since/3600;
            long m = (since%3600)/60;
            last += String.format("  +%sh %smin",h,m);
        }
        return "Extractor{" +
                "UID='" + UID + '\'' +
                ", strength=" + strength +
                ", capacity=" + capacity +
                ", collected=" + collected +
                ", sector=" + sector +
                ", factionID=" + faction + " (" + factionName + ")" +
                ", type=" + type +
                ", oreId=" + oreId + "("+ElementInformation.getKeyId(oreId)+")"+
                ", lastEmptied=" + last +
                ", effectMiningBonus=" + entityMiningBonus +
                '}';
    }

    public int getEntityMiningBonus() {
        return entityMiningBonus;
    }
}
