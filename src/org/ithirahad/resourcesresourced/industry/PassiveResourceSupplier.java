package org.ithirahad.resourcesresourced.industry;

import api.mod.StarLoader;
import org.ithirahad.resourcesresourced.RRUtils.DebugUI;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.ithirahad.resourcesresourced.events.ExtractorBonusCalculateEvent;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;

import java.util.*;

import static org.ithirahad.resourcesresourced.RRSConfiguration.*;
import static org.ithirahad.resourcesresourced.RRUtils.DebugUI.lastFactionBonusException;

/**
 * STARMADE MOD
 * Creator: Ithirahad Ivrar'kiim
 * Date: 1 Nov. 2021
 * Time: Running out
 *
 * This class represents something that produces raw resources which can be extracted passively over time: a gas giant or terrestrial planet etc.
 * Extractors can be added to it to farm the resources.
 */
//@SuppressWarnings("all")
public class PassiveResourceSupplier {
    //Each supply entry contains regenRate, amount collected, and a list of stations that have extractors
    public float passivePoolRegenRate; //per second
    private float activePoolBaseRegenRate; //per second
    public double activeHarvestingPool; //amount of resources stored for active harvesting
    public int activePoolCap; //maximum amount of resources stored for active harvesting
    public short resourceTypeId;
    public ResourceSourceType type;
    private float totalExPower;
    private transient long lastPassiveUpdate;
    private transient long lastActiveUpdate;
    private final Vector3i sector;

    //private final ArrayList<Extractor> extractors = new ArrayList<>();
    private HashMap<String,Extractor> extractor_by_uid = new HashMap<>(); //FIXME causes nullpointer
    public PassiveResourceSupplier(float passivePoolRegen, short resourceTypeId, ResourceSourceType type, Vector3i sector){
        this.passivePoolRegenRate = (float) (Math.round(passivePoolRegen * 1000.0) / 1000.0); //rounded number
        this.activePoolBaseRegenRate = passivePoolRegenRate * ACTIVE_POOL_REGEN_MODIFIER;
        this.activePoolCap = (int) Math.ceil(passivePoolRegenRate * ACTIVE_POOL_SIZE_MODIFIER);
        this.resourceTypeId = resourceTypeId;
        this.type = type;
        this.sector = sector;
    }

    //TODO dont save if no extractors exist for this resource well
    //TODO ensure extractors are no double entries.

    public void beforeSerialize() {
        if(!extractor_by_uid.isEmpty())
            recalcExtractionPower();
        //last update time is not saved, so that time while server is offline doesn't count into the resource gathering.
        updatePassiveResources(true, System.currentTimeMillis());
        updateActiveResources();
    }

    public void afterDeserialize() {

        //rebuild lost (circular) references
        for (Extractor e: extractor_by_uid.values()) {
            e.setSupplier(this);
        }

        //force initial update
        updatePassiveResources(true, System.currentTimeMillis());
        lastActiveUpdate = System.currentTimeMillis();
    }

    public synchronized Extractor addExtractor(String uid, ResourceSourceType type, Vector3i sector){
        //make sure harvester doesnt exist yet with this UID
        if (existsExtractor(uid))
            return getExtractor(uid);

        Extractor ex = new Extractor(uid, type,  sector, resourceTypeId);
        extractor_by_uid.put(uid, ex);
        ex.setSupplier(this);
        return ex;
    }

    public synchronized boolean existsExtractor(String uid) {
        if (extractor_by_uid ==null) {
            DebugUI.log("extractor map is null for "+ this, true);
            extractor_by_uid = new HashMap<>();
        }
        return extractor_by_uid.containsKey(uid);
    }

    public synchronized void removeExtractor(String uid){
        extractor_by_uid.remove(uid);
    }

    public Collection<String> getAllIDs() {
        return extractor_by_uid.keySet();
    }

    public synchronized Collection<Extractor> getAllExtractors() {
        return extractor_by_uid.values();
    }

    public synchronized void clearExtractors(){
        extractor_by_uid.clear();
    }

    public synchronized Extractor getExtractor(String uid) {
        if (!existsExtractor(uid))
            return null;

        Extractor out = extractor_by_uid.get(uid);
        assert out!= null && uid.equals(out.UID);
        return out;
    }

    /**
     'Macro' function.
     Retrieves as much resource as can be fit into the specified capacity, subtracting it from the corresponding record, divided by however many resources there are.
     */
    public synchronized int claimPassiveResources(String uid, double maxCapacity) {
        if (existsExtractor(uid)) {
            double capacityPerItem = Math.max(ElementKeyMap.getInfo(resourceTypeId).getVolume(),0.00000000001f); // 0 would mean potential divide-by-zero issues
            int max = (int)(maxCapacity/capacityPerItem);
            return getExtractor(uid).withdrawCollected(max);
        }
        else
            return 0;
    }

    public void setLastPassiveUpdate(long lastPassiveUpdate) {
        this.lastPassiveUpdate = lastPassiveUpdate;
    }

    /**
     * Will update the resource well [TODO: if it's loaded?]
     * Equals one ore-generation-update, distributed generated ore proportionally to connected extractors.
     * @param force force update, regardless if loaded or not.
     * @param currentTime
     */
    public void updatePassiveResources(boolean force, long currentTime){
        if (force || isLoaded()) {
            if (extractor_by_uid.isEmpty()) {
                lastPassiveUpdate = currentTime;
                return;
            }

            long millisSinceLastUpdate = currentTime- lastPassiveUpdate;
            boolean nullUpdate = (lastPassiveUpdate <=0);
            boolean onLoaded = (millisSinceLastUpdate>10000);

            DebugUI.log("\n################## UPDATING WELL... Last Update: ("+(millisSinceLastUpdate/1000)+" sec\n"+ this,(!nullUpdate && onLoaded));
            lastPassiveUpdate = currentTime;

            if (nullUpdate) //Initial update after server start; don't do anything here. -> Time the server was offline doesn't count into passive generation.
                return;

            //calculated needed numbers like total regen collected over time
            float regenRateTimed = (passivePoolRegenRate *millisSinceLastUpdate)/(1000f); //millis -> seconds -> sum of regen rate for x seconds
            boolean oversubscribed = (totalExPower> passivePoolRegenRate);

            //increase collected ores in virtual extractors
            for (Extractor ex: extractor_by_uid.values()) {
                DebugUI.log("BEFORE UPDATING extractor: "+ ex,onLoaded);
                float changeOre = 0;
                if (!oversubscribed)
                    changeOre = (ex.getStrength() * millisSinceLastUpdate)/1000f; //extraction power adjusted for time
                else {
                    changeOre = regenRateTimed * (ex.getStrength()/totalExPower); //proportional extraction power adjusted for time
                }

                changeOre *= getSituationMiningBonus(ex.getUID(), ex.oreId, ex.type, ex.faction, ex.sector);
                changeOre *= ex.getEntityMiningBonus();

                ex.incCollected(changeOre);

                DebugUI.log(String.format("Changed ore quantity for extr. strength=%s: %s",ex.getStrength(),changeOre), onLoaded);
                DebugUI.log("FINISHED UPDATING extractor: "+ ex,onLoaded);
            }
            DebugUI.log("\n",onLoaded);
        }
    }

    public void updateActiveResources(){
        long now = System.currentTimeMillis();
        double rawNewResources = ((now - lastActiveUpdate)/1000.) * activePoolBaseRegenRate;
        activeHarvestingPool = Math.min(activePoolCap, activeHarvestingPool + rawNewResources);
        lastActiveUpdate = now;
    }

    public int claimActiveResources(float harvestingPower, double availableCapacity){
        updateActiveResources();

        int available = (int) Math.floor(activeHarvestingPool);
        int intPower = (int)Math.floor(harvestingPower);
        int result;
        if(available - intPower >= 0){
            result = intPower;
        }
        else{
            result = available;
        }
        result = Math.max(0,Math.min(result,(int) Math.floor(availableCapacity/ElementKeyMap.getInfo(resourceTypeId).getVolume())));
        activeHarvestingPool -= result;
        return result;
    }

    private boolean isLoaded() {
        //TODO: true if well or any extractor sectors are loaded, add hook for central hubs
        return true; //TODO
    }

    public boolean contains(String uid){
        return extractor_by_uid.containsKey(uid);
    }

    /**
     * event system: resource well is notified that a connected extractor now has a different pulling strength
     * @param ex
     * @param oldPower
     */
    public void onExtractorPowerChanged(Extractor ex, float oldPower) {
        //adjust total power to correctly represent sum of extractors.
        totalExPower = Math.max(0,totalExPower-oldPower);
        totalExPower += ex.getStrength();
        if(DEBUG_LOGGING) System.out.println(String.format("[MOD][Resources ReSourced] extractor for supplier changed extraction power from %s to %s, total ex power now %s",oldPower,ex.getStrength(),totalExPower));
    }

    /**
     * Clears any existing extractors that harvest from this resource supplier, and replaces them with the ones from <code>source</code>.<br>
     * Currently used when loading extractors from the persistence file.
     * @param source The source for extractors to subscribe to this well
     */
    public void copyFrom(PassiveResourceSupplier source) {
        clearExtractors();
        extractor_by_uid.putAll(source.extractor_by_uid);
        lastPassiveUpdate = source.lastPassiveUpdate;
    }

    public boolean isEmpty() {
        return extractor_by_uid.isEmpty();
    }

    public float getPassivePoolRegenRate() {
        return passivePoolRegenRate;
    }

    public short getResourceTypeId() {
        return resourceTypeId;
    }

    public ResourceSourceType getType() {
        return type;
    }

    public float getTotalExPower() {
        return totalExPower;
    }

    public Vector3i getSector() {
        return sector;
    }

    @Override
    public String toString() {
        recalcExtractionPower();

        String typeIDString = "UNKNOWN TYPE";
        try {
            typeIDString = ElementInformation.getKeyId(resourceTypeId);
        } catch(Exception ex){
            ex.printStackTrace();
           System.err.println("[MOD][Resources ReSourced][WARNING] Attempted to retrieve unknown type ID when printing passive supplier info. Were the IDs remapped between restarts?");
        }

        return "PassiveResourceSupplier{" + "\n" +
                "* sector = " + sector+ "\n" +
                "* regenRate = " + passivePoolRegenRate + "\n" +
                "* ("+(passivePoolRegenRate * 3600) + " items per hr)\n" +
                "* resourceTypeId = " + typeIDString + "\n" +
                "* type = " + type + "\n" +
                "* totalExPower = " + totalExPower + "\n" +
                "* lastUpdate = " + DebugUI.millisToString(lastPassiveUpdate,false) + "\n" +
                "* tSinceLast = " + DebugUI.millisToString(System.currentTimeMillis()- lastPassiveUpdate,true) + "\n" +
                '}';
    }

    public int getExtractorCount() {
        return extractor_by_uid.size();
    }

    public void recalcExtractionPower() {
        totalExPower = 0;
        for(Extractor ex : extractor_by_uid.values()){
            totalExPower += ex.strength;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PassiveResourceSupplier that = (PassiveResourceSupplier) o;
        return resourceTypeId == that.resourceTypeId &&
                sector.equals(that.sector);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resourceTypeId, sector);
    }

    public static int getSituationMiningBonus(String entityUID, short resType, ResourceSourceType rst, int factionId, Vector3i sector) {
        //UID and resource/source type variables just for the benefit of mods subscribing to the event, which might want that for whatever reason
        int result;
        try{
            Universe Eä = GameServerState.instance.getUniverse();
            Vector3i tmpSectorPosition = new Vector3i(sector);
            StellarSystem sys = MiscUtils.getStellarSystemFromSecPosWithoutDangerousTmps(tmpSectorPosition);
            result = Eä.getSystemOwnerShipType(sys, factionId).getMiningBonusMult();
            // TODO: We need to be sure that there won't be any cases where retrieving this somehow breaks everything...
            //  It's relatively deep into SM eldritch magic and this getStellarSystemFromSecPos method can generate a system in DB if missing (!)
            if(DEBUG_LOGGING) System.err.println("[MOD][Resources ReSourced] Received faction mining bonus: " + result);
        }
        catch(Exception ex){
            System.err.println("[MOD][Resources ReSourced][WARNING] Unable to retrieve faction mining bonus:");
            lastFactionBonusException = ex;
            ex.printStackTrace();
            result = 1;
        }
        if(DEBUG_LOGGING) System.err.println("[MOD][Resources ReSourced] Recieved server mining bonus: " + (Integer) ServerConfig.MINING_BONUS.getCurrentState());
        result *= (Integer) ServerConfig.MINING_BONUS.getCurrentState();

        ExtractorBonusCalculateEvent exev = new ExtractorBonusCalculateEvent(entityUID, factionId, sector,  resType, rst, result);
        StarLoader.fireEvent(exev, true);
        result = exev.getBonus();
        if(DEBUG_LOGGING) System.err.println("[MOD][Resources ReSourced] Calculated total situational mining bonus: " + result);
        return result;
    }

    public boolean hasExtractors() {
        return !extractor_by_uid.isEmpty();
    }
}
