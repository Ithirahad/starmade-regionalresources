package org.ithirahad.resourcesresourced.industry;

public enum ResourceSourceType {
    SPACE, GROUND
}
