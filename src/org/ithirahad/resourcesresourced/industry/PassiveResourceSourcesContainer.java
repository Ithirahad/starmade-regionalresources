package org.ithirahad.resourcesresourced.industry;

import api.common.GameServer;
import org.apache.commons.lang3.ArrayUtils;
import org.ithirahad.resourcesresourced.RRSElementInfoManager;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.movie.craterstudio.data.tuples.Pair;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.server.ServerMessage;

import javax.vecmath.Vector4f;
import java.util.*;

import static org.ithirahad.resourcesresourced.ResourcesReSourced.container;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.modInstance;
import static org.ithirahad.resourcesresourced.listeners.BlockRemovalLogic.getStarResourceSector;

public class PassiveResourceSourcesContainer {
    private static final PassiveResourceSupplier[] nothing = new PassiveResourceSupplier[]{};
    private final LinkedHashMap<Vector3i, PassiveResourceSupplier[]> resourceMap = new LinkedHashMap<>();

    public HashMap<Vector3i, PassiveResourceSupplier[]> getMap(){
        return resourceMap;
    }

    public void copyFrom(PassiveResourceSourcesContainer origin){
        resourceMap.clear();
        resourceMap.putAll(origin.getMap());
    }

    /**
     * clones active resource wells into this container from the origin
     * @param origin The container to copy wells from.
     */
    public void copyOnlyActiveFrom(PassiveResourceSourcesContainer origin){
        resourceMap.clear();

        Map<Vector3i, PassiveResourceSupplier[]> originMap = origin.getMap();

        for(Vector3i key : originMap.keySet()) {
            boolean hasAnyActive = false;
            for(PassiveResourceSupplier well : originMap.get(key)){
                if (!well.isEmpty()) {
                    hasAnyActive = true;
                    break;
                }
            }
            if(hasAnyActive) resourceMap.put(new Vector3i(key), originMap.get(key));
            modInstance.logInfo("Copied active resource well at " + key + ": " + originMap.get(key).length + " sources.");
        }
    }

    public void beforeSerialize() {
        for (PassiveResourceSupplier[] ps: resourceMap.values()) {
            for (PassiveResourceSupplier p: ps)
                p.beforeSerialize();
        }
        bustGhosts();
    }
    public void afterDeserialize() {
        for (PassiveResourceSupplier[] ps: resourceMap.values()) {
            for (PassiveResourceSupplier p: ps)
                p.afterDeserialize();
        }
    }

    public PassiveResourceSupplier[] getPassiveSources(Vector3i sector){
        return resourceMap.containsKey(sector) ? resourceMap.get(sector) : nothing;
    }

    //FIXME this is wacky and needs more clear and documented logic. when is old stuff kept, when overwritten, when deleted.

    /**
     * Sets passive resources for this sector. Will merge new and existing values and copy over extractor data from matches.
     * @param sector The target sector to set resources for.
     * @param newValues The new resource source(s) to add.
     * @return the newly-added or merged array of resource suppliers.
     */
    public PassiveResourceSupplier[] setPassiveSources(Vector3i sector, PassiveResourceSupplier[] newValues){
        //Intention: get rid of wells that don't exist in the new generated version
        //keep existing wells that have a new counterpart

        if (resourceMap.containsKey(sector)) {
            //prepare data for easy existence check
            PassiveResourceSupplier[] oldValues = resourceMap.get(sector);
            HashMap<PassiveResourceSupplier,Integer> oldWells = new HashMap<>(); //well->index in array
            int i = 0;
            for (PassiveResourceSupplier w: resourceMap.get(sector)) {
                oldWells.put(w,i);
                i++;
            }

            //copy over old data that is still relevant
            for (i = 0; i < newValues.length; i++) {
                if (oldWells.containsKey(newValues[i])) {
                    int idx = oldWells.get(newValues[i]);
                    newValues[i].copyFrom(oldValues[idx]); //copy old data into new well (copies extractors and last extraction time)
                }
            }
        }


        resourceMap.put(sector,newValues);
        return newValues;
    }

    public void resetPassiveSources(Vector3i sector){
        resourceMap.remove(sector);
    }

    public PassiveResourceSupplier[] addPassiveSources(Vector3i sector, PassiveResourceSupplier[]... rsc){
        return resourceMap.put(sector, (PassiveResourceSupplier[]) ArrayUtils.addAll(getPassiveSources(sector),rsc));
    }

    public Collection<PassiveResourceSupplier[]> getAllSources(){return resourceMap.values();}

    public PassiveResourceSupplier[] getAllAvailableSourcesInArea(Vector3i sector, Vector3i system) {
        PassiveResourceSupplier[][] allSources = {
                container.getPassiveSources(sector),
                container.getPassiveSources(new Vector3i(sector.x + 1, sector.y, sector.z)),
                container.getPassiveSources(new Vector3i(sector.x - 1, sector.y, sector.z)),
                container.getPassiveSources(new Vector3i(sector.x, sector.y + 1, sector.z)),
                container.getPassiveSources(new Vector3i(sector.x, sector.y - 1, sector.z)),
                container.getPassiveSources(new Vector3i(sector.x, sector.y, sector.z + 1)),
                container.getPassiveSources(new Vector3i(sector.x, sector.y, sector.z - 1)),
                container.getPassiveSources(getStarResourceSector(system))
        };
        ArrayList<PassiveResourceSupplier> results = new ArrayList<>();
        for(PassiveResourceSupplier[] rsc : allSources) Collections.addAll(results, rsc);
        return results.toArray(new PassiveResourceSupplier[]{});
    } //macro

    public PassiveResourceSupplier[] getAllAvailableSourcesInArea(SegmentController controller) {
        return getAllAvailableSourcesInArea(controller.getSector(new Vector3i()),controller.getSystem(new Vector3i()));
    }

    //TODO return custom iterator to loop over all suppliers and ignore the array->supplier structure

    public List<Pair<Vector3i, Vector4f>> getSpaceSourceColorsAndLocationsForDisplay(SegmentController controller){
        ArrayList<Pair<Vector3i, Vector4f>> result = new ArrayList<>();

        HashSet<PassiveResourceSupplier> adjacentSourceSetsPresentInContainer = new HashSet<>(Arrays.asList(getAllAvailableSourcesInArea(controller)));

        for(PassiveResourceSupplier source : adjacentSourceSetsPresentInContainer){
            Vector3i key = source.getSector();
            //we check if there is any special resource to give a colour, else give a generic one.
            if(source.type == ResourceSourceType.SPACE){

                result.add(new Pair<>(key,getResourceColor(source.resourceTypeId)));
            }
            //this is a stupid way of determining validity, but it's either this or a pointless boolean...
        }
        return result;
    }

    public static Vector4f getResourceColor(short resourceTypeId){
        short parsyne = RRSElementInfoManager.elementEntries.get("Parsyne Plasma").id;
        short anbaric = RRSElementInfoManager.elementEntries.get("Anbaric Vapor").id;
        short castellium = -1;
        if(ResourcesReSourced.bastionInitiativeIsPresentAndActive) castellium = RRSElementInfoManager.elementEntries.get("Castellium Condensate").id;

        Vector4f color = new Vector4f();
        if(resourceTypeId == anbaric) color.set(0.79f,0.12f,0.06f,1f);
        else if(resourceTypeId == parsyne) color.set(0.95f,0.98f,0.99f,1f);
        else if(resourceTypeId == castellium) color.set(0.06f, 0.86f, 0.97f, 1f);
        else color.set(0.8f,0.8f,0.83f,1f); //generic colour

        return color;
    } //TODO move elsewhere

    public static Vector4f getResourceColor(PassiveResourceSupplier resource){
        return getResourceColor(resource.resourceTypeId);
    }


    public List<PassiveResourceSupplier> getPassiveSourcesInSystem(Vector3i sys) {
        List<PassiveResourceSupplier> rs = new LinkedList<>();
        Vector3i triedSystemPos = new Vector3i();
        for(Vector3i sector : resourceMap.keySet()){
            if(sys.equals(VoidSystem.getContainingSystem(sector,triedSystemPos))){
                Collections.addAll(rs, resourceMap.get(sector));
            }
        }
        return rs;
    }

    public boolean isEmpty() {
        return resourceMap.isEmpty();
    }

    /**
    Removes extractors with nonexistent IDs from all resource suppliers.
     */
    public void bustGhosts() {
        for(PassiveResourceSupplier[] arr : resourceMap.values()) for(PassiveResourceSupplier well : arr){
            List<String> ghosts = new LinkedList<>();
            for(String id : well.getAllIDs()) {
                if(!MiscUtils.existsInDBWithoutFluff(id))
                    ghosts.add(id);
            }
            for(String ghost : ghosts){
                modInstance.logWarning("Removed ghost extractor: " + ghost);
                well.removeExtractor(ghost);
            }

            if(ghosts.size() > 0) for (RegisteredClientOnServer client : GameServer.getServerState().getClients().values()) {
                try {
                    PlayerState player = GameServer.getServerState().getPlayerFromName(client.getPlayerName());
                    player.sendServerMessage(Lng.astr("[SERVER][WARNING] Removed " + ghosts.size() + " ghost extractors."), ServerMessage.MESSAGE_TYPE_INFO); //TODO: MESSAGE_TYPE_DIALOG can be used for the sensor readouts for now.
                } catch (PlayerNotFountException ignore) {}
            }
        }
    }

    public int size() {
        return resourceMap.size();
    }
}

