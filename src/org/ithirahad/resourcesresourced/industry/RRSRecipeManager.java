package org.ithirahad.resourcesresourced.industry;

import api.config.BlockConfig;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.ithirahad.resourcesresourced.events.RRSRecipeAddEvent;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.data.element.*;
import org.schema.game.server.data.GameServerState;

import java.util.ArrayList;
import java.util.List;

import static api.config.BlockConfig.getElements;
import static api.mod.StarLoader.fireEvent;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.*;
import static org.schema.game.common.data.element.ElementKeyMap.*;

public class RRSRecipeManager {
    public static int CAPSULE_BAKE_TIME = 100;
    public static int T0_BAKE_TIME = 5;
    public static int T1_BAKE_TIME = 5;
    public static int T2_BAKE_TIME = 5;
    public static int T3_BAKE_TIME = 7;
    public static int HARVESTER_EXTRACTION_CYCLES = 5; //vapor/magmatic
    public static int BLOCK_BAKE_TIME = 1; //Block dis/assembly in batch should happen more or less instantly (balancing via component build times only)
    public static int BLOCK_DISASSEMBLY_BAKE_TIME = 100;
    public static int COMPONENT_RECYCLE_BAKE_TIME = 1800;
    public static int commonAmountSmall = 5; //amount of materials to craft components
    public static int commonAmountMedium = 15;
    public static int commonAmountLarge = 30;
    public static int commonAmountHuge = 500;
    public static int rareAmountSmall = 5;
    public static int rareAmountMedium = 15;
    public static int rareAmountLarge = 30;
    public static int rareAmountHuge = 200;

    private static int COMPONENT_FACTORY_INDEX; //component factory
    private static int BLOCK_FACTORY_INDEX; //block factory

    static ElementCategory lightCategory;
    static ElementCategory terrainCategory;
    static ElementCategory basicArmorCategory;
    static ElementCategory standardArmorCategory;
    static ElementCategory advancedArmorCategory; //Also includes crystal armor
    static ElementCategory ingotCategory;
    static ElementCategory decoCategory; //scaffold
    static ElementCategory logicCategory;
    static ElementCategory railCategory;
    static ElementCategory plantCategory;
    static ElementCategory carvedStoneCategory;

    public static ArrayList<ElementInformation> oldFactories = new ArrayList<>();
    public static ArrayList<ElementInformation> oldCapsules = new ArrayList<>();
    //probably bad practice to do deprecation lists as static like this, but it's either this or change the args every time I think of an old category to decorativify

    public static boolean[] hasRRSRecipe;
    public static boolean[] hasComponentRecipe;

    public static FixedRecipe blockDisassemblyRecipes = new FixedRecipe(){
        {
            name = "Block Disassembly";
        }
    };
    public static FixedRecipe componentRecyclingRecipes = new FixedRecipe(){
        {
            name = "Component Recycling";
        }
    };
    private static final ArrayList<FixedRecipeProduct> componentRecyclingProducts = new ArrayList<>();
    private static final ArrayList<FixedRecipeProduct> blockDisassemblyProducts = new ArrayList<>();

    public static void addRecipes(ElementInformation componentFactory, ElementInformation blockFactory) {
        hasRRSRecipe = new boolean[2048]; //Very sparse array with values corresponding to whether or not there's an RRS recipe, at positions corresponding to IDs.
        hasComponentRecipe = new boolean[2048]; //ditto but for components
        //clear out the vanilla capsule/macro recipes
        ElementKeyMap.capsuleRecipe.recipeProducts = new FixedRecipeProduct[]{};
        ElementKeyMap.personalCapsuleRecipe.recipeProducts = new FixedRecipeProduct[]{};
        ElementKeyMap.macroBlockRecipe.recipeProducts = new FixedRecipeProduct[]{};
        ElementKeyMap.microAssemblerRecipe.recipeProducts = new FixedRecipeProduct[]{};

        COMPONENT_FACTORY_INDEX = BlockConfig.customFactories.get(componentFactory.id);
        BLOCK_FACTORY_INDEX = BlockConfig.customFactories.get(blockFactory.id);

        int crystal = ElementKeyMap.CRYSTAL_CRIRCUITS;
        int mesh = ElementKeyMap.METAL_MESH;
        int rockDust = elementEntries.get("Rock Dust Capsule").id;
        int plantCrap = elementEntries.get("Floral Protomaterial Capsule").id;
        int ferron = elementEntries.get("Ferron Capsule").id;
        int aegium = elementEntries.get("Aegium Capsule").id;
        int anbaric = elementEntries.get("Anbaric Capsule").id;
        int parsyne = elementEntries.get("Parsyne Capsule").id;
        int thermyn = elementEntries.get("Thermyn Capsule").id;
        int macetine = elementEntries.get("Macetine Capsule").id;
        int threns = elementEntries.get("Threns Capsule").id;

        //----PERSONAL REFINERIES
        BlockConfig.addRefineryRecipe(ElementKeyMap.personalCapsuleRecipe, new FactoryResource[]{fr(1, "Crystalline Ore")}, new FactoryResource[]{fr(PERSONAL_CAPSULE_CONVERSION_FACTOR, crystal)});
        BlockConfig.addRefineryRecipe(ElementKeyMap.personalCapsuleRecipe, new FactoryResource[]{fr(1, "Metallic Ore")}, new FactoryResource[]{fr(PERSONAL_CAPSULE_CONVERSION_FACTOR, mesh)});

        //----PERSONAL COMPONENTS
        BlockConfig.addRefineryRecipe(ElementKeyMap.microAssemblerRecipe, new FactoryResource[]{fr(commonAmountMedium, crystal)}, new FactoryResource[]{comp(1, "Crystal Panel")});
        BlockConfig.addRefineryRecipe(ElementKeyMap.microAssemblerRecipe, new FactoryResource[]{fr(commonAmountMedium, mesh)}, new FactoryResource[]{comp(1, "Metal Sheet")});

        //----PERSONAL BLOCKS
        BlockConfig.addRefineryRecipe(ElementKeyMap.macroBlockRecipe, new FactoryResource[]{comp(2, "Crystal Panel")}, new FactoryResource[]{fr(1, "Component Fabricator")});
        BlockConfig.addRefineryRecipe(ElementKeyMap.macroBlockRecipe, new FactoryResource[]{comp(2, "Metal Sheet")}, new FactoryResource[]{fr(1, "Block Assembler")});

        //----CAPSULE REFINERY
        addRefine(fr(1, "Metallic Ore"), fr(BASE_ORE_CAPSULE_CONVERSION_FACTOR, mesh));
        addRefine(fr(1, "Crystalline Ore"), fr(BASE_ORE_CAPSULE_CONVERSION_FACTOR, crystal)); //TODO: custom conversion factor for these?
        addRefine(fr(1, "Ferron Ore"), fr(BASE_ORE_CAPSULE_CONVERSION_FACTOR, "Ferron Capsule"));
        addRefine(fr(1, "Thermyn Amalgam"), fr(BASE_LIQUID_CAPSULE_CONVERSION_FACTOR, "Thermyn Capsule"));
        addRefine(fr(1, "Aegium Ore"), fr(BASE_ORE_CAPSULE_CONVERSION_FACTOR, "Aegium Capsule"));
        addRefine(fr(1, "Parsyne Plasma"), fr(BASE_GAS_CAPSULE_CONVERSION_FACTOR, "Parsyne Capsule"));
        addRefine(fr(1, "Anbaric Vapor"), fr(BASE_GAS_CAPSULE_CONVERSION_FACTOR, "Anbaric Capsule"));
        addRefine(fr(1, "Macetine Aggregate"), fr(BASE_ORE_CAPSULE_CONVERSION_FACTOR, "Macetine Capsule"));
        addRefine(fr(1, "Threns Relic"), fr(BASE_ORE_CAPSULE_CONVERSION_FACTOR, "Threns Capsule"));

        //Scrap
        addRefine(fr(5, 546), fr(1, mesh));
        addRefine(fr(5, 547), fr(1, crystal));


        //----COMPONENT FAB
        //T0

        //T1
        addComponent("Omnichrome Paint", 1, fr(1, crystal), fr(1, rockDust));
        addComponent("Crystal Light Source", 1, fr(1, crystal));
        addComponent("Crystal Energy Focus", 1, fr(commonAmountMedium, crystal));
        addComponent("Standard Circuitry", 1, fr(commonAmountMedium, crystal), fr(commonAmountSmall, mesh));
        addComponent("Energy Cell", 1, fr(commonAmountSmall, crystal), fr(commonAmountSmall, mesh));
        addComponent("Crystal Panel", 1, fr(commonAmountMedium, crystal));
        addComponent("Metal Frame", 1, fr(1, mesh));
        addComponent("Metal Sheet", 1, fr(commonAmountSmall, mesh)); //The game uses a lot more metal than crystal; the 'expensive' component should not be expensive

        //T2
        addComponent("Parsyne Amplifying Focus", 2, fr(rareAmountSmall, parsyne), fr(commonAmountMedium, crystal));
        addComponent("Parsyne Holographic Processor", 2, fr(rareAmountMedium, parsyne), fr(commonAmountMedium, mesh));
        addComponent("Macetine Alloy Plating", 2, fr(rareAmountMedium, macetine), fr(commonAmountLarge, mesh));
        addComponent("Macetine Magnetic Rail", 2, fr(rareAmountSmall, macetine), fr(commonAmountMedium, mesh));
        addComponent("Ferron Resonant Circuitry", 2, fr(rareAmountLarge, ferron), fr(commonAmountMedium, crystal));
        addComponent("Aegium Field Emitter", 2, fr(rareAmountSmall, aegium), fr(commonAmountMedium, crystal));
        addComponent("Anbaric Distortion Coil", 2, fr(rareAmountSmall, anbaric), fr(commonAmountMedium, mesh));
        addComponent("Thermyn Power Charge", 2, fr(8, thermyn), fr(commonAmountMedium, mesh));
        addComponent("Threns Wire Matrix", 2, fr(rareAmountSmall, threns), fr(commonAmountSmall, mesh));

        //T3
        addComponent("Prismatic Circuit", 3, fr(rareAmountMedium, ferron), fr(rareAmountMedium, parsyne), fr(rareAmountMedium, aegium), fr(rareAmountMedium, macetine), fr(commonAmountMedium, crystal), fr(commonAmountMedium, mesh));

        //----BLOCK ASSM

        // BATCH STUFF
        lightCategory = getInfo(55).type;
        terrainCategory = getInfo(453).type;
        basicArmorCategory = getParentCat(getInfo(598).type);
        standardArmorCategory = getParentCat(getInfo(5).type);
        advancedArmorCategory = getParentCat(getInfo(263).type); //Also includes crystal armor
        ingotCategory = getParentCat(getInfo(468).type);
        decoCategory = getInfo(656).type; //scaffold
        logicCategory = getInfo(ElementKeyMap.SIGNAL_NOT_BLOCK_ID).type;
        railCategory = getInfo(ElementKeyMap.RAIL_BLOCK_BASIC).type;
        plantCategory = getInfo(ElementKeyMap.TERRAIN_YHOLE_PURPLE_SPRITE).type;
        carvedStoneCategory = getInfo(145).type;

        assert lightCategory != null;
        assert ingotCategory != null;
        assert terrainCategory != null;
        assert basicArmorCategory != null;
        assert standardArmorCategory != null;
        assert advancedArmorCategory != null;
        //just so IntelliJ stops whining

        for (ElementInformation info : getInfoArray()) {
            if (info != null && !info.deprecated && !hasRRSRecipe[info.id]) {
                ElementCategory parent = getParentCat(info.type);
                if (parent != null && info.sourceReference == 0) { // 'source reference' would mean it's placed via another block
                    if (basicArmorCategory.equals(parent)) { //Why did I do the .equals backwards? No clue, but I'm keeping it :D
                        addBlock(info, comp(6, "Metal Sheet"), comp(1, "Omnichrome Paint"));
                    } else if (standardArmorCategory.equals(parent) || info.id == 436 || info.id == 438) { //incls. hazard armors
                        addBlock(info, comp(6, "Metal Sheet"), comp(1, "Macetine Alloy Plating"), comp(1, "Omnichrome Paint"));
                    } else if (advancedArmorCategory.equals(parent)) {
                        if (info.name.contains("Crystal"))
                            addBlock(info, comp(6, "Crystal Panel"), comp(2, "Macetine Alloy Plating"), comp(1, "Omnichrome Paint"));
                        else
                            addBlock(info, comp(6, "Metal Sheet"), comp(2, "Macetine Alloy Plating"), comp(1, "Omnichrome Paint"));
                    } else if (decoCategory.equals(info.type) || isParentCat(decoCategory.getCategory(), info)) {
                        if (info.id != 220 && info.id != 440) //mesh/composite shouldn't be craftable this way
                            addBlock(info, comp(1, "Metal Frame"));
                    } else if (sameCatAs(info, lightCategory) || isSiblingCatTo(info, lightCategory.getCategory())) { //either a light or a light bar
                        addBlock(info, comp(1, "Metal Frame"), comp(1, "Crystal Light Source"));
                    } else if (ingotCategory.equals(info.type)) {
                        if (info.name.contains("Ingot"))
                            addBlock(info, comp(1, "Metal Sheet"), comp(1, "Omnichrome Paint")); //...else? check this
                    } else if (plantCategory.equals(info.type) || info.id == TERRAIN_CACTUS_ID || info.id == TERRAIN_TREE_LEAF_ID || info.id == TERRAIN_ICEPLANET_LEAVES || info.id == TERRAIN_TREE_TRUNK_ID) {
                        addRefine(fr(1,info.id),fr(1,plantCrap));
                        addBlock(info, fr(1, plantCrap));
                    } else if (terrainCategory.equals(info.type) || getInfo(TERRAIN_ROCK_GREEN).type.equals(info.type)) { //can't be bothered to make a variable for the coloured rock subtype
                        if(info.id == TERRAIN_EARTH_TOP_DIRT || info.id == TERRAIN_EARTH_TOP_ROCK || info.id == TERRAIN_PURPLE_ALIEN_VINE || info.id == TERRAIN_PURPLE_ALIEN_TOP){
                            addRefine(fr(1, info.id), fr(1, rockDust), fr(1, plantCrap));
                            addBlock(info, fr(1, rockDust), fr(1, plantCrap));
                        }
                        else{
                            addRefine(fr(1, info.id), fr(1, rockDust));
                            addBlock(info, fr(1, rockDust));
                        }
                        //TODO: ice isn't rock... add a water capsule? lol
                    } else if (logicCategory.equals(info.type)) {
                        addBlock(info, comp(1, "Metal Frame"), comp(1, "Standard Circuitry"));
                    } else if (railCategory.equals(info.type)) {
                        addBlock(info, comp(2, "Macetine Magnetic Rail"), comp(1, "Metal Frame"));
                    } else if (carvedStoneCategory.equals(info.type)) {
                        addBlock(info, fr(1, rockDust), comp(1, "Omnichrome Paint"));
                    }
                }
            }
        }

        for (ElementInformation oldfac : oldFactories) addBlock(oldfac, comp(1, "Metal Frame"), comp(1, "Crystal Panel"));
        for (ElementInformation oldcap : oldCapsules) addBlock(oldcap, comp(1, "Crystal Panel"), comp(1, "Omnichrome Paint"));

        //INDIVIDUAL RECIPES

        addBlock(ElementKeyMap.WATER, fr(1, ElementKeyMap.TERRAIN_ICE_ID)); //TODO: infinite loop/stack overflow with reverse recipe
        addBlock(ElementKeyMap.TERRAIN_ICEPLANET_CRYSTAL, fr(10, ElementKeyMap.TERRAIN_ICE_ID));
        //TODO: consider separate terrain processor factory (might be unnecessarily complex, but the terrain ecosystem seems significantly different)

        // -- BASICS

        addBlock(ElementKeyMap.CORE_ID, comp(1, "Metal Frame"), comp(1, "Standard Circuitry"), comp(1, "Energy Cell"), comp(6, "Crystal Energy Focus"));
        addBlock(1008, comp(1, "Metal Frame"), comp(1, "Energy Cell")); //power reactor
        addBlock(1009, comp(1, "Metal Frame"), comp(1, "Standard Circuitry")); //power stabilizer
        addBlock(1010, comp(2, "Metal Frame")); //chamber conduit
        addBlock(ElementKeyMap.THRUSTER_ID, comp(1, "Metal Frame"), comp(1, "Energy Cell"), comp(1, "Crystal Energy Focus"));
        addBlock(ElementKeyMap.SHIELD_CAP_ID, comp(1, "Metal Frame"), comp(1, "Energy Cell"), comp(1, "Aegium Field Emitter"));
        addBlock(ElementKeyMap.SHIELD_REGEN_ID, comp(1, "Metal Frame"), comp(1, "Ferron Resonant Circuitry"), comp(1, "Aegium Field Emitter"));
        addBlock(ElementKeyMap.STASH_ELEMENT, comp(4, "Metal Frame"), comp(2, "Metal Sheet"), comp(1, "Standard Circuitry"));
        addBlock(ElementKeyMap.CARGO_SPACE, comp(1, "Metal Frame"));

        addBlock(ElementKeyMap.COCKPIT_ID, comp(1,"Metal Frame"), comp(1, "Standard Circuitry")); //Camera

        addBlock(ElementKeyMap.GRAVITY_ID, comp(1, "Metal Frame"), comp(1, "Standard Circuitry")); //Lorewise this should need an anbaric coil, but that'd be sort of evil

        addBlock(ElementKeyMap.SALVAGE_CONTROLLER_ID, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Standard Circuitry"), comp(1, "Crystal Panel"));
        addBlock(ElementKeyMap.SALVAGE_ID, comp(1, "Metal Frame"), comp(1, "Crystal Energy Focus"));

        addBlock(elementEntries.get("Gas Harvester Computer"), comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Standard Circuitry"), comp(1, "Crystal Panel"));
        addBlock(elementEntries.get("Gas Harvester Module"), comp(1, "Metal Frame"), comp(1, "Crystal Energy Focus"));

        addBlock(ElementKeyMap.AI_ELEMENT, comp(1, "Metal Frame"), comp(1, "Standard Circuitry"));

        addBlock(66, comp(1, "Metal Frame")); //Stabilizer stream node (deprecated but w/e)

        // -- FACTION AND STATION BLOCKS

        addBlock(ElementKeyMap.FACTION_BLOCK, comp(1, "Metal Frame"), comp(5, "Standard Circuitry"));
        addBlock(ElementKeyMap.FACTION_FACTION_EXCEPTION_ID, comp(1, "Metal Frame"), comp(1, "Standard Circuitry"));
        addBlock(ElementKeyMap.FACTION_PUBLIC_EXCEPTION_ID, comp(1, "Metal Frame"), comp(1, "Standard Circuitry"));
        addBlock(ElementKeyMap.PLAYER_SPAWN_MODULE, comp(1, "Metal Frame"), comp(10, "Standard Circuitry"), comp(1, "Crystal Energy Focus"));
        addBlock(ElementKeyMap.BUILD_BLOCK_ID, comp(1, "Metal Sheet"), comp(1, "Standard Circuitry"), comp(1, "Crystal Energy Focus"));
        addBlock(ElementKeyMap.SHOP_BLOCK_ID, comp(6, "Metal Frame"), comp(4, "Crystal Panel"), comp(4, "Standard Circuitry"));

        addBlock(ElementKeyMap.SHIPYARD_COMPUTER, comp(1, "Metal Frame"), comp(2, "Metal Sheet"), comp(1,"Standard Circuitry"), comp(1,"Crystal Panel"), comp(1, "Aegium Field Emitter"), comp(1, "Parsyne Holographic Processor"));
        addBlock(ElementKeyMap.SHIPYARD_MODULE, comp(1, "Metal Frame"), comp(1, "Standard Circuitry"));
        addBlock(ElementKeyMap.SHIPYARD_CORE_POSITION, comp(1, "Aegium Field Emitter"));

        // -- INDUSTRY BLOCKS

        addBlock(ElementKeyMap.FACTORY_CAPSULE_ASSEMBLER_ID, comp(2, "Crystal Panel"));
        addBlock("Component Fabricator", comp(2, "Crystal Panel"));
        addBlock("Block Assembler", comp(2, "Metal Sheet"));
        addBlock("Vapor Siphon", comp(2, "Metal Frame"), comp(1, "Crystal Energy Focus"));
        addBlock("Magmatic Extractor", comp(2, "Metal Frame"), comp(1, "Crystal Energy Focus"));
        addBlock("Block Disassembler", comp(2, "Metal Frame"), comp(1, "Standard Circuitry"));
        addBlock("Component Recycler", comp(2, "Metal Frame"), comp(1, "Crystal Energy Focus"));
        addBlock(ElementKeyMap.FACTORY_INPUT_ENH_ID, comp(24, "Macetine Magnetic Rail"), comp(2, "Metal Frame"), comp(1, "Standard Circuitry")); //TODO: add Macetine Magnetic Rails to this recipe on next recipe adjustment

        // -- DOORS

        addBlock(ElementKeyMap.DOOR_ELEMENT, comp(1, "Metal Frame"), comp(1, "Metal Sheet"));
        addBlock(589, comp(1, "Metal Frame"), comp(1, "Crystal Panel")); //glass door
        addBlock(591, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(6, "Macetine Alloy Plating")); //blast door

        addBlock(659, comp(1, "Metal Frame"), comp(1, "Crystal Energy Focus"), comp(6, "Macetine Alloy Plating")); //force field
        addBlock(660, comp(1, "Metal Frame"), comp(1, "Crystal Energy Focus"), comp(6, "Macetine Alloy Plating")); //force field
        addBlock(661, comp(1, "Metal Frame"), comp(1, "Crystal Energy Focus"), comp(6, "Macetine Alloy Plating")); //force field
        addBlock(1129, comp(1, "Metal Frame"), comp(1, "Crystal Energy Focus"), comp(6, "Macetine Alloy Plating")); //force field

        // -- CHAMBERS

        addBlock(991, comp(1, "Metal Frame"), comp(1, "Standard Circuitry")); //Mass Chamber
        addBlock(1048, comp(1, "Metal Frame"), comp(1, "Thermyn Power Charge"), comp(1, "Energy Cell")); //Power chamber

        addBlock(1011, comp(1, "Metal Frame"), comp(1, "Anbaric Distortion Coil"), comp(1, "Crystal Energy Focus")); //Mobility Chamber
        addBlock(1013, comp(1, "Metal Frame"), comp(1, "Anbaric Distortion Coil"), comp(1, "Standard Circuitry")); //FTL Chamber

        addBlock(1012, comp(1, "Metal Frame"), comp(1, "Ferron Resonant Circuitry"), comp(1, "Parsyne Holographic Processor")); //Recon chamber
        addBlock(1014, comp(1, "Metal Frame"), comp(2, "Ferron Resonant Circuitry"), comp(1, "Parsyne Holographic Processor"), comp(1, "Anbaric Distortion Coil")); //Stealth chamber

        addBlock(1015, comp(1, "Metal Frame"), comp(1, "Aegium Field Emitter"), comp(1, "Crystal Energy Focus")); //Logistics chamber
        addBlock(1046, comp(1, "Metal Frame"), comp(1, "Aegium Field Emitter"), comp(1, "Standard Circuitry")); //Defence chamber

        // -- WEAPONS

        addBlock(414, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Standard Circuitry"), comp(10, "Parsyne Amplifying Focus")); //Damage Beam computer
        addBlock(415, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Parsyne Amplifying Focus")); //Damage Beam

        addBlock(6, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Standard Circuitry"), comp(10, "Macetine Magnetic Rail")); //Cannon computer
        addBlock(16, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Macetine Magnetic Rail")); //Cannon

        addBlock(38, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Standard Circuitry"), comp(10, "Thermyn Power Charge")); //Missile computer
        addBlock(32, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Thermyn Power Charge")); //Missile
        addBlock(362, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(9, "Thermyn Power Charge")); //Missile Capacity

        addBlock(ElementKeyMap.EXPLOSIVE_ID, comp(1, "Metal Frame"), comp(1, "Standard Circuitry"), comp(10, "Thermyn Power Charge"));

        addBlock(ElementKeyMap.MINE_LAYER, comp(4, "Metal Frame"), comp(2, "Metal Sheet"), comp(10, "Ferron Resonant Circuitry"), comp(10, "Thermyn Power Charge"));
        addBlock(ElementKeyMap.MINE_CORE, comp(1, "Metal Frame"), comp(1, "Energy Cell"), comp(1, "Thermyn Power Charge"), comp(1, "Ferron Resonant Circuitry"));

        addBlock(ElementKeyMap.MINE_MOD_STEALTH,    comp(1,"Metal Frame"), comp(1,"Ferron Resonant Circuitry"));
        addBlock(ElementKeyMap.MINE_MOD_PERSONAL,   comp(1,"Metal Frame"), comp(1, "Standard Circuitry"));
        addBlock(ElementKeyMap.MINE_MOD_FRIENDS,    comp(1,"Metal Frame"), comp(1, "Standard Circuitry"));
        addBlock(ElementKeyMap.MINE_TYPE_CANNON,    comp(1, "Metal Frame"), comp(9, "Macetine Magnetic Rail"), comp(1, "Energy Cell"));
        addBlock(ElementKeyMap.MINE_TYPE_MISSILE,   comp(1, "Metal Frame"), comp(9, "Thermyn Power Charge"), comp(1, "Energy Cell"));
        addBlock(ElementKeyMap.MINE_TYPE_PROXIMITY, comp(1, "Metal Frame"), comp(1, "Standard Circuitry"));
        addBlock(ElementKeyMap.MINE_MOD_STRENGTH,   comp(1, "Metal Frame"), comp(1, "Thermyn Power Charge"));

        addBlock(ElementKeyMap.PUSH_PULSE_CONTROLLER_ID, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Standard Circuitry"), comp(10, "Aegium Field Emitter"));
        addBlock(ElementKeyMap.PUSH_PULSE_ID,  comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Aegium Field Emitter"));
        //Not entirely certain as to why these are still here...

        // -- TERTIARIES

        addBlock(349, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Prismatic Circuit"), comp(10, "Aegium Field Emitter")); //EM Computer
        addBlock(350, comp(1, "Metal Frame"), comp(1, "Aegium Field Emitter"));

        addBlock(351, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Prismatic Circuit"), comp(10, "Thermyn Power Charge")); //Heat Computer
        addBlock(352, comp(1, "Metal Frame"), comp(1, "Thermyn Power Charge"));

        addBlock(353, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Prismatic Circuit"), comp(10, "Macetine Magnetic Rail")); //Kinetic Computer
        addBlock(354, comp(1, "Metal Frame"), comp(1, "Macetine Magnetic Rail"));

        // -- UTILITY BEAMS

        addBlock(360, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Standard Circuitry"), comp(10, "Anbaric Distortion Coil")); //Tractor beam computer
        addBlock(361, comp(1, "Metal Frame"), comp(1, "Crystal Energy Focus"), comp(1, "Anbaric Distortion Coil")); //Tractor beam

        addBlock(ElementKeyMap.REPAIR_CONTROLLER_ID, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Parsyne Holographic Processor"), comp(10, "Aegium Field Emitter")); //Astrotech Computer
        addBlock(ElementKeyMap.REPAIR_ID, comp(1, "Metal Frame"), comp(1, "Parsyne Amplifying Focus"), comp(1, "Aegium Field Emitter")); //Astrotech

        //Rest in peace, power and shield transfer beams

        // -- OTHER ODDS & ENDS

        addBlock(ElementKeyMap.TRANSPORTER_CONTROLLER, comp(1,"Metal Frame"), comp(6, "Metal Sheet"), comp(1,"Crystal Panel"), comp(5,"Standard Circuitry"), comp(10,"Anbaric Distortion Coil"));
        addBlock(ElementKeyMap.TRANSPORTER_MODULE, comp(1,"Metal Frame"), comp(1,"Anbaric Distortion Coil"), comp(1,"Aegium Field Emitter"));

        addBlock(ElementKeyMap.RACE_GATE_CONTROLLER, comp(1, "Metal Frame"), comp(5, "Standard Circuitry"), comp(1,"Crystal Panel"));
        addBlock(ElementKeyMap.RACE_GATE_MODULE, comp(2, "Metal Frame"));

        addBlock(ElementKeyMap.ACTIVATION_GATE_CONTROLLER, comp(1, "Metal Frame"), comp(5, "Standard Circuitry"), comp(1,"Crystal Panel"));
        addBlock(ElementKeyMap.ACTIVATION_GATE_MODULE, comp(2, "Metal Frame"));

        addBlock(ElementKeyMap.WARP_GATE_CONTROLLER, comp(1, "Metal Frame"), comp(6, "Metal Sheet"), comp(1, "Crystal Panel"), comp(1, "Parsyne Holographic Processor"), comp(10, "Anbaric Distortion Coil"));
        addBlock(ElementKeyMap.WARP_GATE_MODULE, comp(1, "Metal Frame"), comp(1, "Anbaric Distortion Coil"));

        addBlock(ElementKeyMap.REPULSE_MODULE, comp(1, "Metal Frame"), comp(1, "Crystal Energy Focus"), comp(1, "Anbaric Distortion Coil"));
        addBlock(ElementKeyMap.LIFT_ELEMENT, comp(1, "Metal Frame"), comp(1, "Standard Circuitry")); //plex lift :\

        addBlock(671, comp(1, "Metal Frame"), comp(1, "Standard Circuitry")); //rail mass enhancer
        addBlock(672, comp(1, "Metal Frame"), comp(1, "Standard Circuitry"), comp(1, "Crystal Panel")); //rail speed controller

        addBlock(ElementKeyMap.GLASS_ID, comp(6, "Crystal Panel"), comp(1, "Metal Frame"));

        RRSRecipeAddEvent ev = new RRSRecipeAddEvent();
        fireEvent(ev, false);

        componentRecyclingRecipes.recipeProducts = componentRecyclingProducts.toArray(new FixedRecipeProduct[componentRecyclingProducts.size()]);
        blockDisassemblyRecipes.recipeProducts = blockDisassemblyProducts.toArray(new FixedRecipeProduct[blockDisassemblyProducts.size()]);
        System.err.println("[MOD][Resources ReSourced] Finished adding recipes for own blocks.");
    }

    public static FactoryResource fr(int count, int id) {
        return new FactoryResource(count, (short) id);
    }

    public static FactoryResource fr(int count, String name) {
        return new FactoryResource(count, elementEntries.get(name).id);
    }

    public static FactoryResource comp(int count, String name) {
        try {
            short id = elementEntries.get("Component: " + name).id;
            return new FactoryResource(count, id);
        } catch (NullPointerException oof) {
            System.err.println("[MOD][RRS][ERROR] Oof! Attempted to retrieve invalid component name \"" + name + "\" from index. That is to say: Ithirahad typed something wrong. :(");
            System.err.println("[MOD][RRS] Using fallback component, but please report this error.");
            oof.printStackTrace();
            return new FactoryResource(1, elementEntries.get("Component: Metal Frame").id);
        }
    }

    public static void addComponent(String name, int tier, FactoryResource... ingredients) {
        int bakeTime = 1;
        if (tier == 0) bakeTime = T0_BAKE_TIME;
        else if (tier == 1) bakeTime = T1_BAKE_TIME;
        else if (tier == 2) bakeTime = T2_BAKE_TIME;
        else if (tier == 3) bakeTime = T3_BAKE_TIME;
        ElementInformation product = elementEntries.get("Component: " + name);
        product.consistence.clear();
        product.setFactoryBakeTime(bakeTime);
        BlockConfig.addRecipe(product, COMPONENT_FACTORY_INDEX, bakeTime, ingredients);

        FixedRecipeProduct recyclingRecipe = new FixedRecipeProduct();
        recyclingRecipe.input = new FactoryResource[]{fr(1,product.id)};
        recyclingRecipe.output = ingredients;
        componentRecyclingProducts.add(recyclingRecipe);

        product.recalcTotalConsistence();
        hasComponentRecipe[product.id] = true;
        hasRRSRecipe[product.id] = true;
    }

    public static void addRefine(FactoryResource input, FactoryResource... outputs) {
        for(FactoryResource r : outputs){
            short t = r.type;
            hasRRSRecipe[t] = true;
            hasComponentRecipe[t] = true; //idfk, I don't think this is actually used
        }
        ElementKeyMap.getInfo(input.type).setFactoryBakeTime(CAPSULE_BAKE_TIME);
        BlockConfig.addRefineryRecipe(ElementKeyMap.capsuleRecipe, new FactoryResource[]{input}, outputs);
    }

    public static void addBlock(ElementInformation product, FactoryResource... ingredients) {
        product.consistence.clear();
        product.setFactoryBakeTime(BLOCK_BAKE_TIME);
        BlockConfig.addRecipe(product, BLOCK_FACTORY_INDEX, BLOCK_BAKE_TIME, ingredients);
        //TODO: calibrate shop base prices according to each recipe... plus some markup (50% or more) if in wrong zone with rare resource.
        // Will need to compile lists for this, probably a hashmap of arraylists per resource used?

        FixedRecipeProduct recyclingRecipe = new FixedRecipeProduct();
        recyclingRecipe.input = new FactoryResource[]{fr(1,product.id)};
        recyclingRecipe.output = ingredients;
        blockDisassemblyProducts.add(recyclingRecipe);

        product.recalcTotalConsistence();
        hasRRSRecipe[product.id] = true;
    }

    public static void addBlock(String name, FactoryResource... ingredients) {
        addBlock(elementEntries.get(name), ingredients);
    }

    public static void addBlock(int id, FactoryResource... ingredients) {
        addBlock(getInfo(id), ingredients);
    }

    public static ElementCountMap getConstituentComponents(ElementCountMap originalMap) {
        ElementCountMap resourceMap = new ElementCountMap();
        for (short blockID : ElementKeyMap.keySet) {
            short referenceElementID = blockID; //ID of reference block (e.g. full block for wedge, or root chamber for chamber)

            if(getInfo(referenceElementID).chamberRoot != 0) referenceElementID = (short) getInfo(referenceElementID).chamberRoot;
            if(getInfo(referenceElementID).sourceReference != 0) referenceElementID = (short) getInfo(blockID).sourceReference;

            //Kind of unfortunate that there's no way to just get at the ElementCountMap's included element IDs, because there's just a dumb array and no list of what's actually there. Oh well.
            int quantity = originalMap.get(blockID);
            if (quantity > 0 && ElementKeyMap.isValidType(referenceElementID)) {
                ElementCountMap componentsOfBlock;
                if(sameCatAs(getInfo(blockID), "Components")){
                    componentsOfBlock = new ElementCountMap();
                    int[] quantities = new int[ElementKeyMap.highestType + 1]; //Very sparse array with values corresponding to quantities, at positions corresponding to IDs.
                    int[] oreQuantities = new int[32];
                    //TODO: !!!!!!!!!!!!! Empty ore reqs might mean infinite NPC production. Need to override that faction behaviour or map these resources to NPC production
                    quantities[blockID] = originalMap.get(blockID);
                    componentsOfBlock.add(quantities,oreQuantities); //shouldn't drill down any further than components
                }
                else if (!ElementKeyMap.getInfo(referenceElementID).consistence.isEmpty())
                    componentsOfBlock = getElementCountMapFromRecipe(ElementKeyMap.getInfo(referenceElementID).getConsistence(), quantity);
                else {
                    int[] quantities = new int[ElementKeyMap.highestType + 1]; //Very sparse array with values corresponding to quantities, at positions corresponding to IDs.
                    int[] oreQuantities = new int[32];
                    //TODO: !!!!!!!!!!!!! Empty ore reqs might mean infinite NPC production. Need to override that faction behaviour or map these resources to NPC production
                    quantities[referenceElementID] = originalMap.get(blockID);
                    componentsOfBlock = new ElementCountMap();
                    componentsOfBlock.add(quantities, oreQuantities);
                    //fallback: just keep the original block if no consistence info available
                }
                resourceMap.add(componentsOfBlock);
            }
        }
        return resourceMap;
    }

    public static ElementCountMap getElementCountMapFromRecipe(List<FactoryResource> source, int quantity) {
        ElementCountMap result = new ElementCountMap();
        int[] quantities = new int[ElementKeyMap.highestType + 1]; //Very sparse array with values corresponding to quantities, at positions corresponding to IDs.
        int[] oreQuantities = new int[32];
        //TODO: !!!!!!!!!!!!! Empty ore reqs might mean infinite NPC production. Need to override that faction behaviour or map these resources to NPC production
        for (FactoryResource resource : source) {
            quantities[resource.type] += (resource.count * quantity);
        }
        result.add(quantities, oreQuantities);
        return result;
    }

    public static void addModFallbackRecipes() {
        int rockDust = elementEntries.get("Rock Dust Capsule").id;
        int plantCrap = elementEntries.get("Floral Protomaterial Capsule").id;

        ArrayList<ElementInformation> modElements = getElements();

        for(ElementInformation item : modElements){
            short id = item.id;
            if(id > infoArray.length){
                System.err.println("[MOD][Resources ReSourced][WARNING] Picked up mod block element information \""+item.name+"\" with configured ID greater than info array length. ID: " + id + "; Highest Array Position: " + (infoArray.length - 1));
                if( GameClientState.instance != null) ResourcesReSourced.displayBrokenBlocksWarning = true;
            }
            if(!hasRRSRecipe[id]
                && (GameServerState.instance != null || id < infoArray.length) //Purposefully allow servers to pass this check no matter what and crash for their own protection
            )
                {
                item = getInfo(id);
                if(
                    !item.deprecated &&
                    item.consistence.size() != 0 && //if the consistence is empty it presumably was not supposed to be crafted in the first place.
                    item.sourceReference == 0 && //nonzero source reference implies it's a derivative block, e.g. a hull shape
                    item.chamberRoot == 0//ditto for a nonzero chamber root; chamber selections cannot be crafted
                )
                {
                    ArrayList<FactoryResource> ingredients = new ArrayList<>();

                    assert lightCategory != null;
                    assert ingotCategory != null;
                    assert terrainCategory != null;
                    assert basicArmorCategory != null;
                    assert standardArmorCategory != null;
                    assert advancedArmorCategory != null;

                    //These are only useful assuming the mod author bothers to give their blocks the appropriate category,
                    // which unfortunately is likely to not be the case oftentimes. Oh well. It's the first and easiest thing to try.

                    ElementCategory parent = getParentCat(item.type);
                    if (parent != null) {
                        if (decoCategory.equals(item.type) || isParentCat(decoCategory.getCategory(), item)) {
                            addBlock(item, comp(1, "Metal Frame"));
                            continue;
                        } else if (basicArmorCategory.equals(parent)) { //Why did I do the .equals backwards? No clue, but I'm keeping it :D
                            addBlock(item, comp(6, "Metal Sheet"), comp(1, "Omnichrome Paint"));
                            continue;
                        } else if (standardArmorCategory.equals(parent)) {
                            addBlock(item, comp(6, "Metal Sheet"), comp(1, "Macetine Alloy Plating"), comp(1, "Omnichrome Paint"));
                            continue;
                        } else if (advancedArmorCategory.equals(parent)) {
                            if (item.name.contains("Crystal"))
                                addBlock(item, comp(6, "Crystal Panel"), comp(2, "Macetine Alloy Plating"), comp(1, "Omnichrome Paint"));
                            else
                                addBlock(item, comp(6, "Metal Sheet"), comp(2, "Macetine Alloy Plating"), comp(1, "Omnichrome Paint"));
                            continue;
                        } else if (sameCatAs(item, lightCategory) || isSiblingCatTo(item, lightCategory.getCategory())) { //either a light or a light bar
                            addBlock(item, comp(1, "Metal Frame"), comp(1, "Crystal Light Source"));
                            continue;
                        } else if (ingotCategory.equals(item.type)) {
                            if (item.name.contains("Ingot")) {
                                addBlock(item, comp(1, "Metal Sheet"), comp(1, "Omnichrome Paint")); //...else? check this
                                continue;
                            }
                        } else if (terrainCategory.equals(item.type)) {
                            if (item.name.contains("Crystal"))
                                addBlock(item, fr(1, "Rock Dust Capsule"), fr(1, CRYSTAL_CRIRCUITS));
                            else addBlock(item, fr(1, "Rock Dust Capsule"));
                            continue;
                        } else if (logicCategory.equals(item.type)) {
                            addBlock(item, comp(1, "Metal Frame"), comp(1, "Standard Circuitry"));
                            continue;
                        } else if (railCategory.equals(item.type)) {
                            addBlock(item, comp(2, "Macetine Magnetic Rail"), comp(1, "Metal Frame"));
                            continue;
                        } else if (plantCategory.equals(item.type)) {
                            addBlock(item, fr(1, plantCrap));
                            continue;
                        } else if (carvedStoneCategory.equals(item.type)) {
                            addBlock(item, fr(1, rockDust), comp(1, "Omnichrome Paint"));
                            continue;
                        }
                    }
                    else {
                        /*String name = item.getNameUntranslated().toLowerCase();
                        //now we start with name-checking conditions...
                        if () {

                        } else */ for (FactoryResource vanillaIngredient : item.consistence)
                            ingredients.add(translateToComponents(ElementKeyMap.getInfo(id).getNameUntranslated(), vanillaIngredient));
                    }
                    if(ingredients.size() /*still*/ == 0) ingredients.add(comp(8, "Metal Frame")); //it's probably a block, so it has eight edges, right? :P
                    //ultimate fallback for someone's super janky block with no recipe

                    FactoryResource[] finalIngredients = ingredients.toArray(new FactoryResource[ingredients.size()]);
                    addBlock(id,finalIngredients);
                }
            }
        }
        componentRecyclingRecipes.recipeProducts = componentRecyclingProducts.toArray(new FixedRecipeProduct[componentRecyclingProducts.size()]);
        blockDisassemblyRecipes.recipeProducts = blockDisassemblyProducts.toArray(new FixedRecipeProduct[blockDisassemblyProducts.size()]);
        //first pass is technically redundant, but eh.
        System.err.println("[MOD][Resources ReSourced] Finished adding recipes for mod blocks.");
    }

    private static FactoryResource translateToComponents(String elementName, FactoryResource orig) {
        FactoryResource result = null;
        String name = elementName.toLowerCase();

        if(orig.type == ElementKeyMap.CRYSTAL_CRIRCUITS){
            //depends on count
            if(name.contains("light")) result = comp(1, "Crystal Light Source");
            else if(name.contains("power") || name.contains("energy")) result = comp(1, "Crystal Power Cell");
            else if(name.contains("beam") || name.contains("projector")) result = comp(1, "Crystal Energy Focus");
            else if(orig.count == 1 || name.contains("gravity")) result = comp(1, "Standard Circuitry");
            else if(orig.count < 10) result = comp(1, "Crystal Panel");
            else result = comp(4, "Crystal Panel");
        }
        else if(orig.type == ElementKeyMap.METAL_MESH){
            //ditto
            if(orig.count == 1) result = comp(1, "Metal Frame");
            else if(orig.count < 10) result = comp(1, "Metal Sheet");
            else result = comp(6, "Metal Sheet");
        }
        else if(orig.type == 866){
            result = comp(1, "Macetine Alloy Plating");
        }
        else if(orig.type == 867){
            result = comp(2, "Macetine Alloy Plating");
        }
        else if(name.contains("paint")){
            result = comp(1, "Omnichrome Paint");
        }
        else {
            ShortArrayList capsuleIDs = new ShortArrayList();
            for(ElementInformation info : oldCapsules) capsuleIDs.add(info.id);
            //TODO: doing this every time is kind of slow, not that it really matters here in the init cycle

            if(capsuleIDs.contains(orig.type)){
                switch(orig.type){
                    case 178: //Fertikeen, hulls and pulses
                        result = comp(Math.max(1,orig.count / 50), "Aegium Field Emitter");
                        break;
                    case 186: //Bastyn, race gates and EMP
                        if(orig.count <= 10) result = comp(1, "Macetine Magnetic Rail");
                        else result = comp(Math.max(1,orig.count / 50),"Aegium Field Emitter");
                        break;
                    case 162: //Sertise, shield manipulation
                        result = comp(Math.max(1,orig.count / 50), "Aegium Field Emitter");
                        break;
                    case 174: //Rammet, shields
                        if(orig.count >= 200) result = comp(Math.max(4,orig.count/50),"Anbaric Distortion Coil");
                        else result = comp(Math.max(1,orig.count/50),"Parsyne Amplifying Focus");
                        break;
                    case 150: //Jisper, advanced rails, tractor beams, and formerly power manipulation
                        result = comp(Math.max(1,orig.count/25), "Macetine Magnetic Rail");
                        break;
                    case 146: //Sintyr, rails
                        result = comp(Math.max(1,orig.count/50), "Macetine Magnetic Rail");
                        break;
                    case 154: //original Threns, missiles, shipyards, scanners, missile capacity
                        result = comp(Math.max(1,orig.count/50),"Thermyn Power Charge");
                        //weird for shipyards and scanners, but honestly I'm putting more effort into this
                        // than modders probably will for their vanilla recipes
                        break;
                    case 182: //Hattel, faction modules, scanners, and security
                        result = comp(Math.max(1,orig.count/10), "Ferron Resonant Circuitry");
                        break;
                    case 158: //Sapsun, recon chambers, shield regen, and astrotech
                        result = comp(Math.max(1,orig.count/50),"Parsyne Amplifying Focus");
                        break;
                    case 166: //Parseen, recon chambers and shield rechargers
                        result = comp(Math.max(1,orig.count / 50), "Ferron Resonant Circuitry");
                        break;
                    case 202: //Macet, FTL
                    case 198: //Nocx, FTL and gravity
                        result = comp(Math.max(1,orig.count/50), "Anbaric Distortion Coil");
                        break;
                    case 170: //Zercaner, AI, beams, and formerly kinetics
                        result = comp(Math.max(1,orig.count/100), "Parsyne Holographic Processor");
                        break;
                    case 194: //Mattise, ditto (also FTL interdictors)
                        result = comp(Math.max(1,orig.count/50), "Anbaric Distortion Coil");
                        break;
                    default:
                        result = comp(Math.max(1,orig.count/50), "Metal Frame");
                }
            }
        }
        if(result == null) result = comp(1, "Metal Frame");
        return result;
    }
}
