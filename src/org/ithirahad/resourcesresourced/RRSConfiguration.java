package org.ithirahad.resourcesresourced;

import api.common.GameCommon;
import org.schema.game.server.data.ServerConfig;

public class RRSConfiguration {
    public static boolean DEBUG_LOGGING = false;
    public static final String CONFIG_NAME_STEM = "Resources ReSourced - World";

    //----VISUAL/UI
    public static float MAP_ZONE_COLOUR_ALPHA = 0.35f; //transparency of zone map marking colours. Probably does nothing at the moment.
    public static boolean USE_MAP_FOW = false; //respect vanilla Fog of War when drawing. Not currently implemented.

    //----WORLD
    public static boolean FILTER_ROGUE_VOIDS_BUG_FOR_ALL_SYSTEMS = true; //suppress "rogue" voids that make it into the list somehow. affects galaxy gen

    public static boolean USE_GALAXY_CORE_REGION = true; //whether or not to create galactic core regions. These are special regions with small amounts of everything, at the center of a galaxy.
    public static float GALAXY_CORE_RADIUS = 5; //radius of core region.

    public static float MID_ZONE_TYPE_START = 0.25f;
    public static float FAR_ZONE_TYPE_START = 0.55f;

    public static int MAJOR_RESOURCE_ZONES_PER_TYPE = 3; //how many zones to generate per standard zone type
    public static boolean USE_MINOR_ZONES;
    public static int MINOR_RESOURCE_ZONES_PER_TYPE = 6;

    public static int MAJOR_ZONE_MIN_STAR_COUNT = 7; //minimum number of stars allowed in a full-size resource zone; lower than this will force regeneration elsewhere in the galaxy
    public static int MAJOR_ZONE_MAX_STAR_COUNT = 30;

    public static int MINOR_ZONE_MIN_STAR_COUNT = 2;
    public static int MINOR_ZONE_MAX_STAR_COUNT = 4;
    // TODO: max stars based on zone type
    public static float ZONE_STAR_LOOK_RADIUS_INITIAL = 3f; //From a randomly-selected center star, how far to look for stars to add to a resource zone?
    public static float ZONE_STAR_LOOK_RADIUS_SECONDARY = 2.5f; //From a randomly-selected star already in the zone, how far to look for further stars to add? //do we need/want this?
    public static float MINOR_ZONE_LOOK_RADIUS = 2.5f;
    // ...maybe Moar Void should come with its own defaults for this. When there are so many non-star systems you need much larger look radii for zones.
    public static float SEARCH_RADIUS_MODIFIER_FOR_DISTANCE_FROM_GALACTIC_CENTRE = 2f; //Modifier factor for the size of the look zone based on distance from the galactic centre
    public static int ZONE_SEARCH_BLOBS_MIN = 2; //How many search iterations to run (excl. center) when making a zone
    public static int ZONE_SEARCH_BLOBS_MAX = 4;
    public static float ASTEROID_PLANET_FROSTLINE = 0.4f; //beyond this point, cold asteroids/planets can spawn
    public static float ASTEROID_PLANET_FIRELINE = 0.7f; //beyond this point, hot asteroids/planets can spawn

    public static float MAX_SYSTEM_RESOURCE_DENSITY = 1f;
    public static float MIN_BARRENS_SYSTEM_RESOURCE_DENSITY = 0.1f; //might be kinda cruel :D
    public static float MIN_ZONE_SYSTEM_RESOURCE_DENSITY = 0.4f;

    public static float SPECIAL_PLANET_RATE_BASE = 0.25f; //provided other spawning conditions are met (distance from star, etc.) chance that a planet in a resource zone will be the corresponding type.
    // Does not apply to Anbaric giants or Mist planets overall.

    public static float ACTIVE_POOL_REGEN_MODIFIER = 0.25f;
    public static float ACTIVE_POOL_SIZE_MODIFIER = 720;

    public static float MINOR_ZONE_PLANET_RESOURCE_MODIFIER = 0.5f; // minor zone resource density is multiplied by this
    public static float MINOR_ZONE_SPECIAL_PLANET_MODIFIER = 2f;
    public static float CORE_ZONE_SPECIAL_PLANET_MODIFIER = 0.2f; //multiplier for chance of core zones spawning special planet types of any kind

    public static float GAUSSIAN_PASSIVE_RESOURCE_BONUS_CAP = 9f; //sanity cap for bonus multiplier
    public static float GAUSSIAN_PASSIVE_RESOURCE_BONUS_SIGMA = 0.5f; //standard deviation width of bonus multiplier

    public static float DENSE_ASTEROID_RESOURCE_CHANCE = 0.003f; //resource spawn rate in dense asteroids
    public static float STANDARD_ASTEROID_RESOURCE_CHANCE = 0.00125f; //resource spawn rate in normal or rare material asteroids

    public static float OFFTYPE_ASTEROID_CHANCE_MIN = 0.01f;
    public static float OFFTYPE_ASTEROID_CHANCE_MAX = 0.075f;

    public static float OFFTYPE_PLANET_CHANCE_MIN = 0.02f;
    public static float OFFTYPE_PLANET_CHANCE_MAX = 0.1f;

    public static float OFFTYPE_CHROMA_CHANCE = 0.005f; //chance that an off-type asteroid will be a chroma asteroid

    public static float GAS_GIANT_MIN_RADIUS = 0.6f;
    public static float GAS_GIANT_MAX_RADIUS = 1.25f;
    public static float GAS_GIANT_MIN_ROTATION = 0.8f;
    public static float GAS_GIANT_MAX_ROTATION = 0.9f;

    public static float GIANT_GRAVITY_FACTOR = 2.5f;
    public static float FLOOR_REPEL_FORCE_FACTOR = 100f; //Force that shoves you out of the depths
    public static float DRAG_FORCE_MULTIPLIER = 0.4f; //Percent of entity velocity(?). This is at max depth; it drops off at higher altitudes. Technically should be some sort of fancy function, but it's linear because this is not KSP

    public static float NAME_GENERIC_ADJECTIVE_CHANCE = 0.75f; //out of 1. how likely to use a generic (non ore based) zone name
    public static float NAME_PREFIX_CHANCE = 0.1f; //out of 1. how likely to append a prefix if conditions are met

    //----RESOURCES
    public static float RSC_ANBARIC_MIN_REGEN_RATE_SEC = 0.5f;
    public static float RSC_ANBARIC_MAX_REGEN_RATE_SEC = 1.5f;

    public static float RSC_PARSYNE_MIN_REGEN_RATE_SEC = 0.5f;
    public static float RSC_PARSYNE_MAX_REGEN_RATE_SEC = 2f;
    public static float RSC_PARSYNE_PLANET_MOD = 0.05f; //fraction of star output range used by Parsyne gas giants (keeping in mind that stars would still be outputting!)

    public static float RSC_THERMYN_MIN_REGEN_RATE_SEC = 0.1f;
    public static float RSC_THERMYN_MAX_REGEN_RATE_SEC = 1.0f;

    //Note that these are BASE rates - subject to Gaussian modifiers.

    public static float EXTRACTION_RATE_PER_SECOND_PER_BLOCK = 0.002f;
    public static long PASSIVE_POOL_UPDATE_INTERVAL_MS = 5000; //passive resource pool update rate

    public static float REFINING_MACETINE_BYPRODUCT_RATE = 0.05f; //how much Macetine is created per other resource
    public static float COMPONENT_RECYCLE_FAIL_RATE = 0.5f; //failure will sometimes yield scrap metal/composite

    //----SYSTEMS
    public static float ASTROMETRIC_SCANNER_CHARGE_TIME = 1.5f;
    public static float ASTROMETRIC_SCANNER_CHARGE_POWER_PER_MASS = 10.0f; //honestly just a token value to make it feel like it interacts with the ship

    public static float RAMSCOOP_HARVEST_PER_BLOCK = 1f;
    public static float GAS_MINER_CHAMBER_BONUS_1 = 1.5f;
    public static float GAS_MINER_CHAMBER_BONUS_2 = 2f; //non stacking
    public static float RAMSCOOP_MAX_BONUS_FROM_SPEED = 9f; //bonus value at max server speed. total multiplier is 1 + this.

    public static String getConfigName(){
        String identifier = GameCommon.isClientConnectedToServer() ? GameCommon.getUniqueContextId() : (String) ServerConfig.WORLD.getCurrentState();
        return CONFIG_NAME_STEM + " --- " + identifier;
    }
}
