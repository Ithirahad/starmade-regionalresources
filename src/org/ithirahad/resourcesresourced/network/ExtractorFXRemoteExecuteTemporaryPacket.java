package org.ithirahad.resourcesresourced.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.ithirahad.resourcesresourced.vfx.ExtractorPulseEffect;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.io.IOException;

public class ExtractorFXRemoteExecuteTemporaryPacket extends Packet {
    int sectorId;
    Vector3i sector;
    Vector3f startPosition;
    Vector3f endPosition;
    Vector4f color;

    public ExtractorFXRemoteExecuteTemporaryPacket(){
        super();
    }

    public ExtractorFXRemoteExecuteTemporaryPacket(int sectorID, Vector3i loc, Vector3f start, Vector3f end, Vector4f resourceColor){
        sectorId = sectorID;
        sector = loc;
        startPosition = start;
        endPosition = end;
        color = resourceColor;
    }

    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        sectorId = b.readInt();
        sector = b.readVector();
        startPosition = b.readVector3f();
        endPosition = b.readVector3f();
        color = b.readVector4f();
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        b.writeInt(sectorId);
        b.writeVector(sector);
        b.writeVector3f(startPosition);
        b.writeVector3f(endPosition);
        b.writeVector4f(color);
    }

    @Override
    public void processPacketOnClient() {
        ExtractorPulseEffect.fireEffectClient(sectorId, sector, startPosition, endPosition, color);
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        //what
    }
}
