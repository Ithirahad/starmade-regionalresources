package org.ithirahad.resourcesresourced.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.ithirahad.resourcesresourced.vfx.RamscoopEffect;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.io.IOException;

public class RamscoopFXExecutePacket extends Packet {
    int sectorId;
    Vector3i sector;
    Vector3f blockPosition;
    Vector3f direction;
    Vector3f velocity;
    Vector4f color;
    float strength;
    float durationSeconds;
    private int entity;

    public RamscoopFXExecutePacket() {
        super();
    }

    public RamscoopFXExecutePacket(int sectorID, int entityID, Vector3i loc, Vector3f pos, Vector3f dir, Vector3f vel, float durationSeconds, float power, Vector4f effectColor) {
        sectorId = sectorID;
        sector = loc;
        blockPosition = pos;
        direction = dir;
        velocity = vel;
        color = effectColor;
        strength = power;
        entity = entityID;
    }

    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        sectorId = b.readInt();
        entity = b.readInt();
        sector = b.readVector();
        blockPosition = b.readVector3f();
        direction = b.readVector3f();
        velocity = b.readVector3f();
        durationSeconds = b.readFloat();
        strength = b.readFloat();
        color = b.readVector4f();
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        b.writeInt(sectorId);
        b.writeInt(entity);
        b.writeVector(sector);
        b.writeVector3f(blockPosition);
        b.writeVector3f (direction);
        b.writeVector3f(velocity);
        b.writeFloat(durationSeconds);
        b.writeFloat(strength);
        b.writeVector4f(color);
    }

    @Override
    public void processPacketOnClient() {
        RamscoopEffect.FireEffectClient(sectorId, entity, sector, blockPosition, direction, velocity, durationSeconds, strength, color);
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        //why
    }
}