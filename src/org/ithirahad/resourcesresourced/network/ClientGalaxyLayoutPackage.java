package org.ithirahad.resourcesresourced.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.schema.game.common.data.player.PlayerState;

import java.io.IOException;

public class ClientGalaxyLayoutPackage extends Packet {
    //todo: provision for client 'dummy' regions - type UNSCANNED and SCANNED - that provide no specifics (e.g. zone type or ore concentrations) but preserve the name, skybox color, and planet positions/types
    //todo: persistent serverside list of regions client knows about. Add region UIDs?

    @Override
    public void readPacketData(PacketReadBuffer packetReadBuffer) throws IOException {
        //todo: write to galaxy map
    }

    @Override
    public void writePacketData(PacketWriteBuffer packetWriteBuffer) throws IOException {

    }

    @Override
    public void processPacketOnClient() {

    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {

    }
}
