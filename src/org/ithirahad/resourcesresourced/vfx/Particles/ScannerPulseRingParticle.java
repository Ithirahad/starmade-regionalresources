package org.ithirahad.resourcesresourced.vfx.Particles;

import api.utils.particle.ModParticle;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

public class ScannerPulseRingParticle extends ModParticle {
    Vector4f startColor = new Vector4f(1,1,1,0.5F);
    Vector4f endColor = new Vector4f(0.1F,0.6F,0.8F,0F); //fade to transparency
    private final float startSize;
    private final float endSize;
    public ScannerPulseRingParticle(float startSize, float endSize, Vector3f pos) {
        this.startSize = startSize;
        this.endSize = endSize;
        this.position = pos;
        this.lifetimeMs = 6000;
    }

    @Override
    public void spawn() {
        this.endColor.x = colorR/255f;
        this.endColor.y = colorG/255f;
        this.endColor.z = colorB/255f;
    }

    @Override
    public void update(long currentTime) {
        colorOverTime(this, currentTime, startColor, endColor);
        sizeOverTime(this, currentTime, startSize, endSize);
        this.sizeX += sizeX * getLifetimePercent(currentTime) * 0.3;
        this.sizeY += sizeY * getLifetimePercent(currentTime) * 0.3;
    }

}
