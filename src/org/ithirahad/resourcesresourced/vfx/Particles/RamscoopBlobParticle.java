package org.ithirahad.resourcesresourced.vfx.Particles;

import api.utils.particle.ModParticle;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.ithirahad.resourcesresourced.vfx.RamscoopEffect;
import org.schema.common.FastMath;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.network.objects.container.TransformTimed;

import javax.vecmath.Vector3f;

import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.lerp;
import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.smoothstep;

public class RamscoopBlobParticle extends ModParticle {
    private static final Vector3f V3F_ZERO = new Vector3f(0,0,0);
    private final float startSize;
    private final float endSize;
    private final boolean orientToEnd;
    float tPct;
    private final Vector3f localPosLinear; //linear approach progress from straight-ahead point to nozzle
    private final Vector3f localPosConical; //linear approach progress from offset point to nozzle
    private final Vector3f localStartWithRandomOffset;
    private final Vector3f targetBlockPos;
    private final Vector3f localStartLinear;
    private Vector3f firingDirection; //where's the nozzle pointed
    private final TransformTimed parent;
    private float initialRotation = 0;
    private boolean firstUpdate = true;
    private final Vector3f localTruePosition; //position relative to the harvester output.

    Vector3f lookAt = new Vector3f();

    //TODO: TEMPORARY
    public RamscoopBlobParticle(float startSize, float endSize, Vector3f startWithOffset, Vector3f startWithoutOffset, SegmentController targetEntity, Vector3f targetBlockPosition, Vector3f direction, boolean orientToGoal) {
        this.startSize = startSize;
        this.endSize = endSize;
        this.localStartWithRandomOffset = new Vector3f(startWithOffset);
        this.localTruePosition = new Vector3f(startWithOffset);
        this.localStartLinear = new Vector3f(startWithoutOffset);
        this.localPosConical = new Vector3f(localTruePosition);
        this.localPosLinear = new Vector3f(localStartLinear);
        this.parent = targetEntity.getWorldTransform(); //moves with ship, or should
        this.targetBlockPos = new Vector3f(targetBlockPosition);
        this.orientToEnd = orientToGoal;
        this.startTime = System.currentTimeMillis();
    }

    @Override
    public void spawn() {

    }

    @Override
    public void update(long currentTime) {
        sizeOverTime(this, currentTime, startSize, endSize);
        tPct = getLifetimePercent(currentTime);
        colorA = (byte) (127 * smoothstep(0,0.75f, FastMath.pow(tPct,2.5f))); //ramping fade in
        localPosLinear.set(lerp(localStartLinear, V3F_ZERO,FastMath.pow(tPct,0.3f)));
        localPosConical.set(lerp(localStartWithRandomOffset, V3F_ZERO,FastMath.pow(tPct,0.3f)));
        //MiscUtils.smoothstep(localPosConical, localPosLinear, tPct, localTruePosition); //change to lerping by the the sqrt of fractional lifetime?

        localTruePosition.set(lerp(localPosConical,localPosLinear,tPct));

        localTruePosition.scale(10);
        localTruePosition.sub(RamscoopEffect.originOffset); //transform.origin seems to be at block 16,16,16, not 0,0,0

        position.set(localTruePosition);
        position.add(targetBlockPos); //output block offset
        parent.basis.transform(position); //rotate to match ship

        position.add(parent.origin); //world-space offset

        //lookAt.scale(-1);
        lookAt.normalize();

        if(orientToEnd){
            lookAt.set(localTruePosition);
            lookAt.sub(localPosLinear);
            parent.basis.transform(lookAt);
            //if this works, it should make the particle's normals point towards the theoretical linear path - meaning that the edge should always point toward the endpoint.
            setArbitraryNormalOverride(lookAt);
        }
    }

}
