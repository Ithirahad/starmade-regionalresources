package org.ithirahad.resourcesresourced.vfx;

import api.common.GameServer;
import api.network.packets.PacketUtil;
import com.bulletphysics.linearmath.QuaternionUtil;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import org.ithirahad.resourcesresourced.RRSConfiguration;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.ithirahad.resourcesresourced.RRUtils.RealTimeDelayedAction;
import org.ithirahad.resourcesresourced.network.RamscoopFXExecutePacket;
import org.ithirahad.resourcesresourced.vfx.Particles.RamscoopBlobParticle;
import org.lwjgl.util.vector.Quaternion;
import org.schema.common.util.linAlg.Quaternion4f;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.network.objects.Sendable;

import javax.vecmath.*;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.*;
import static org.ithirahad.resourcesresourced.shipsystems.gasharvester.GasHarvesterElementManager.HARVEST_SAMPLE_RATE_MS;
import static org.ithirahad.resourcesresourced.vfx.ExtractorPulseEffect.nextSignedFloat;
import static org.lwjgl.util.vector.Matrix4f.rotate;

public class RamscoopEffect {
    private static final float velocityScalar = 0.05f;
    public static final Vector3f originOffset = new Vector3f(16,16,16);
    private static final Vector3f forwardDir = new Vector3f(0,0,1);


    public static void FireEffectClient(final int sectorId, final int entity, Vector3i sector, final Vector3f blockPosition, final Vector3f worldOrientation, final Vector3f velocity, float durationSeconds, final float powerMultiplier, final Vector4f color){
        final long durationMs = (long) (HARVEST_SAMPLE_RATE_MS * 2.5);

        final SegmentController parentShip;
        SegmentController tempSeg;
        try{
            Int2ObjectOpenHashMap<Sendable> locals = GameClientState.instance.getLocalAndRemoteObjectContainer().getLocalObjects();
            Sendable s = locals.get(entity);
            tempSeg = (SegmentController) s;
        }
        catch(Exception ex){
            ex.printStackTrace();
            System.err.println("[MOD][Resources ReSourced] Could not play effect!");
            return;
        }

        parentShip = tempSeg;
        final Vector3f localOrientation = new Vector3f(worldOrientation);

        if(parentShip != null) {
            Matrix3f inversion = new Matrix3f(parentShip.getWorldTransform().basis);
            inversion.invert();
            inversion.transform(localOrientation); //transform shot direction

            new RealTimeDelayedAction(10, -1, true){

                int density;
                int moteLifetimeMs;
                float speedMult;
                final float offsetScale = min(5000f,20f + (powerMultiplier/24)); //more than 5km of particle effect is probably not healthy
                final long endTime = System.currentTimeMillis() + durationMs;
                Vector3f localStart;
                Vector3f localLinearStart;
                boolean swap = false;
                final List<RamscoopBlobParticle> blobs = new LinkedList<>();

                @Override
                public void doAction() {
                    long now = System.currentTimeMillis();
                    if(now >= endTime) cancel();
                    else {
                        moteLifetimeMs = (int) lerp(1500,500, speedMult,true);
                        speedMult = getSpeedFraction(parentShip);
                        density = (int) lerp(3,40,speedMult/5,true); //no more than 40 particles per wave
                        for(int i = 0; i < density; i++) {
                            Vector3f offset = new Vector3f();
                            offset.set(nextSignedFloat(), nextSignedFloat(), nextSignedFloat());
                            offset.normalize();
                            offset.z *= 2; //mitigating 'ring effect' issues as particles approach the nozzle
                            offset.scale(offsetScale);

                            localStart = new Vector3f(0,0,offsetScale * 5f); //relative to block pos

                            localLinearStart = new Vector3f(localStart);
                            localStart.add(offset);
                            localLinearStart.z = localStart.z; //prevent layer synching as particles get closer to linear motion

                            //TODO: rotate/coord swap these depending on shot orientation...

                            if (swap)
                                blobs.add(ParticleEffectsManager.spawnRamscoopMote(sectorId, parentShip, blockPosition, localOrientation, localLinearStart, localStart, color, moteLifetimeMs, 15, powerMultiplier/500f));
                            else
                                ParticleEffectsManager.spawnRamscoopSliver(sectorId, parentShip, blockPosition, localOrientation, localLinearStart, localStart, color, 500);
                            swap = !swap;
                        }

                        for (Iterator<RamscoopBlobParticle> iterator = blobs.iterator(); iterator.hasNext(); ) {
                            RamscoopBlobParticle blob = iterator.next();
                            if(now - blob.startTime >= blob.lifetimeMs) iterator.remove(); //dereference to avoid memory leak
                            else blob.lifetimeMs = moteLifetimeMs; //change speed/lifetime. 10ms might feel a bit juddery, but tbh it probably isn't noticeable outside of crazy accelerations
                        }
                    }
                }
            }.startCountdown();
        }
    }

    public static void FireHarvestEffectServer(final int sectorId, int entityID, Vector3i sector, final Vector3f outputBlockPosition, final Vector3f direction, final Vector3f velocity, float durationSeconds, float powerMultiplier, Vector4f resourceColor){
        RamscoopFXExecutePacket packet = new RamscoopFXExecutePacket(sectorId, entityID, sector, outputBlockPosition, direction, velocity, durationSeconds, powerMultiplier, resourceColor);
        Vector3i delta = new Vector3i();
        for(PlayerState player : GameServer.getServerState().getPlayerStatesByDbId().values()) if(player != null) {
            delta.set(player.getCurrentSector());
            delta.sub(sector);
            if (delta.lengthSquared() <= 4) PacketUtil.sendPacket(player, packet);
        }
    }

    private static float getSpeedFraction(SegmentController s){
        float speedFrac = s.getSpeedCurrent()/s.getMaxServerSpeed();
        //TODO: multiply by max(0,dot product of normalized harvest vector and flight vector)
        speedFrac = min(speedFrac,5.0f); //sanity check; wormhole accel shouldn't cause particle spam any more than it should let you one-tap a whole gas giant
        return speedFrac * RRSConfiguration.RAMSCOOP_MAX_BONUS_FROM_SPEED;
    }
}
