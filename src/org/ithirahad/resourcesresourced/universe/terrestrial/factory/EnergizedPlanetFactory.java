package org.ithirahad.resourcesresourced.universe.terrestrial.factory;

import org.ithirahad.resourcesresourced.universe.terrestrial.TerrestrialPlanetGroundType;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.controller.world.factory.terrain.GeneratorFloraPlugin;
import org.schema.game.server.controller.world.factory.terrain.TerrainDeco;

import javax.vecmath.Vector3f;
import java.util.Random;

public class EnergizedPlanetFactory extends RRSPlanetFactory {

    public EnergizedPlanetFactory(long seed, Vector3f[] polygons, float radius, TerrestrialPlanetGroundType materials){
        super(seed, polygons, radius, materials);

        this.minable = new TerrainDeco[4];
        this.minable[0] = new GeneratorFloraPlugin(ElementKeyMap.TERRAIN_GLOW_TRAP_SPRITE, this.getTop(), this.getFiller());
        this.minable[1] = new GeneratorFloraPlugin(ElementKeyMap.TERRAIN_ROCK_SPRITE, this.getTop(), this.getFiller());
        this.minable[2] = new GeneratorFloraPlugin(ElementKeyMap.TERRAIN_YHOLE_PURPLE_SPRITE, this.getTop(), this.getFiller());
        this.minable[3] = new GeneratorFloraPlugin(ElementKeyMap.TERRAIN_ICE_CRAG_SPRITE, this.getTop(), this.getFiller());
    }

    @Override
    public void createAdditionalRegions(Random random) {
    //wish I had any clue as to how this would work, but I don't.
    }
}
