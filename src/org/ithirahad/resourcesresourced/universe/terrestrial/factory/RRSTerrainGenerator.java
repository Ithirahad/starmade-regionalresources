package org.ithirahad.resourcesresourced.universe.terrestrial.factory;

import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.schema.common.FastMath;
import org.schema.game.common.data.Dodecahedron;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.SegmentDataInterface;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.RequestDataPlanet;
import org.schema.game.server.controller.world.factory.WorldCreatorPlanetFactory;
import org.schema.game.server.controller.world.factory.regions.Region;
import org.schema.game.server.controller.world.factory.terrain.TerrainGenerator;

import java.util.List;

/**
 * Just an ugly hack in order to fix a strange bug (or bugs) without dealing with class replacement. None of this code is really mine.
 */

public class RRSTerrainGenerator extends TerrainGenerator {
    private final static int HEIGHT_BLOCKS = plateauHeight * 16;

    public RRSTerrainGenerator(long seed) {
        super(seed);
    }

    private boolean isPointInDodeca(int dx, int dz, int x, int z, RequestDataPlanet requestData){
        return Dodecahedron.pnpoly(((WorldCreatorPlanetFactory) MiscUtils.readSuperclassPrivateField(TerrainGenerator.class,"worldCreator", this)).poly,
                margin(dx + (15 - x)), margin(dz + (15 - z)), requestData.getR().mar);
    }
    private boolean isSegmentInDodeca(int xPos, int zPos, int dx, int dz, RequestDataPlanet requestData){

        return
                isPointInDodeca(dx, dz, 0, 0, requestData) ||
                        isPointInDodeca(dx, dz, 0, 15, requestData) ||
                        isPointInDodeca(dx, dz, 15, 15, requestData) ||
                        isPointInDodeca(dx, dz, 15, 0, requestData);



    }
    private boolean isSegmentAllInDodeca(int xPos, int zPos, int dx, int dz, RequestDataPlanet requestData){

        return
                isPointInDodeca(dx, dz, 0, 0, requestData) &&
                        isPointInDodeca(dx, dz, 0, 15, requestData) &&
                        isPointInDodeca(dx, dz, 15, 15, requestData) &&
                        isPointInDodeca(dx, dz, 15, 0, requestData);



    }

    @Override
    public void decorateWithBlockTypes(int xPos, int yPos, int zPos,
                                       short informationArray[], SegmentDataInterface data,
                                       RequestDataPlanet requestData) throws SegmentDataWriteException {


        // byte byte0 = 63;
        // double d = 0.03125D;
        int dx = (64 - xPos) * 16;
        int dz = (64 - zPos) * 16;
        // stoneNoise = noiseGen4.generateNoiseOctaves(stoneNoise, x * 16, z *
        // 16, 0, 16, 16, 1, d * 2D, d * 2D, d * 2D);

        if(!isSegmentInDodeca(xPos, zPos, dx, dz, requestData)){
            return;
        }
        boolean allInDodeca = isSegmentAllInDodeca(xPos, zPos, dx, dz, requestData);

        for (int z = 0; z < 16; z++) {
            for (int x = 0; x < 16; x++) {
                // int k = (int)(stoneNoise[i + j * 16] / 3D + 3D +
                // rand.nextDouble() * 0.25D);
                int l = -1;
                int distX = dx + (15 - x);
                int distZ = dz + (15 - z);

                // float dist = FastMath.sqrt(distX * distX + distZ * distZ);
                // System.err.println("DIST "+dist);
                int maxHeight;
                maxHeight = (Integer) MiscUtils.readSuperclassPrivateField(TerrainGenerator.class,"maxHeight",this);
                int minHeight;
                minHeight = (Integer) MiscUtils.readSuperclassPrivateField(TerrainGenerator.class,"minHeight",this);

                int margin = (Integer) MiscUtils.readSuperclassPrivateField(TerrainGenerator.class,"margin",this);
                float mar = requestData.getR().mar;
                
                WorldCreatorPlanetFactory factory = (WorldCreatorPlanetFactory) MiscUtils.readSuperclassPrivateField(TerrainGenerator.class,"worldCreator",this);
                if (allInDodeca || Dodecahedron.pnpoly(factory.poly,
                        margin(dx + (15 - x)), margin(dz + (15 - z)), mar)) {
                    float nearestEdge = Dodecahedron.nearestEdge(
                            factory.poly, margin(dx + (15 - x)), margin(dz
                                    + (15 - z)), mar);

                    maxHeight = Math.min(
                            maxHeight,
                            Math.round(Math.max(
                                    planetEdgeHeight,
                                    (FastMath.pow(nearestEdge
                                            + planetEdgeHeight + 24f, 0.8f)))));
                    minHeight = (int) Math.max(1, planetEdgeHeight
                            - (nearestEdge - 4));

                    if (nearestEdge > 20) {
                        maxHeight = 64;
                    } else if (nearestEdge > 5) {
                        maxHeight = (int) Math.min(
                                FastMath.pow(maxHeight, 1.07f), 64);
                    }

                    // System.err.println("NEAREST EDGE "+nearestEdge);
                    // if(Dodecahedron.pnpoly(PlanetCreatorThread.polygon, dx +
                    // (15 - x), dz + (15 - z))){
                    // maxHeight -= (32 - (FastMath.sqrt((240 - dist)*40)));
                    // maxHeight = Math.max(1, maxHeight);
                    // }

                    RRSDecorateHeight(x, z, dx, dz, maxHeight, minHeight,
                            informationArray, data, requestData);
                }
            }
        }
    }


    private void RRSDecorateHeight(int x, int z, int dx, int dz, int maxHeight,
                                int minHeight, short[] informationArray, SegmentDataInterface data,
                                RequestDataPlanet requestData) throws SegmentDataWriteException {
        int top = -1;
        WorldCreatorPlanetFactory factory = (WorldCreatorPlanetFactory) MiscUtils.readSuperclassPrivateField(TerrainGenerator.class,"worldCreator",this);

        if(data == null){
            throw new NullPointerException("Data null");
        }
        if(factory == null){
            throw new NullPointerException("worldCreator null");
        }
        int segPosYMin = Math.abs(data.getSegmentPos().y);

        int segPosYMax = segPosYMin + 16;

        float marginSolo = requestData.getR().mar - 1.0f;

        int xzCoordIndex = (x * 16 + z) * HEIGHT_BLOCKS;
        short topType = factory.getTop();
        ///INSERTED CODE
        byte topMaterialOrientation = (byte) (ElementKeyMap.getInfo(topType).orientatable /*failing that, .getTextureIds().length > 1*/ ? Element.TOP : (byte) -1);
        //TODO: be sure that -1 is right - all the SegmentDataInterface implementations seem to use it as default orientation when none is specified,
        // but one can never be sure...
        ///
        byte activateOnPlacement = ElementInformation
                .activateOnPlacement(topType);

        final int min = Math.max(segPosYMin, 0);
        final int max = Math.min(maxHeight, segPosYMax);


        float testX = margin(dx + (15 - x));
        float testZ = margin(dz + (15 - z));

        float planetHeightInv = 1f/planetEdgeHeight;
        mainLoop:
        for (int y = max-1; y >= min; y--) {

            if (y < planetEdgeHeight && y > 0 &&
                    !Dodecahedron.pnpoly(
                            factory.poly,
                            testX,
                            testZ,
                            1.0f + (y * planetHeightInv) * marginSolo)) {
                //smooth out dodecahedron edges
                return;
            }


            int infoIndex = xzCoordIndex + y;

            if (y <= minHeight && y >= segPosYMin && y < segPosYMax
                    && informationArray[infoIndex] <= 0) {

                data.setInfoElementForcedAddUnsynched(
                        (byte) (x),
                        ((byte) ((y) % 16)),
                        (byte) (z),
                        topType,
                        topMaterialOrientation,
                        activateOnPlacement,
                        false);
            } else {

                if (informationArray[infoIndex] > 0) {
                    if (top < 0) {
                        //first solid block is the top and is set once
                        top = y;
                    }
                }
                if (y >= segPosYMin && y < segPosYMax) {

                    if (regions != null) {
                        synchronized (regions) {
                            requestData.getR().p.set(data.getSegmentPos().x + x,
                                    y, data.getSegmentPos().z + z);

                            if (optimizedRegions != null) {
                                int factor = regions[0].optimizeFactor;

                                requestData.getR().pFac.x = (requestData.getR().p.x + Short.MAX_VALUE)
                                        / factor;
                                requestData.getR().pFac.y = (requestData.getR().p.y + Short.MAX_VALUE)
                                        / factor;
                                requestData.getR().pFac.z = (requestData.getR().p.z + Short.MAX_VALUE)
                                        / factor;

                                List<Region> rList = optimizedRegions
                                        .get(requestData.getR().pFac);

                                if (rList != null) {
                                    final int rsize = rList.size();
                                    for (int i = 0; i < rsize; i++) {
                                        if (rList.get(i).contains(
                                                requestData.getR().p)) {
                                            short deligate = rList
                                                    .get(i)
                                                    .deligate(requestData.getR().p);
                                            if (deligate != Element.TYPE_ALL) {
                                                data.setInfoElementForcedAddUnsynched(
                                                        (byte) (x),
                                                        ((byte) ((y) % 16)),
                                                        (byte) (z),
                                                        deligate, false);
                                                continue mainLoop;
                                            }
                                        }
                                    }
                                }

                            } else {
                                final int rLength = regions.length;
                                for (int i = 0; i < rLength; i++) {
                                    if (regions[i].contains(requestData.getR().p)) {
                                        short deligate = regions[i]
                                                .deligate(requestData.getR().p);
                                        if (deligate != Element.TYPE_ALL) {
                                            data.setInfoElementForcedAddUnsynched(
                                                    (byte) (x),
                                                    ((byte) ((y) % 16)),
                                                    (byte) (z), deligate,
                                                    false);
                                            continue mainLoop;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (top > 0 && y == top) {
                        data.setInfoElementForcedAddUnsynched(
                                (byte) (x),
                                ((byte) ((y) % 16)),
                                (byte) (z),
                                topType,
                                topMaterialOrientation,
                                activateOnPlacement, false);
                    } else {
                        data.setInfoElementForcedAddUnsynched(
                                (byte) (x),
                                ((byte) ((y) % 16)),
                                (byte) (z),
                                informationArray[infoIndex] > 0 ? informationArray[infoIndex]
                                        : 0, false);
                    }
                }
            }

        }
    }
}
