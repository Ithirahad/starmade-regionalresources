package org.ithirahad.resourcesresourced.universe.terrestrial;

import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import org.ithirahad.resourcesresourced.universe.terrestrial.factory.EnergizedPlanetFactory;
import org.ithirahad.resourcesresourced.universe.terrestrial.factory.RRSPlanetFactory;
import org.ithirahad.resourcesresourced.universe.terrestrial.factory.VanillaTweaks.*;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.generator.PlanetCreatorThread;
import org.schema.game.server.controller.world.factory.WorldCreatorFactory;

import java.util.Random;

public class TerrestrialSheet {
    public final RRSTerrestrialPlanetType planetType;
    public final float resourceDensity; //TODO: use this value
    public final TerrestrialPlanetGroundType materials;
    public final long globalSeed;
    public PassiveResourceSupplier[] resources;

    public TerrestrialSheet(RRSTerrestrialPlanetType type, int globalSeed, float resourceDensity) {
        Random rng = new Random(globalSeed);

        this.planetType = type;
        this.resourceDensity = resourceDensity;
        this.globalSeed = globalSeed;

        TerrestrialPlanetGroundType[] groundTypes = type.getGroundTypes();
        if(groundTypes.length > 1)
        materials = groundTypes[rng.nextInt(groundTypes.length)];
        else materials = TerrestrialPlanetGroundType.BARREN_LUNAR;
    }

    public static WorldCreatorFactory getFallbackPlanetCreator(Planet segment) {
        PlanetCreatorThread thread = (PlanetCreatorThread) segment.getCreatorThread();
        float radius = segment.getCore() != null ? segment.getCore().getRadius() : 100.0F; //literally what the dodecahedron uses
        TerrestrialPlanetGroundType materials = TerrestrialPlanetGroundType.BARREN_LUNAR; //TODO: Maybe use a unique type that's more visible, like pink hull :D
        return new RRSPlanetFactory(segment.getSeed(), thread.polygon, radius, materials){
            @Override
            public void createAdditionalRegions(Random random) {}
        };
    }

    public WorldCreatorFactory getCreatorFactory(Planet segment) {
        PlanetCreatorThread thread = (PlanetCreatorThread) segment.getCreatorThread();
        float radius = segment.getCore() != null ? segment.getCore().getRadius() : 100.0F; //literally what the dodecahedron uses

        switch(planetType){
            case V_RED:
                return new VMarsPlanetFactory(segment.getSeed(), thread.polygon, radius);
            case V_TERRAN:
                return new VEarthPlanetFactory(segment.getSeed(), thread.polygon, radius);
            case V_SAND:
                return new VDesertPlanetFactory(segment.getSeed(), thread.polygon, radius);
            case V_PURPLE:
                return new VPurplePlanetFactory(segment.getSeed(), thread.polygon, radius);
            case V_ICE:
                return new VIcePlanetFactory(segment.getSeed(), thread.polygon, radius);
            case RRS_ENERGETIC:
                return new EnergizedPlanetFactory(segment.getSeed(), thread.polygon, radius, materials);
                /*
            case RRS_COLD:
                return new VIcePlanetFactory(segment.getSeed(), thread.polygon, radius); //TODO
            case RRS_TEMPERATE:
                return new VEarthPlanetFactory(segment.getSeed(), thread.polygon, radius); //TODO
            case RRS_HOT:
                return new VDesertPlanetFactory(segment.getSeed(), thread.polygon, radius); //TODO
            case RRS_EXTRADIMENSIONAL:
                return new VPurplePlanetFactory(segment.getSeed(), thread.polygon, radius); //TODO
                 */
            default:
                return new RRSPlanetFactory(segment.getSeed(), thread.polygon, radius, materials){
                    @Override
                    public void createAdditionalRegions(Random random) {}
                };
        }
    }
}
