package org.ithirahad.resourcesresourced.universe.gasplanet;

import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import org.ithirahad.resourcesresourced.industry.ResourceSourceType;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ElementKeyMap;

import javax.vecmath.Color4f;
import java.util.ArrayList;
import java.util.Random;

import static org.ithirahad.resourcesresourced.RRSConfiguration.*;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.lerp;

public enum GasGiantType {
    ALGAE("blooming gas giant","home to a species of buoyant algae, feeding off of sunlight and organic vapours in the planet's cloud tops."), //Very rare microbe-bearing gas giants
    ANBARIC("anbaric vapor gas giant", "suffused with an anomalous and highly useful vapor substance, known as Anbaric. Stations nearby equipped with a Vapor Siphon can harvest the Anbaric Vapor for refinement."), //Anbaric-bearing gas giants
    ANBARIC_FAINT("exotic vapor giant", "likely a captured world that was ejected from a distant star system and drifted into the galactic core region. It contains traces of a strange and useful vapor substance, known as Anbaric Vapor, which is normally only found in certain areas of the galaxy. Stations nearby equipped with a Vapor Siphon can harvest small amounts of this vapor for refinement."),
    PARSYNE("misty gas giant", "notable for having an atmosphere which has accumulated Parsyne plasma from its parent star. However, it is permanently bound to gaseous molecules within the planet, and thus most of it is not in a harvestable or usable state. Stations nearby equipped with a Vapor Siphon can harvest small amounts of Parsyne Plasma from this planet as well as the parent star."),//"Misty" systems
    PARSYNE_FAINT("exotic plasma giant", "likely a captured world that was ejected from a distant star system and drifted into the galactic core region. It contains traces of a useful misty plasma substance, known as Parsyne Plasma, which is normally only found in certain areas of the galaxy. Stations nearby equipped with a Vapor Siphon can harvest small amounts of this plasma for refinement."),
    STORM("turbulent gas giant", "a typical gaseous planet, with global currents and storms containing winds in excess of half the speed of sound."), //cf Jupiter
    CALM("gas giant", "a fairly average gaseous planet. Despite its relatively calm appearance, this planet - like most gas giants - experiences powerful global winds."), //cf Saturn
    ICE("ice giant", "an extremely cold, windy world. Nearby stations equipped with a Vapor Siphon can accumulate and harvest Ice blocks from this planet."), //cf Uranus
    HOT_JUPITER("hot gas giant", "a torrid, landless world in close proximity to its star. Due to the searing temperatures, metallic and crystalline elements may be found circulating through its atmosphere as gases and vapors, which can be harvested by nearby stations equipped with a Vapor Siphon."),
    RELIC("engineered gas giant", "a gaseous world that was once inundated with picotech-derived artificial materials. Most has settled into its depths and become unreachable, but there is a enough remaining in the cloud tops to give the planet a distinct appearance. The reason for this infusion is unknown.");

    private final String typeName;
    private final String description;
    GasGiantType(String typeName, String description){
        this.typeName = typeName;
        this.description = description;
    }

    public String typeName(){
        return typeName;
    }

    public String description(){
        return description;
    }

    public PassiveResourceSupplier[] getGasGiantResourceSet(long seed, Vector3i sector) {
        Random rng = new Random(seed);
        ArrayList<PassiveResourceSupplier> result = new ArrayList<>();
        switch(this){
            case RELIC:
                if(rng.nextBoolean())
                    result.add(new PassiveResourceSupplier(3f,elementEntries.get("Metallic Ore").id, ResourceSourceType.SPACE,sector));
                if(rng.nextBoolean())
                    if(rng.nextBoolean())
                        result.add(new PassiveResourceSupplier(1f,elementEntries.get("Crystalline Ore").id, ResourceSourceType.SPACE,sector));
                break;
            case ANBARIC:
                result.add(new PassiveResourceSupplier(lerp(RSC_ANBARIC_MIN_REGEN_RATE_SEC,RSC_ANBARIC_MAX_REGEN_RATE_SEC,rng.nextFloat()),
                        elementEntries.get("Anbaric Vapor").id, ResourceSourceType.SPACE, sector));
                break;
            case ANBARIC_FAINT:
                result.add(new PassiveResourceSupplier(0.2f, elementEntries.get("Anbaric Vapor").id, ResourceSourceType.SPACE,sector));
                break;
            case PARSYNE:
                result.add(new PassiveResourceSupplier(lerp(RSC_PARSYNE_MIN_REGEN_RATE_SEC * RSC_PARSYNE_PLANET_MOD,RSC_PARSYNE_MAX_REGEN_RATE_SEC * RSC_PARSYNE_PLANET_MOD,rng.nextFloat()), elementEntries.get("Parsyne Plasma").id, ResourceSourceType.SPACE,sector));
                break;
            case PARSYNE_FAINT:
                result.add(new PassiveResourceSupplier(0.2f, elementEntries.get("Parsyne Plasma").id, ResourceSourceType.SPACE,sector));
                break;
            case HOT_JUPITER:
                if(rng.nextBoolean())
                    result.add(new PassiveResourceSupplier(0.4f,elementEntries.get("Rock Dust Capsule").id, ResourceSourceType.SPACE,sector));
                if(rng.nextBoolean())
                    result.add(new PassiveResourceSupplier(0.3f,elementEntries.get("Metallic Ore").id, ResourceSourceType.SPACE,sector));
                if(rng.nextBoolean())
                    if(rng.nextBoolean())
                        result.add(new PassiveResourceSupplier(0.2f,elementEntries.get("Crystalline Ore").id, ResourceSourceType.SPACE,sector));
                break;
            case ICE:
                result.add(new PassiveResourceSupplier(0.4f,ElementKeyMap.TERRAIN_ICE_ID, ResourceSourceType.SPACE,sector));
                break;
            case ALGAE:
                result.add(new PassiveResourceSupplier(0.2f, ElementKeyMap.WATER, ResourceSourceType.SPACE,sector));
                result.add(new PassiveResourceSupplier(0.4f, elementEntries.get("Floral Protomaterial Capsule").id, ResourceSourceType.SPACE,sector));
                //TODO: Add common gas item for FTL fuel mod(s)? EDIT: Actually just have an extractable resource type select event so dependent mods can subscribe to that
                // TODO: values for generic resources. Turns out they matter a lot more than we assumed initially
                break;
        }
        PassiveResourceSupplier[] a = new PassiveResourceSupplier[result.size()];
        result.toArray(a);
        return a;
    }

    public Color4f getGasGiantColorDark(long seed) {
        Random rng = new Random(seed);
        float a = rng.nextFloat();
        float b = rng.nextFloat();
        float c = rng.nextFloat();
        switch(this){
            case ANBARIC:
                return new Color4f(a*0.4f,a*0.132f, a*0.03f,1f);
            case ANBARIC_FAINT:
                return new Color4f(a*0.4f,a*0.232f, a*0.1f,1f);
            case PARSYNE:
                a = rng.nextFloat();
                return new Color4f(a*0.6f + (rng.nextFloat() * 0.05f),a*0.6f + (rng.nextFloat() * 0.05f),a*0.6f + (b * 0.1f),1f);
            case PARSYNE_FAINT:
                a = rng.nextFloat();
                return new Color4f(a*0.4f + (rng.nextFloat() * 0.05f),a*0.4f + (rng.nextFloat() * 0.05f),a*0.4f + (b * 0.1f),1f);
            case STORM:
                return new Color4f(a*0.4f,a*0.14f, a*0.03f,1f);
            case CALM:
                return new Color4f(0.85f-(a*0.06f),0.84f - (b*0.06f), 0.72f - (c*0.06f),1f);
            case ICE:
                if(rng.nextBoolean())
                    return new Color4f(0.1f,0.1f,0.35f + (a*0.2f),1f);
                else
                    return new Color4f(0.15f, 0.6f - (a*0.07f), 0.75f  - (b*0.1f), 1f);
            case HOT_JUPITER:
                return new Color4f(a*0.4f,a*0.14f, b*0.14f,1f);
            case ALGAE:
                return new Color4f((a*0.1f) + 0.05f,(b*0.3f) + 0.2f,c * 0.25f,1f);
            case RELIC:
                return new Color4f(a*0.4f,a*0.45f,a*0.4f,1f); //some shade of green-grey
            default:
                return new Color4f(0.4f,0.14f,0.03f,1f);
        }
    }

    public Color4f getGasGiantColorLight(long seed) {
        Random rng = new Random(seed);
        float a = rng.nextFloat();
        float b = rng.nextFloat();
        float c = rng.nextFloat();
        switch(this) {
            case ANBARIC:
                return new Color4f(0.4f + (a*0.2f), 0.12f + (b*0.1f), c*0.2f,1f);
            case ANBARIC_FAINT:
                return new Color4f(0.5f + (a*0.15f), 0.32f + (b*0.1f), c*0.3f,1f);
            case PARSYNE:
                return new Color4f(0.83f-(a*0.1f),0.85f-(b*0.05f),0.89f-(c*0.05f),1f);
            case RELIC:
            case STORM:
                return new Color4f(0.89f-(a*0.1f),0.88f - (b*0.1f), 0.76f - (b*0.1f),1f);
            case CALM:
                return new Color4f(0.89f-(a*0.06f),0.88f - (b*0.06f), 0.76f - (c*0.06f),1f);
            case ICE:
                if(rng.nextBoolean())
                    return new Color4f(0.15f,0.15f + (a * 0.19f),0.37f + (a*0.2f),1f);
                else
                    return new Color4f(0.2f+(a*0.2f), 0.62f - (a*0.09f), 0.77f  - (b*0.12f), 1f);
            case HOT_JUPITER:
                return new Color4f(0.89f-(a*0.1f),0.88f - (b*0.08f), 0.76f - (b*0.1f),1f);
            case ALGAE:
                return new Color4f((a*0.2f) + 0.25f,(b*0.3f) + 0.6f,c * 0.45f,1f);
            default:
                return new Color4f(0.89f, 0.88f, 0.76f, 1f);
        }
    }
}
