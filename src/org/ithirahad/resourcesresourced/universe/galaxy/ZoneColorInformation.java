package org.ithirahad.resourcesresourced.universe.galaxy;

import javax.vecmath.Tuple4f;
import javax.vecmath.Vector4f;

public class ZoneColorInformation {
    public final boolean clampHueAndSaturation; //must the r, g, and b values change proportionally?
    public final Tuple4f minValues;
    public final Tuple4f maxValues;

    public ZoneColorInformation(boolean clampHueAndSaturation, Tuple4f minValues, Tuple4f maxValues){
        this.clampHueAndSaturation = clampHueAndSaturation;
        this.minValues = minValues;
        this.maxValues = maxValues;
    }

    public ZoneColorInformation(boolean clampHueAndSaturation, float r1, float r2, float g1, float g2, float b1, float b2, float a1, float a2){
        this(clampHueAndSaturation, new Vector4f(r1,g1,b1,a1), new Vector4f(r2,g2,b2,a2));
    }
}
