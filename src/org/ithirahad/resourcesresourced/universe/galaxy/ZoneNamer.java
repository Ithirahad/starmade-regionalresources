package org.ithirahad.resourcesresourced.universe.galaxy;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import org.apache.commons.lang3.ArrayUtils;
import org.schema.common.util.linAlg.Vector3i;

import java.util.HashMap;
import java.util.Random;

import static org.ithirahad.resourcesresourced.RRSConfiguration.NAME_GENERIC_ADJECTIVE_CHANCE;
import static org.ithirahad.resourcesresourced.RRSConfiguration.NAME_PREFIX_CHANCE;


public class ZoneNamer {
    public static String[] majorSuffixes = {"Stars","Expanse","Stretch","Isles Region","Belt","Reach","Strand","Cluster","Veil","Vale","Sea Region","Star Sea","Ocean Region","Star Ocean","Mantle","Thicket","Star Formation","Cascade","Falls","Verge","Shore","Fade","Pass","Passage","River","Creek","Drift","Solitude","Domain","Wilds","Wildlands","Archipelago","Frontier","Impasse","Span","Vestige","Remnant","Shards","Pocket","Meadows Region","Fade","Heath","Branch","Cache","Immensity","Ether","Stain","Scar","Steps","Den","Redoubt","Labyrinth","Astropolis","Spires Region","Clutch","Starfield","Keys"}; //TODO: configurable
    public static String[] minorSuffixes = {"Isle","Islet","Branch","Pocket","Den","End","Strand","Enclave","Hamlet","Sun Haven","Redoubt","Corner","Edge","Curiosity","Folly","Impasse","Terminus","Peak","Zenith","Nadir","Point","Isolate","Exclave","Oubliette","Strait","Spar","Spur","Skystead","Perch","Quagmire","Quandary","Eyrie","Foothill","Tail","Run","Keys","Prominence","Nest","Outlier","Outlands","Shore","Drift","Scatter","Star Finger"}; //is that a fucking JoJo reference? (yes)
    public static String[] majorPrefixes = {"Greater","Great","Outer","Inner","High","Grand","Alpha"};
    public static String[] standardZoneAdjectives = {
            //Arranged so they can be removed in thematic lines or clusters as easily as possible
            //TODO: configurable
            "Drifting","Reaching","Creeping",
            "Drowned","Shadowed","Fallen","Dim","Marred",
            "Harmonious","Harmony",
            "Arcadian","Utopian","Halcyon",
            "Grand",
            "Brilliant", "Beacon", "Luminous", "Luminiferous", "Lighthouse", "Gleaming",
            "Corona", "Crown", "Pinnacle",
            "Elysian", "Empyrean", "Divinity's", "Heaven's", "Providence","Valhalla","Nirvana",
            "Archangel","Angel","Seraph","Cherub","Valkyrie",
            "Forsaken","Tenebrous","Shrouded", "Shadowy", "Wisp", "Echo",
            "Lone", "Forlorn",
            "Intrepid","Brave","Heedless","Fool's","Dauntless",
            "Protean","Shifting","Grey","Purgatory",
            "Orphean","Tartarous","Hadean","Charonine",
            "Stygian", "Abyssal", "Infernal", "Nadir", "Grim",
            "Phantom","Ghoul","Ghast","Spectre","Demon",
            "Tormentor's",
            "Widow's","Widower's",
            "Arrowhead","Bullet","Blade","Bayonet","Sword","Katana","Scimitar","Sabre","Dagger","Thorned","Spearhead",
            "Sage","Rose","Mandrake","Amaranth","Nightshade","Silphium","Foxglove","Lily","Asphodel",
            "Aura","Miasmic", "Nebulous", "Fogborne",
            "Cloudborne","Cumulus","Stratus","Nimbus","Cirrus",
            "Archer's","Hunter's", "Seeker's","Treader's","Chaser's",
            "Mender's", "Oracle's", "Seer's", "Augur's","Mystic's",
            "Mason's","Fletcher's",
            "Dreamer's","Drifter's","Wanderer's","Vagabond's","Peregrine","Eidolon",
            "Heir's", "Baron's", "Ascendant's","Noble's",
            "Barbarian","Savage",
            "Rogue","Thief's","Thieves'","Robber's","Scoundrel","Liar's","Wicked",
            "Edda","Legend's","Kalevala","Odyssey","Gilgamesh","Cu Chullain's","Beowulf's",
            "Excalibur","Gjallarhorn","Grail",
            "Thor's","Odin's","Tyr's","Idunn's","Freya's","Loki's","Zeus's","Poseidon's","Hera's","Athena's","Iris","Perun's","Veles's","Svarog's","Stribog's","Zhiva's",
            "Ranger's","Hero's","Sorcerer's","Knight's","Sellsword","Enchanter's",
            "Dragon's", "Golem", "Fae", "Goblin", "Gnommish", "Elfin", "Dwarrows'",
            "Morass","Mire","Swampflame",
            "Burning", "Candle", "Inferno","Kindling", "Ember", "Blazing", "Scalding", "Torrid", "Sparkfire", "Candlelight", "Wildfire", "Dragon's Breath",
            "Whirlwind", "Tradewind", "Doldrum", "Windward", "Leeward", "Seabreeze", "Waterspout",
            "Tidal", "Crest", "Wavecrest", "Tsunami", "Gyre", "Rippling",
            "Boulder", "Monument", "Monolith", "Obelisk",
            "Argent", "Silvery",
            "Auric", "Gilded",
            "Alpha","Bravo","Delta","Echo","Foxtrot","Juliett","Kilo","Sierra","Tango","Victor",
            "Protonic","Neutrino","Plasmonic","Thermion","Soliton",
            "Dawn","Sunrise","Noontide","Dusk","Gloaming","Midnight",
            "Tribute"
    };

    public static String[] minorZoneAdjectives = {
            "Tumbling", "Aimless",
            "Synchronous",
            "Idyll", "Tranquil",
            "Glowing", "Glimmering", "Shining", "Twinkling", "Arclight", "Lustre", "Spotlight", "Glister",
            "Spring","Summer","Autumn's",
            "Forgotten","Shady","Dim",
            "Lonely",
            "Limbo","Liminal",
            "Boggart","Hob","Domovoy","Selkie","Sprite's","Fairy's","Bugbear",
            "Backwater", "Shanty", "Backwoods", "Bottleneck",
            "Knife", "Stiletto",
            "Snapdragon","Petunia","Dandelion","Rosette","Daisy","Tulip",
            "Foggy","Blindsight",
            "Cloudburst", "Squall",
            "Guzzler's","Wagtail","Sparrow","Chickadee's","Applenook","Ghillie","Tadpole","Jigsaw","Juggler's","Hornswoggler's","Stickler's","Boondoggle",
            "Backdraft","Updraft","Downdraft",
            "Peon's","Porker's","Yhole","Dingbat","Bumblebee"
    };

    public static HashMap<Vector3i, IntOpenHashSet> usedMajorNames = new HashMap<>(); //Each IntOpenHashSet is for a different galaxy
    public static HashMap<Vector3i, IntOpenHashSet> usedMinorNames = new HashMap<>(); //ditto

    public static String[] allAdjectives = null;

    public String generateZoneName(Vector3i galaxy, ZoneClass zoneClass, long seed, boolean isMinor) {
        if(zoneClass == ZoneClass.RR_CORE) return "Galactic Core";

        if(allAdjectives == null) allAdjectives = ArrayUtils.addAll(standardZoneAdjectives,minorZoneAdjectives);

        IntOpenHashSet galaxyUsedMajorZoneNames;
        IntOpenHashSet galaxyUsedMinorZoneNames; //TODO: is this the best type for this? does it matter?
        if(usedMajorNames.containsKey(galaxy)) galaxyUsedMajorZoneNames = usedMajorNames.get(galaxy);
        else{
            galaxyUsedMajorZoneNames = new IntOpenHashSet();
            usedMajorNames.put(galaxy,galaxyUsedMajorZoneNames);
        }
        if(usedMinorNames.containsKey(galaxy)) galaxyUsedMinorZoneNames = usedMinorNames.get(galaxy);
        else{
            galaxyUsedMinorZoneNames = new IntOpenHashSet();
            usedMinorNames.put(galaxy,galaxyUsedMinorZoneNames);
        }

        Random rng = new Random(seed);
        String result = "";

        if(!isMinor && new Random(seed).nextFloat() < (NAME_PREFIX_CHANCE/4)){ //this is bad but it doesn't patch over configs by default, and this needs to reduce significantly.... also, used isolated Random so that messing with this will not change subsequent things
            result = majorPrefixes[rng.nextInt(majorPrefixes.length)] + " "; //e.g. Greater
        }

        if(rng.nextFloat() < NAME_GENERIC_ADJECTIVE_CHANCE){
            if(isMinor) {
                if (galaxyUsedMinorZoneNames.size() >= allAdjectives.length) galaxyUsedMinorZoneNames.clear();
                Integer index;
                do index = rng.nextInt(allAdjectives.length); while (galaxyUsedMinorZoneNames.contains(index)); //TODO: should probably invert this to pick from a linkedlist or something of AVAILABLE names
                galaxyUsedMajorZoneNames.add(index);
                result += allAdjectives[index]; //e.g. Midnight
            }
            else {
                if (galaxyUsedMajorZoneNames.size() >= standardZoneAdjectives.length) galaxyUsedMajorZoneNames.clear();
                Integer index;
                do index = rng.nextInt(standardZoneAdjectives.length); while (galaxyUsedMajorZoneNames.contains(index));
                galaxyUsedMajorZoneNames.add(index);
                result += standardZoneAdjectives[index]; //e.g. Midnight
            }
        }
        else {
            String[] results = zoneClass.adjective();
            if (results.length > 1) result += results[rng.nextInt(results.length)]; //e.g. Glittering
            else result += results[0];
        }

        String[] suffixes = isMinor? minorSuffixes : majorSuffixes;
        result += " " + suffixes[rng.nextInt(suffixes.length)]; //e.g. Veil
        return result;
    }
}
