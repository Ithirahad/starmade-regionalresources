package org.ithirahad.resourcesresourced.universe.galaxy;

import org.ithirahad.resourcesresourced.RRUtils.SpaceGridMap;
import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import org.ithirahad.resourcesresourced.industry.ResourceSourceType;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemClass;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemSheet;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import static org.ithirahad.resourcesresourced.RRSConfiguration.*;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.lerp;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.container;
import static org.ithirahad.resourcesresourced.listeners.BlockRemovalLogic.getStarResourceSector;
import static org.ithirahad.resourcesresourced.universe.starsystem.SystemClass.*;

public class ResourceZone {
    public final String name;
    public final long seed;
    private final boolean minorZone;
    public final ZoneClass zoneClass;
    private final SpaceGridMap<SystemSheet> systemDictionary;
    private final SpaceGridMap<SystemSheet> voidDictionary;
    private final Random rng;
    private Vector3i principalStar = null; //TODO: if we go with bubble display, this might have to be like a Spacegridmap<radius>
    public final Galaxy galaxy;

    public ResourceZone(Galaxy galaxy, long seed, ZoneClass type, boolean isMinor){
        this.rng = new Random(seed);
        this.seed = seed;
        this.galaxy = galaxy;
        this.name = (new ZoneNamer()).generateZoneName(galaxy.galaxyPos, type, seed, isMinor);
        this.minorZone = isMinor;
        zoneClass = type;
        systemDictionary = new SpaceGridMap<>(/*GridMapScale.GALAXY, false*/);
        voidDictionary = new SpaceGridMap<>();
    }

    public ResourceZone(Galaxy galaxy, String name, ZoneClass type, boolean isMinor, Collection<Vector3i> systemList){
        this.seed = ResourcesReSourced.seeds.get(galaxy.galaxyPos);
        this.rng = new Random(seed);
        this.name = name;
        this.galaxy = galaxy;
        this.minorZone = isMinor;
        zoneClass = type;
        systemDictionary = new SpaceGridMap<>(/*GridMapScale.GALAXY, false*/);
        voidDictionary = new SpaceGridMap<>();
        System.err.println("[MOD][Resources ReSourced] Bulk adding " + systemList.size() + " stars to the " + this.name + "!");
        for(Vector3i system : systemList){
            //System.err.println("[MOD][Resources ReSourced][WARNING] Adding star " + star.toString() + " to " + zoneName + "!");
            add(system);
        }
        System.err.println("[MOD][Resources ReSourced] Finished adding stars. Zone now contains " + systemDictionary.count() + " stars.");
    }

    public void add(Vector3i system){
        boolean overwrite = false;
        if(system == null){
            System.err.println("[MOD][Resources ReSourced][ERROR] Attempting to add a null Vector3i to " + name + "!");
            System.exit(-1); //(...yes, it's that bad)
        }

        overwrite = systemDictionary.containsLocation(system) || voidDictionary.containsLocation(system);
        int debugPreSizeStars = systemDictionary.count();
        int debugPreSizeVoids = voidDictionary.count();
        Vector3i systemOrig64 = new Vector3i(system);
        systemOrig64.add(64,64,64);

        boolean isVoid = galaxy.isVoid(systemOrig64);
        SystemSheet sheet = generateSystemSheet(system, isVoid); //was and should be inline; made it separate for debug purposes
        if(isVoid) voidDictionary.put(system,sheet);
        else systemDictionary.put(system,sheet);

        if(isVoid) {
            //placeholder for any special void behaviours (zone based deep-space anomalies?)
        }
        else {
            SystemClass cls = systemDictionary.get(system).systemClass;
            if (cls == RR_MISTY) {
                Random lrand = new Random(galaxy.getSeed() + system.hashCode());
                Vector3i sector = getStarResourceSector(system);
                float sourceRate = lerp(RSC_PARSYNE_MIN_REGEN_RATE_SEC, RSC_PARSYNE_MAX_REGEN_RATE_SEC, lrand.nextFloat()) * sheet.resourceDensity;
                if ((GameServerState.instance != null)) container.setPassiveSources(sector,
                        new PassiveResourceSupplier[]{
                                new PassiveResourceSupplier(sourceRate,
                                        elementEntries.get("Parsyne Plasma").id, ResourceSourceType.SPACE, sector)
                        });
            }
        }

        if(overwrite){
            if(DEBUG_LOGGING) System.err.println("[MOD][Resources ReSourced][WARNING] Zone "+ name +" overwrote a " + (isVoid? "void" : "solar") + " system sheet at ("+system.toString()+")!");
        }
        else if(systemDictionary.count() == debugPreSizeStars && voidDictionary.count() == debugPreSizeVoids){
            System.err.println("[MOD][Resources ReSourced][ERROR] Failed to add a system ("+system.toString()+") to " + name + "!");
            System.exit(-1);
        }
    }

    public void add(@NotNull Collection<Vector3i> starSystems){
        if(!starSystems.isEmpty()) for(Vector3i system : starSystems) {
            add(system);
            /*
            boolean isVoid = galaxy.isVoid(system);
            systemDictionary.put(system, generateSystemSheet(system, isVoid));
            if(isVoid){
                //placeholder
            }
            else {
                if (systemDictionary.get(system).systemClass == RR_MISTY) {
                    Random lrand = new Random(galaxy.getSeed() + system.hashCode());
                    Vector3i sector = new Vector3i(system); //TODO: sys or sys - 64,64,64?
                    sector.scale(VoidSystem.SYSTEM_SIZE);
                    sector.add(8, 8, 8); //center of system(?) Doesn't really matter tbh; could use the corner
                    if ((GameServerState.instance != null)) Main.container.sectorResourceSources.put(sector,
                            new PassiveResourceSupplier[]{
                                    new PassiveResourceSupplier(PARSYNE_MIN_REGEN_RATE + lrand.nextInt(PARSYNE_MAX_REGEN_RATE - PARSYNE_MIN_REGEN_RATE),
                                            elementEntries.get("Parsyne Plasma").id)
                            });
                }
            }
            */
        }
        else System.err.println("[MOD][Resources ReSourced][WARNING] Attempted to add empty system list to " + name + "!");
    }

    public void remove(Vector3i starSystem){
        systemDictionary.remove(starSystem); //hopefully we don't need this; that would have strange implications.
        //if this does get called, don't forget to add the system to the unassociated systems zone.
    }

    public SystemSheet getSheetFor(Vector3i system) throws IllegalArgumentException{
        boolean sys = systemDictionary.containsLocation(system);
        boolean vd = voidDictionary.containsLocation(system);
        if(!sys && !vd) {
            throw new IllegalArgumentException("[MOD][Resources ReSourced] Cannot provide system sheet: zone does not contain the specified system!");
        }
        else return sys? systemDictionary.get(system) : voidDictionary.get(system);
    }

    public boolean containsStar(Vector3i star){
        return systemDictionary.containsLocation(star);
    };

    public int getStarCount(){
        return systemDictionary.count();
    }

    public int getVoidCount(){
        return voidDictionary.count();
    }

    private SystemSheet generateSystemSheet(Vector3i loc, boolean isVoid){
        //TODO: move from external 'generate' to internal 'populate' for most info?
        Random lrand = new Random(galaxy.getSystemSeed(loc));

        float offTypeRocks = OFFTYPE_ASTEROID_CHANCE_MIN + (lrand.nextFloat() * OFFTYPE_ASTEROID_CHANCE_MAX);
        float offTypePlanets = OFFTYPE_PLANET_CHANCE_MIN + (lrand.nextFloat() * OFFTYPE_PLANET_CHANCE_MAX);
        float specialTypePlanetRate = SPECIAL_PLANET_RATE_BASE; //can't be the same as resource density, as there are simply too few planets in the universe for that to make sense
        float rscDensity;
        if(isVoid) rscDensity = 0f;
        else{
            if(this.zoneClass == ZoneClass.NONE) rscDensity = lerp(MIN_BARRENS_SYSTEM_RESOURCE_DENSITY,MAX_SYSTEM_RESOURCE_DENSITY, rng.nextFloat());
            else rscDensity = lerp(MIN_ZONE_SYSTEM_RESOURCE_DENSITY,MAX_SYSTEM_RESOURCE_DENSITY, rng.nextFloat());
        }
        if(minorZone){
            specialTypePlanetRate *= MINOR_ZONE_SPECIAL_PLANET_MODIFIER;
        }
        SystemClass cls = pickSystemType(isVoid);

        if (this.zoneClass == ZoneClass.RR_EXTRADIMENSIONAL) {
            offTypeRocks = 0.0f; //no asteroids from this universe
        //} else if (this.zoneClass == ZoneClass.RR_PARAMETRIC) {
            //would have to pass a value or range of values to this later.
        } else if (this.zoneClass == ZoneClass.RR_CORE){
            specialTypePlanetRate *= CORE_ZONE_SPECIAL_PLANET_MODIFIER;
        }

        return new SystemSheet(cls, offTypeRocks, offTypePlanets, this, rscDensity, specialTypePlanetRate);
    }

    public SystemClass pickSystemType(boolean isVoid){
        SystemClass result;
        if(isVoid) switch(this.zoneClass){
            case RR_FERRIC:
                result = RR_FERRON;
                break;
            case RR_METAL_DENSE:
                result = RR_METAL_VOID;
                break;
            case RR_CRYSTAL_DENSE:
                result = RR_CRYSTAL_VOID;
                break;
            case RR_ANBARIC:
                result = RR_ANBARIC_VOID;
                break;
            case RR_MISTY:
                result = RR_MISTY_VOID;
                break;
            case RR_ENERGETIC:
                result = RR_ENERGETIC_VOID;
                break;
            case RR_FRIGID:
                result = RR_FRIGID_VOID;
                break;
            case RR_EXTRADIMENSIONAL:
                result = RR_EXTRADIMENSIONAL_VOID;
                break;
            default:
                result = NORMAL_VOID;
        }
        else switch(this.zoneClass){
            //TODO: Any special rules for random systems in zones being off-types go here
            //or if we want some kind of super dense jackpot versions of the systems... hmmmmmm
            case NONE:
                result = SystemClass.NORMAL;
                break;
            case RR_CORE:
                result = SystemClass.RR_CORE;
                break;
            case RR_METAL_DENSE:
                result = SystemClass.RR_METAL_RICH;
                break;
            case RR_MISTY:
                result = SystemClass.RR_MISTY;
                break;
            case RR_ANBARIC:
                result = SystemClass.RR_ANBARIC;
                break;
            case RR_ENERGETIC:
                result = SystemClass.RR_ENERGETIC;
                break;
            case RR_EXTRADIMENSIONAL:
                result = SystemClass.RR_EXTRADIMENSIONAL;
                break;
            case RR_FRIGID:
                result = SystemClass.RR_FRIGID;
                break;
            case RR_CRYSTAL_DENSE:
                result = SystemClass.RR_CRYSTAL_RICH;
                break;
            case RR_FERRIC:
                result = SystemClass.RR_FERRON;
                break;
            case RR_PARAMETRIC:
                result = SystemClass.RR_PARAMETRIC;
                break;
            //-------PATHFINDER CRAP-------
            case PF_COMETARY:
                result = SystemClass.PF_COMETARY;
                break;
            case PF_DUSTY:
                result = SystemClass.PF_DUSTY;
                break;
            case PF_EXOTIC:
                result = SystemClass.PF_EXOTIC;
                break;
            case PF_FLUX:
                result = SystemClass.PF_FLUX;
                break;
            case PF_FRIGID:
                result = SystemClass.PF_FRIGID;
                break;
            case PF_PYROCLASTIC:
                result = SystemClass.PF_PYROCLASTIC;
                break;
            case PF_RUDDY:
                result = SystemClass.PF_RUDDY;
                break;
            case PF_TEMPERATE:
                result = SystemClass.PF_TEMPERATE;
                break;
            default:
                result = NORMAL;
        }
        return result;
    }

    public void setPrincipalStar(Vector3i loc){
        principalStar = new Vector3i(loc);
    }

    public Vector3i getPrincipalStar(){
        if(principalStar == null){
            System.err.println("Warning: the " + name + " zone has no principal star!");
            return new Vector3i();
        }
        return new Vector3i(principalStar);
    }

    public ArrayList<Vector3i> getStars() {
        return new ArrayList<Vector3i>(systemDictionary.keySet());
    }

    public ArrayList<Vector3i> getVoids() {
        return new ArrayList<Vector3i>(voidDictionary.keySet());
    }

    public boolean isMinor() {
        return minorZone;
    }
    //TODO: these should probably be some type of Set
}
