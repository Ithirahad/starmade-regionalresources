package org.ithirahad.resourcesresourced.universe.asteroid.rockfactory;

import org.schema.common.FastMath;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.controller.world.factory.WorldCreatorFloatingRockFactory;
import org.schema.game.server.controller.world.factory.planet.structures.TerrainStructure;
import org.schema.game.server.controller.world.factory.planet.structures.TerrainStructureList;
import org.schema.game.server.controller.world.factory.terrain.GeneratorResourcePlugin;
import org.schema.game.server.controller.world.factory.terrain.TerrainDeco;

import java.util.Random;

import static org.ithirahad.resourcesresourced.RRSConfiguration.DENSE_ASTEROID_RESOURCE_CHANCE;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.oreEntries;

public class AegiumRockFactory extends WorldCreatorFloatingRockFactory {


    private static final int[] crystalIDs = {
            452, //purple crystal
            455, //yellow crystal
            459, //blue crystal
    };
    private final short CORE_MATERIAL_TYPE = 275; //frozen rock
    private final short SCATTER_CRYSTAL_TYPE;
    private final short CRUST_MATERIAL_TYPE = 64; //ice
    private final short MANTLE_MATERIAL_TYPE;

    private final int CORE_MATERIAL_INDEX;
    private final int SCATTER_CRYSTAL_INDEX;
    private final int CRUST_MATERIAL_INDEX;
    private final int MANTLE_MATERIAL_INDEX;

    private final Random rand;
    private final short AEGIUM_ORE_TYPE;

    public AegiumRockFactory(long seed) {
        super(seed);
        rand = new Random(seed);

        MANTLE_MATERIAL_TYPE = rand.nextBoolean()? ElementKeyMap.TERRAIN_ICEPLANET_CRYSTAL : ElementKeyMap.TERRAIN_WATER; //ice crystal or water
        SCATTER_CRYSTAL_TYPE = (short)crystalIDs[rand.nextInt(crystalIDs.length)];
        AEGIUM_ORE_TYPE = oreEntries.get("Aegium Ore").left.id;

        CORE_MATERIAL_INDEX = registerBlock(CORE_MATERIAL_TYPE);
        SCATTER_CRYSTAL_INDEX = registerBlock(SCATTER_CRYSTAL_TYPE);
        CRUST_MATERIAL_INDEX = registerBlock(CRUST_MATERIAL_TYPE);
        MANTLE_MATERIAL_INDEX = registerBlock(MANTLE_MATERIAL_TYPE);
    }

    @Override
    protected void terrainStructurePlacement(byte x, byte y, byte z, float v, TerrainStructureList terrainStructureList, Random random) {
        if (FastMath.rand.nextFloat() <= (0.000625))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.IceShard, (short) (random.nextInt(3) + 4), ((short) (random.nextInt(10) + 20)), ((short)  (random.nextInt(3) + 4)));
        if (FastMath.rand.nextFloat() <= (DENSE_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, AEGIUM_ORE_TYPE, CRUST_MATERIAL_TYPE, defaultResourceSize);
        if (FastMath.rand.nextFloat() <= (DENSE_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, AEGIUM_ORE_TYPE, CORE_MATERIAL_TYPE, defaultResourceSize);
    }

    @Override
    protected int getRandomSolidType(float v, Random random) {
        short type;
        if (v < 0.09F) type = (short) (rand.nextFloat() > 0.998f ? SCATTER_CRYSTAL_INDEX : CRUST_MATERIAL_INDEX);
        else if (v < 0.15f) type = (short) (rand.nextFloat() > 0.998f ? SCATTER_CRYSTAL_INDEX : MANTLE_MATERIAL_INDEX);
        else type = (short) CORE_MATERIAL_INDEX; //inner core is made of ice planet rock

        return type;
        //if this looks bad, we can just do purple crystal or harmonic colors (purple/blue/yellow/black)
    }

    @Override
    public void setMinable(Random random) {
        this.minable = new TerrainDeco[1];
        this.minable[0] = new GeneratorResourcePlugin(4 , AEGIUM_ORE_TYPE, CORE_MATERIAL_TYPE);
    }
}