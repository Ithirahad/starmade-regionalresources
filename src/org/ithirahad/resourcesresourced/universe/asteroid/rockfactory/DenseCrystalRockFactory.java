package org.ithirahad.resourcesresourced.universe.asteroid.rockfactory;

import org.schema.common.FastMath;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.controller.world.factory.WorldCreatorFloatingRockFactory;
import org.schema.game.server.controller.world.factory.planet.structures.TerrainStructure;
import org.schema.game.server.controller.world.factory.planet.structures.TerrainStructureList;
import org.schema.game.server.controller.world.factory.terrain.GeneratorResourcePlugin;
import org.schema.game.server.controller.world.factory.terrain.TerrainDeco;

import java.util.Random;

import static org.ithirahad.resourcesresourced.RRSConfiguration.DENSE_ASTEROID_RESOURCE_CHANCE;
import static org.ithirahad.resourcesresourced.RRSConfiguration.STANDARD_ASTEROID_RESOURCE_CHANCE;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.oreEntries;

public class DenseCrystalRockFactory extends WorldCreatorFloatingRockFactory {

    private static final int[] crystalIDs = {
            286, //ice crystal
            452, //purple crystal
            453, //black crystal
            454, //white crystal
            455, //yellow crystal?
            456, //red crystal
            457, //orange crystal
            458, //green crystal
            459, //blue crystal
    };
    private final short PRIMARY_CRYSTAL_TYPE;
    private final short SECONDARY_CRYSTAL_TYPE;
    private final short CRUST_MATERIAL_TYPE = 159; //dolom
    private final short MANTLE_MATERIAL_TYPE = 454; //white crystal
    private final short CORE_MATERIAL_TYPE = ElementKeyMap.TERRAIN_ROCK_WHITE;

    private final int PRIMARY_CRYSTAL_INDEX;
    private final int SECONDARY_CRYSTAL_INDEX;
    private final int CRUST_MATERIAL_INDEX;
    private final int MANTLE_MATERIAL_INDEX;
    private final int CORE_MATERIAL_INDEX;

    private final Random rand;
    private final short CRYSTAL_ORE_ELEMENT_TYPE;

    public DenseCrystalRockFactory(long seed) {
        super(seed);
        rand = new Random(seed);
        PRIMARY_CRYSTAL_TYPE = (short)crystalIDs[rand.nextInt(crystalIDs.length)];
        SECONDARY_CRYSTAL_TYPE = (short)crystalIDs[rand.nextInt(crystalIDs.length)];
        CRYSTAL_ORE_ELEMENT_TYPE = oreEntries.get("Crystalline Ore").left.id;

        PRIMARY_CRYSTAL_INDEX = registerBlock(PRIMARY_CRYSTAL_TYPE);
        SECONDARY_CRYSTAL_INDEX = registerBlock(SECONDARY_CRYSTAL_TYPE);
        CRUST_MATERIAL_INDEX = registerBlock(CRUST_MATERIAL_TYPE);
        MANTLE_MATERIAL_INDEX = registerBlock(MANTLE_MATERIAL_TYPE);
        CORE_MATERIAL_INDEX = registerBlock(CORE_MATERIAL_TYPE);
    }

    @Override
    protected void terrainStructurePlacement(byte x, byte y, byte z, float v, TerrainStructureList terrainStructureList, Random random) {
        if (FastMath.rand.nextFloat() <= (DENSE_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, CRYSTAL_ORE_ELEMENT_TYPE, PRIMARY_CRYSTAL_TYPE, defaultResourceSize);
        if (FastMath.rand.nextFloat() <= (DENSE_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, CRYSTAL_ORE_ELEMENT_TYPE, CORE_MATERIAL_TYPE, defaultResourceSize);
        if (FastMath.rand.nextFloat() <= (STANDARD_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.Rock, PRIMARY_CRYSTAL_TYPE, TerrainStructure.toHalfFloat(2), (short) 0);
    }

    @Override
    protected int getRandomSolidType(float v, Random random) {
        short type;
        if (v < 0.07F) type = (short) (rand.nextFloat() > 0.98f ? SECONDARY_CRYSTAL_INDEX : CRUST_MATERIAL_INDEX);
        else if (v < 0.1f) type = (short) (rand.nextFloat() > 0.98f ? SECONDARY_CRYSTAL_INDEX : MANTLE_MATERIAL_INDEX);
        else if (v < 0.25f) type = (short) PRIMARY_CRYSTAL_INDEX; //outer core is made of solid crystal
        else type = (short) CORE_MATERIAL_INDEX;

        return type;
        //if this looks bad, we can just do purple crystal or harmonic colors (purple/blue/yellow/black)
    }

    @Override
    public void setMinable(Random random) {
        this.minable = new TerrainDeco[2];
        this.minable[0] = new GeneratorResourcePlugin(18 , CRYSTAL_ORE_ELEMENT_TYPE, CRUST_MATERIAL_TYPE);
        this.minable[1] = new GeneratorResourcePlugin(18 , CRYSTAL_ORE_ELEMENT_TYPE, CORE_MATERIAL_TYPE);
    }
}
