package org.ithirahad.resourcesresourced.universe.asteroid.rockfactory.common;

import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.controller.world.factory.WorldCreatorFloatingRockFactory;
import org.schema.game.server.controller.world.factory.planet.structures.TerrainStructure;
import org.schema.game.server.controller.world.factory.planet.structures.TerrainStructureList;
import org.schema.game.server.controller.world.factory.terrain.GeneratorResourcePlugin;
import org.schema.game.server.controller.world.factory.terrain.TerrainDeco;

import java.util.Random;

import static org.ithirahad.resourcesresourced.RRSConfiguration.STANDARD_ASTEROID_RESOURCE_CHANCE;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.oreEntries;
import static org.ithirahad.resourcesresourced.universe.asteroid.rockfactory.common.CommonResourceClass.CRYSTAL;
import static org.ithirahad.resourcesresourced.universe.asteroid.rockfactory.common.CommonResourceClass.METAL;

public abstract class CommonTypeFactory extends WorldCreatorFloatingRockFactory {
    protected final Random rand;

    protected static final int CRUST_TERRAN = ElementKeyMap.TERRAIN_ROCK_NORMAL;
    protected static final int CRUST_MARTIAN = ElementKeyMap.TERRAIN_ROCK_MARS;
    protected static final int CRUST_FROZEN = ElementKeyMap.TERRAIN_ICEPLANET_ROCK;
    protected final short CRUST_MATERIAL_TYPE;
    protected final short CORE_MATERIAL_TYPE;
    protected final int CRUST_MATERIAL_INDEX;
    protected final int CORE_MATERIAL_INDEX;

    public final CommonResourceClass type;
    short ore;

    public CommonTypeFactory(long seed, CommonResourceClass type, int crustMaterial, int... coreMaterials) {
        super(seed);
        rand = new Random(seed);
        this.type = type;
        this.CRUST_MATERIAL_TYPE = (short)crustMaterial;
        this.CORE_MATERIAL_TYPE = (short)coreMaterials[rand.nextInt(coreMaterials.length)];
        this.CRUST_MATERIAL_INDEX = registerBlock(CRUST_MATERIAL_TYPE);
        this.CORE_MATERIAL_INDEX = registerBlock(CORE_MATERIAL_TYPE);

        if(type == METAL){
            ore = oreEntries.get("Metallic Ore").left.id;
        }
        else{
            ore = oreEntries.get("Crystalline Ore").left.id;
        }
    }

    //The idea here is that each common asteroid type will have a standardized crust made of some planet rock.
    //They will also have a core will be made of a different material - in this case, a colored rock matching the climate band.
    //e.g. ice asteroids will have white, purple, or blue cores; lava will have black, red, or orange, etc.
    //Of course, these can be mixed with other things...

    @Override
    protected void terrainStructurePlacement(byte x, byte y, byte z, float v, TerrainStructureList terrainStructureList, Random random) {
        if (random.nextFloat() <= STANDARD_ASTEROID_RESOURCE_CHANCE) terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, ore, CRUST_MATERIAL_TYPE, defaultResourceSize);
        if (random.nextFloat() <= STANDARD_ASTEROID_RESOURCE_CHANCE) terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, ore, CORE_MATERIAL_TYPE, defaultResourceSize);
    }

    @Override
    protected int getRandomSolidType(float v, Random random) {
        return v < 0.07F ? CRUST_MATERIAL_INDEX : CORE_MATERIAL_INDEX;
    }

    @Override
    public void setMinable(Random random) {
        this.minable = new TerrainDeco[2];
        if(type == METAL){
            this.minable[0] = new GeneratorResourcePlugin(9,elementEntries.get("Metallic Ore").id, CRUST_MATERIAL_TYPE);
            this.minable[1] = new GeneratorResourcePlugin(9 ,elementEntries.get("Metallic Ore").id, CORE_MATERIAL_TYPE);
        }
        else if(type == CRYSTAL){
            this.minable[0] = new GeneratorResourcePlugin(9 ,elementEntries.get("Crystalline Ore").id, CRUST_MATERIAL_TYPE);
            this.minable[1] = new GeneratorResourcePlugin(9 ,elementEntries.get("Crystalline Ore").id, CORE_MATERIAL_TYPE);
        }
    }
}