package org.ithirahad.resourcesresourced.universe.asteroid.rockfactory.common;

import org.schema.game.common.data.element.ElementKeyMap;

import java.util.Random;

public class CommonRockIcyFactory extends CommonTypeFactory {
    public final int TERRAIN_ICE_INDEX;
    public CommonRockIcyFactory(long seed, CommonResourceClass type) {
        super(seed, type, CRUST_FROZEN, ElementKeyMap.TERRAIN_ROCK_WHITE, ElementKeyMap.TERRAIN_ROCK_BLUE, ElementKeyMap.TERRAIN_ROCK_PURPLE);
        TERRAIN_ICE_INDEX = registerBlock(ElementKeyMap.TERRAIN_ICE_ID);
    }

    @Override
    protected int getRandomSolidType(float v, Random random) {
        int crustMaterial;
        if(rand.nextFloat() > 0.8) crustMaterial = TERRAIN_ICE_INDEX;
        else crustMaterial = this.CRUST_MATERIAL_INDEX;

        return v < 0.07F ? CORE_MATERIAL_INDEX : (rand.nextBoolean() ? crustMaterial : TERRAIN_ICE_INDEX);
    }
}
