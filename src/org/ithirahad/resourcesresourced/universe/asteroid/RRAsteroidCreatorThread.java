package org.ithirahad.resourcesresourced.universe.asteroid;

import api.mod.StarLoader;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import org.ithirahad.resourcesresourced.events.RRSAsteroidSegmentGenerateEvent;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemSheet;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.controller.generator.CreatorThread;
import org.schema.game.common.data.world.*;
import org.schema.game.server.controller.RequestData;
import org.schema.game.server.controller.RequestDataAsteroid;
import org.schema.game.server.controller.TerrainChunkCacheElement;
import org.schema.game.server.controller.world.factory.*;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.ServerConfig;

import static org.ithirahad.resourcesresourced.ResourcesReSourced.getSystemSheet;
import static org.schema.game.common.data.world.SectorInformation.SectorType.VOID;
import static org.schema.game.server.controller.GenerationElementMap.blockDataLookup;

/**
 * This is mostly NOT MY CODE. I claim no ownership or credit for this mess.
 * It is a mostly-duplicate of the decompiled AsteroidCreatorThread, used to avoid a mysterious server-hanging bug.
 * No rights reserved, except to Schine GmbH who are the originators and proper owners. :P
*/
public class RRAsteroidCreatorThread extends CreatorThread {
    public static final ObjectArrayFIFOQueue<RequestData> dataPool;
    private static final boolean OPTIMIZE_DATA = true;
    private WorldCreatorFloatingRockFactory creator = null;

    public RequestData allocateRequestData(int var1, int var2, int var3) {
        synchronized(dataPool) {
            while(dataPool.isEmpty()) {
                try {
                    dataPool.wait();
                } catch (InterruptedException var4) {
                    var4.printStackTrace();
                }
            }

            return (RequestData)dataPool.dequeue();
        }
    }

    public void freeRequestData(RequestData var1, int var2, int var3, int var4) {
        assert var1 != null;

        var1.reset();
        synchronized(dataPool) {
            dataPool.enqueue(var1);
            dataPool.notify();
        }
    }

    public RRAsteroidCreatorThread(FloatingRock var1) {
        super(var1);
        //this constructor is the whole reason I had to make this class. The default constructor tries to make a default creator thread INSTANTLY, which...
        //...causes problems I don't really understand and apparently is not necessary at all.
        //this.creator = getFallbackCreator(); //this is where we had problems before... and apparently we don't even need to assign it at all :D
    }

    public int isConcurrent() {
        return 2;
    }

    public int loadFromDatabase(Segment var1) {
        return -1;
    }

    public void onNoExistingSegmentFound(Segment seg, RequestData reqdata) {
        this.creator = getAppropriateCreatorFromWorldInfo();

        RRSAsteroidSegmentGenerateEvent var3 = new RRSAsteroidSegmentGenerateEvent(this, seg, (RequestDataAsteroid)reqdata, this.creator);
        StarLoader.fireEvent(var3, true);
        this.creator = var3.getWorldCreatorFloatingRockFactory();

        RequestDataAsteroid var5 = (RequestDataAsteroid)reqdata;
        this.creator.createWorld(this.getSegmentController(), seg, var5);
        TerrainChunkCacheElement var6;
        if (!(var6 = var5.currentChunkCache).isEmpty()) {
            Object var7 = null;
            if (var6.isFullyFilledWithOneType()) {
                var7 = new SegmentDataSingle(false, var6.generationElementMap.getBlockDataFromList(0));
            } else if (var6.generationElementMap.containsBlockIndexList.size() <= 16) {
                int[] var8 = new int[var6.generationElementMap.containsBlockIndexList.size()];

                for(int var4 = 0; var4 < var8.length; ++var4) {
                    var8[var4] = var6.generationElementMap.getBlockDataFromList(var4);
                }

                var7 = new SegmentDataBitMap(false, var8, seg.getSegmentData());
            }

            if (var7 != null) {
                ((SegmentData)var7).setSize(seg.getSize());
                seg.getSegmentData().setBlockAddedForced(false);
                seg.getSegmentController().getSegmentProvider().getSegmentDataManager().addToFreeSegmentData(seg.getSegmentData(), true, true);
                ((SegmentData)var7).setBlockAddedForced(true);
                ((SegmentData)var7).assignData(seg);
            }

        }
    }

    /**
     * Replacement method for the original <code>AsteroidSegmentGenerateEvent Listener</code>
     * that would normally execute its on-event routine within the vanilla creator thread at the point when this method is called.<br/>
     * Attempts to interface with RRS world info and select an asteroid type that matches the information about the asteroid's environment.
     * getFactory is synchronized due to seemingly thread-unsafe GenerationElementMap code executed within factory initializers.
     */
    private WorldCreatorFloatingRockFactory getAppropriateCreatorFromWorldInfo() {
        FloatingRock asteroid = (FloatingRock)getSegmentController();
        try {
            long seed = asteroid.getSeed();

            Vector3i hotPotato = new Vector3i(); //just a vector3i to pass around and return sometimes; be careful not to catch it byref or you lose.
            Vector3i systemCoords = new Vector3i(asteroid.getSystem(hotPotato)); //I hope this arg is correct - it seems to just be a write target that gets hot-potatoed a whole lot and then finally returned
            //TODO: nullpointer
            StellarSystem sys = asteroid.getRemoteSector().getServerSector()._getSystem();

            if (sys.getCenterSectorType() != VOID) {
                Vector3i galaxyCoords = Galaxy.getContainingGalaxyFromSystemPos(sys.getPos(), hotPotato);
                SystemSheet systemSheet;
                systemSheet = getSystemSheet(galaxyCoords, systemCoords); //get sheet for this zone

                float temperature = sys.getTemperature(asteroid.getSector(new Vector3i()));
                //basically a reimplementation of a Sector method that is for some reason private

                RRAsteroidType rockType = systemSheet.pickRockType(temperature, seed);

                asteroid.setModCustomNameServer(rockType.getNiceName());
                return rockType.getFactory(seed);
            } else {
                //Set the creator id to our rock type, so we know what string to draw on CustomAsteroidNameListener
                asteroid.setModCustomNameServer(RRAsteroidType.TEMPERATE.getNiceName());
                return RRAsteroidType.TEMPERATE.getFactory(seed); //voids get generic rocks
            }
        } catch (NullPointerException ex){
            /*if(ConfigValues.debug) {
                System.err.println("[MOD][Resources ReSourced][ERROR] Nullpointer error in asteroid generation! Printing stack trace and exiting.");
                ex.printStackTrace();
                System.exit(-1);
            }
            else {
             */
            asteroid.setModCustomNameServer("Asteroid machine broke");
            return getFallbackCreator(); //fallback
            //}
        } //TODO: catch arrayindexoutofboundsexception?
    }

    private WorldCreatorFloatingRockFactory getFallbackCreator() {
        return RRAsteroidType.TEMPERATE.getFactory(getSegmentController().getSeed());
    }

    public boolean predictEmpty(Vector3i var1) {
        return false;
    }

    static {
        dataPool = new ObjectArrayFIFOQueue((Integer) ServerConfig.CHUNK_REQUEST_THREAD_POOL_SIZE_CPU.getCurrentState());

        for(int var0 = 0; var0 < (Integer)ServerConfig.CHUNK_REQUEST_THREAD_POOL_SIZE_CPU.getCurrentState(); ++var0) {
            dataPool.enqueue(new RequestDataAsteroid());
        }

    }
}
