package org.ithirahad.resourcesresourced.universe.asteroid;

import org.ithirahad.resourcesresourced.universe.asteroid.rockfactory.*;
import org.ithirahad.resourcesresourced.universe.asteroid.rockfactory.common.CommonResourceClass;
import org.ithirahad.resourcesresourced.universe.asteroid.rockfactory.common.CommonRockIcyFactory;
import org.ithirahad.resourcesresourced.universe.asteroid.rockfactory.common.CommonRockLavaFactory;
import org.ithirahad.resourcesresourced.universe.asteroid.rockfactory.common.CommonRockStoneFactory;
import org.schema.game.server.controller.world.factory.WorldCreatorFloatingRockFactory;

import java.util.Random;

import static org.ithirahad.resourcesresourced.ResourcesReSourced.GenerationElementMapLock;

public enum RRAsteroidType {
    COLD("Frozen Asteroid (typical)"),
    HOT("Hot Asteroid (typical)"),
    TEMPERATE("Asteroid (typical)"),
    METAL_DENSE("Asteroid (metal-dense)"),
    CRYSTAL_DENSE("Asteroid (crystal-dense)"),
    FERRON("Asteroid (rich in Ferron)"),
    AEGIUM("Comet (rich in Aegium)"),
    CHROMA("Asteroid (Chromatic)"); //rare motherlode asteroid

    static Random rng = new Random();
    private String niceName;
    RRAsteroidType(String niceName) {
        this.niceName = niceName;
    }

    static CommonResourceClass nextResClass(long seed) {
        rng = new Random(seed);
        return (rng.nextInt(4) == 2)? CommonResourceClass.CRYSTAL : CommonResourceClass.METAL; // 1 in 4 is crystal
    }

    public WorldCreatorFloatingRockFactory getFactory(long seed) {
        synchronized(GenerationElementMapLock) {
            switch (this) {
                case COLD:
                    return new CommonRockIcyFactory(seed, nextResClass(seed / 2L));
                case HOT:
                    return new CommonRockLavaFactory(seed, nextResClass(seed / 2L));
                case METAL_DENSE:
                    return new DenseMetalRockFactory(seed);
                case CRYSTAL_DENSE:
                    return new DenseCrystalRockFactory(seed);
                case FERRON:
                    return new FerronRockFactory(seed);
                case AEGIUM:
                    return new AegiumRockFactory(seed);
                case CHROMA:
                    return new ChromaRockFactory(seed);
                case TEMPERATE:
                default:
                    return new CommonRockStoneFactory(seed, nextResClass(seed));
            }
        }
    }

    public String getNiceName() {
        return niceName;
    }
}
