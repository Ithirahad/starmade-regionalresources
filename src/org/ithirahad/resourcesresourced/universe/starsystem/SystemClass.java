package org.ithirahad.resourcesresourced.universe.starsystem;

public enum SystemClass {
    NORMAL,
    //RR PATHFINDER
    PF_PYROCLASTIC, //black 90%, red 10%
    PF_COMETARY, //white 90%, blue 10%
    PF_EXOTIC, //purple 90%, red dirt (yellow) 10%
    PF_FLUX, //blue 90%, green 510%
    PF_DUSTY, //red dirt (yellow) 90%, black 10%
    PF_TEMPERATE, //green 90%, orange 10%
    PF_FRIGID, //orange 90%, white 10%
    PF_RUDDY, //red 90%, red dirt (yellow) 5%, purple 5%
    //REGIONAL RESOURCES (FULL)
    RR_CORE, //Allows VERY SLOW parsyne harvesting, occasional aegium comets or ferron asteroids, occasional thermyn or anbaric planets
    RR_MISTY, //Allows parsyne star harvesting
    RR_FRIGID, //Allows aegium ore spawns
    RR_FERRON, //Allows ferron ore spawns
    RR_ENERGETIC, //Allows thermyn planet spawns
    RR_ANBARIC, //Allows anbaric gas giant spawns
    RR_METAL_RICH, //lots of common metal
    RR_CRYSTAL_RICH, //lots of common crystal
    BA_RELIC,
    RR_PARAMETRIC, //random params
    RR_EXTRADIMENSIONAL, //Reserved :DD

    NORMAL_VOID,
    RR_MISTY_VOID,
    RR_ENERGETIC_VOID,
    RR_ANBARIC_VOID,
    RR_METAL_VOID,
    RR_FERRON_VOID,
    RR_FRIGID_VOID,
    RR_CRYSTAL_VOID,
    RR_EXTRADIMENSIONAL_VOID,

    WARP_VOID,

    UNKNOWN;

    public String description(){
        switch(this){
            case PF_PYROCLASTIC:
                return "This system's asteroids were formed by violent supervolcano explosions and protoplanetary collisions. They contain still-scalding lava, and burning red and orange ores that only form in extreme heat.";
            case PF_COMETARY:
                return "The heat of this system's star seems to be confined to a small radius by unknown phenomena, and its asteroid belts are populated with icy, comet-like rocks. They contain special ores that only condense in the extreme cold.";
            case PF_EXOTIC:
                return "This system formed in a patch of space rich in unusual forms of matter. Many of its asteroids are composed of strange purple stone, rich with precious metals and unique crystals.";
            case PF_FLUX:
                return "This star system's asteroid belts course with a subtle energy flux. Its asteroids are a strange shade of blue, and contain ores useful in the creation of energy shield modules.";
            case PF_DUSTY: case PF_RUDDY:
                return "This system once contained unusual amounts of free-floating dust, sand, and rock. It has condensed into dusty red asteroids.";
            case PF_TEMPERATE:
                return "This system's asteroids formed in unusually mild conditions, allowing the formation of large amounts of minerals that would normally be mostly destroyed in the harsh environment of space. Asteroids with a curious green hue can be found here in multitudes.";
            case PF_FRIGID:
                return "The asteroids in this system seem to have formed very far from their star, but contain relatively low amounts of ice.";
                //-------------------------------------------------------------------------
            case RR_CORE:
                return "The star of this solar system drifts through the cosmos erratically, in the wild gravitational dance of the galactic center.\r\n It may have formed here, or may have been ejected from its original orbit and fallen into the core region.\r\nEither way, core systems such as this one contain small amounts of some resources from all over the galaxy.";
            case RR_MISTY:
                return "This system's star emits trace amounts of strange, whitish mist with energy-refracting properties.\r\nWhen condensed, this mist might prove valuable for the construction of directed energy devices.\r\nWith the proper equipment placed near the star, you may be able to harvest it...";
            case RR_FRIGID:
                return "The heat of this system's star seems to be confined to a small radius by an unknown metaspatial effect, and its asteroid belts are populated with icy, comet-like rocks.\r\nThey contain ethereal crystals that only form in the extreme cold. When refined, the crystals may prove useful in energy field manipulation.";
            case RR_FERRON:
                return "The enriched stellar debris immediately surrounding this system's star has condensed into asteroids full of uncommon metallic ore.\r\nAs these special asteroids only exist very close to the star, mining may prove hazardous. However, when refined, the ore you obtain should yield an unusual and useful metal...";
            case RR_ENERGETIC:
                return "This system appears to have been exposed to exotic matter clouds as it formed. Some planets that formed here may contain a curious, highly-energetic substance beneath their crust.\r\nIf extracted and refined, its energetic and volatile properties can be exploited to generate vast amounts of power... and potentially, unleash it all at once.";
            case RR_ANBARIC:
                return "This system's interplanetary medium contains faint traces of strange vapor, with tantalizing anomalous properties.\r\nThe strange substance is likely to be found in useful quantities within gas giant planets nearby. If harvested and condensed, this vapor may prove interesting... and useful.";
            case RR_METAL_RICH:
                return "Due to the circumstances of its formation, this system's dense asteroids and planets contain exceptional amounts of common metal ores.";
            case RR_CRYSTAL_RICH:
                return "This system's asteroid fields and planets contain particularly exceptional amounts of unexceptional - but useful - common crystals.";
            case RR_PARAMETRIC:
                return "This solar system's characteristics defy current scientific models. Who knows what interesting and useful materials and strange new worlds may be found here...";
            case RR_EXTRADIMENSIONAL:
                return "This system contains seemingly impossible forms of matter. Likely circumstances of formation: <<<UNABLE TO CALCULATE... UNABLE T 01001110 01001111 00100000 01001110 01001111 00100000 01001110 01001111 00100000 01001110 01001111 00100000 01001110 01001111 00100000 01010000 01001100 01000101 01000001 01010011 01000101 01010100 01000101 01010010 01001101 01001001 01001110 01000001 01010100 01000101>>>";
            case NORMAL_VOID:
                return "This block of interstellar void space appears to be largely unremarkable.";
            case RR_MISTY_VOID:
                return "This block of interstellar void space contains traces of parsyne plasma. Though concentrations are low out here in the void, this likely indicates that nearby stars are producing copious amounts of the plasma...";
            case RR_ENERGETIC_VOID:
                return "This block of interstellar void space contains faint but unusual signatures of thermynogenic energy. Nearby star systems likely contain a unique resource...";
            case RR_ANBARIC_VOID:
                return "This block of interstellar void space exhibits faint spacetime fluctuations, as if influenced by tiny wisps of some exotic material that violates the normal laws of physics.\r\nThis material is likely present in larger, useful concentrations within nearby star systems...";
            case RR_CRYSTAL_VOID:
                return "This block of interstellar void space contains the occasional microscopic fleck of crystalline matter, probably ejected from nearby star systems in asteroid collisions.";
            case RR_METAL_VOID:
                return "This block of interstellar void contains a few microscopic 'pebbles' composed of common metals, likely ejected from nearby star systems in asteroid collisions.";
            case RR_FERRON_VOID:
                return "This block of interstellar void space contains a few nanoscopic, resonating particles of a unique metal substance, likely ejected from nearby star systems.";
            case RR_FRIGID_VOID:
                return "Even for interstellar space, this void area is abnormally cold. It seems that almost all of the thermal energy from nearby stars and astronomical phenomena is dampened somehow.";
            case RR_EXTRADIMENSIONAL_VOID:
                return "While mostly empty of matter and energy, this expanse of space exhibits slightly shifting fundamental parameters that should not be possible in our universe.";
            case UNKNOWN:
                return "This system is as yet unexplored, and its contents and nature are unknown...";
            case BA_RELIC:
                return "This system was once a part of an extremely advanced ancient empire. While long gone, their technological traces can be found in the very structure of the local star, planets, and asteroids";
            case WARP_VOID:
                return "This dimensional expanse is suffused with tachyon flux and forms of energy and matter that our science still has no name for. It facilitates faster-than-light travel to the furthest reaches of the galaxy, but don't stare into it too hard - it might stare back into you.";
            default:
                return "This system block appears to be mostly nondescript.";
        }
    }

    public int maxGiants(){
        switch(this){
            case RR_MISTY:
                return 6;
            case RR_FRIGID:
                return 5;
            case RR_ANBARIC:
                return 5;
            case RR_METAL_RICH:
                return 3;
            case RR_CRYSTAL_RICH:
                return 3;
            case RR_EXTRADIMENSIONAL:
                return 2;
            case RR_PARAMETRIC:
                //this would be a custom parameter... hmm
            default:
                return 4;
        }
    };

    public String descriptor(){
        switch(this){
            case NORMAL:
                return "typical";
            case RR_CORE:
                return "core";
            case RR_MISTY:
                return "stellar mist-flooded";
            case RR_FRIGID:
                return "frigid";
            case RR_FERRON:
                return "Ferron-rich";
            case RR_ENERGETIC:
                return "exotic matter-saturated";
            case RR_ANBARIC:
                return "energetic flux-filled";
            case RR_METAL_RICH:
                return "metal-rich";
            case RR_CRYSTAL_RICH:
                return "crystal-rich";
            case RR_PARAMETRIC:
                return "unusual";
            case RR_EXTRADIMENSIONAL:
                return "unreal";
            case NORMAL_VOID:
                return "empty void";
            case BA_RELIC:
                return "ancient relic";
            case RR_ANBARIC_VOID:
            case RR_CRYSTAL_VOID:
            case RR_ENERGETIC_VOID:
            case RR_EXTRADIMENSIONAL_VOID:
            case RR_METAL_VOID:
            case RR_MISTY_VOID:
            case RR_FERRON_VOID:
            case RR_FRIGID_VOID:
                return "largely empty";
            case WARP_VOID:
                return "extradimensional";
            default:
                return "uncategorized";
        }
    }

    public Character classLetter(){
        switch(this){
            case PF_PYROCLASTIC:
                return 'H'; //hot
            case PF_COMETARY:
                return 'C'; //cold
            case PF_DUSTY:
                return 'D'; //desert
            case PF_EXOTIC:
                return 'X'; //Xotic :P
            case PF_FLUX:
                return 'Z'; //more xtreme kool letterz idk
            case PF_FRIGID:
                return 'K'; //idk
            case PF_RUDDY:
                return 'F'; //ferrous
            case PF_TEMPERATE:
                return 'G'; //green/Goldilocks
            //-----------------------------------------------------------
            case RR_METAL_RICH:
                return 'M'; //Metal
            case RR_CRYSTAL_RICH:
                return 'C'; //Crystal
            case RR_MISTY:
                return 'P'; //Parsyne
            case RR_FRIGID:
                return 'A'; //Aegium
            case RR_FERRON:
                return 'F'; //Ferron
            case RR_ANBARIC:
                return 'A'; //Anbaric
            case RR_ENERGETIC:
                return 'T'; //Thermyn
            case RR_PARAMETRIC:
                return 'U'; //Unknown
            case RR_EXTRADIMENSIONAL:
                return '?';
            case WARP_VOID:
                return 'W';
            default:
                return 'N';
        }
    }

    public static SystemClass[] pathfinderClasses(){
        return new SystemClass[]{PF_COMETARY,PF_DUSTY,PF_EXOTIC,PF_FLUX,PF_FRIGID,PF_PYROCLASTIC,PF_RUDDY,PF_TEMPERATE};
    }

    public static SystemClass[] RRClasses(){
        return new SystemClass[]{RR_METAL_RICH, RR_ENERGETIC, RR_ANBARIC, RR_FRIGID, RR_CRYSTAL_RICH, RR_FERRON, RR_MISTY};
        //Core is a special case, so don't return that.
    }
}
