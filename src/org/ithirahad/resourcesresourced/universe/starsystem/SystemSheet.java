package org.ithirahad.resourcesresourced.universe.starsystem;

import api.common.GameCommon;
import api.common.GameServer;
import api.mod.ModStarter;
import org.ithirahad.resourcesresourced.RRUtils.SpaceGridMap;
import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import org.ithirahad.resourcesresourced.listeners.GasPlanetPhysicsListener;
import org.ithirahad.resourcesresourced.universe.asteroid.RRAsteroidType;
import org.ithirahad.resourcesresourced.universe.galaxy.ResourceZone;
import org.ithirahad.resourcesresourced.universe.gasplanet.GasGiantSheet;
import org.ithirahad.resourcesresourced.universe.gasplanet.GasGiantType;
import org.ithirahad.resourcesresourced.universe.terrestrial.RRSTerrestrialPlanetType;
import org.ithirahad.resourcesresourced.universe.terrestrial.TerrestrialSheet;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.world.SectorInformation;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;

import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.ithirahad.resourcesresourced.RRSConfiguration.*;
import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.getGalaxyFromSystemPosSafe;
import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.getStellarSystemFromSecPosWithoutDangerousTmps;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.*;
import static org.ithirahad.resourcesresourced.universe.asteroid.RRAsteroidType.*;
import static org.ithirahad.resourcesresourced.universe.gasplanet.GasGiantType.*;
import static org.ithirahad.resourcesresourced.universe.terrestrial.RRSTerrestrialPlanetType.*;
import static org.schema.game.common.data.world.SectorInformation.SectorType.VOID;
import static org.schema.game.common.data.world.VirtualEntityAttachment.universe;
import static org.schema.game.common.data.world.VoidSystem.SYSTEM_SIZE;
import static org.schema.game.common.data.world.VoidSystem.SYSTEM_SIZE_HALF;
import static org.schema.game.server.data.Galaxy.halfSize;

public class SystemSheet {
    public final ResourceZone zone;
    public SystemClass systemClass; //not final, as someday maybe some type of world event could change it
    public float offTypeAsteroidFrequency; //chance to spawn off-type rocks
    public float offTypePlanetFrequency; //chance to spawn planets that do not match the zone type
    private final SpaceGridMap<GasGiantSheet> gasGiants;
    private final SpaceGridMap<TerrestrialSheet> terrestrialPlanets;
    public final ArrayList<Vector3i> retrievedOrbitSectors = new ArrayList<>();
    public final float resourceDensity; //TODO: serverside should be different somehow, force some exploration
    private final float specialPlanetRate;
    public boolean generatedGiants;
    private boolean generatedTerrestrialsOnServer = false;
    //TODO: allowed off-types list?
    //TODO: parametric stuff?

    private final Vector3i positionShift = new Vector3i(); //position shift
    private final Vector3i halfSizeVector = new Vector3i(SYSTEM_SIZE_HALF,SYSTEM_SIZE_HALF,SYSTEM_SIZE_HALF);

    public SystemSheet(SystemClass systemClass, float offTypeFreqAsteroids, float offTypeFreqPlanets, ResourceZone zone, float resourceDensity, float specialPlanetTypeRate){
        this.zone = zone;
        this.systemClass = systemClass;
        this.offTypeAsteroidFrequency = offTypeFreqAsteroids;
        this.offTypePlanetFrequency = offTypeFreqPlanets;
        this.resourceDensity = resourceDensity;
        specialPlanetRate = specialPlanetTypeRate;
        this.gasGiants = new SpaceGridMap<>();
        this.terrestrialPlanets = new SpaceGridMap<>();
        this.generatedGiants = false; //TODO: load gas giants from resource zone upon entry.
    }

    public SystemSheet(SystemClass systemClass, float offTypeFreqAsteroids, float offTypeFreqPlanets, ResourceZone zone, float resourceDensity){
        this(systemClass,offTypeFreqAsteroids,offTypeFreqPlanets,zone,resourceDensity,SPECIAL_PLANET_RATE_BASE); //backwards compatibility
    }

    public RRSTerrestrialPlanetType pickTerrestrialType(float temperature, long seed, boolean useCustom, boolean minorZone) {
            Random rand = new Random(seed);
            boolean cold = temperature <= ASTEROID_PLANET_FROSTLINE;
            boolean hot = temperature >= ASTEROID_PLANET_FIRELINE - 0.15f; //hot planets shouldn't only be in the burn radius

            if(!useCustom) {
                switch (systemClass) {
                    case RR_FRIGID:
                        if (hot) return rand.nextBoolean() ? V_SAND : V_TERRAN;
                        else return V_ICE;
                    case RR_ENERGETIC:
                        if (rand.nextBoolean()) return RRS_ENERGETIC;
                        break;
                    case RR_EXTRADIMENSIONAL:
                        return V_PURPLE;
                }
                //true default:
                if(rand.nextFloat() < 0.1f) return V_PURPLE;
                else if (hot) return V_SAND;
                else if (cold) return V_ICE;
                else return rand.nextBoolean() ? V_RED : V_TERRAN;
            } //unused case, since RRS Pathfinder never happened. (as it turns out, adding custom resources was the EASY part.)

            else if(rand.nextFloat() < offTypePlanetFrequency) return getRRTypes()[rand.nextInt(getRRTypes().length)]; //get a random RRS planet type
            else{
                float n = rand.nextFloat();

                switch(systemClass) {
                    case RR_MISTY:
                        return RRS_MIST;
                    case RR_FRIGID:
                        if (hot) return rand.nextBoolean() ? RRS_BARREN : RRS_TEMPERATE;
                        else if (n < specialPlanetRate) return RRS_COLD_AEGIUM;
                        else return rand.nextBoolean() ? V_ICE : RRS_COLD;
                        // Special handling since these systems are supposed to be uncharacteristically cold.
                        // No hot planets or deserts, and other standard types only very near the star.
                    case RR_ENERGETIC:
                        if ((minorZone && n < 0.7) || n < 0.45) return RRS_ENERGETIC;
                        // rate for these planets is higher, as at least for now they are the only source of Thermyn.
                        // 70% chance in minor zone, 45% in major.
                        break;
                    case RR_CRYSTAL_RICH:
                        if (n < specialPlanetRate) return RRS_CRYSTAL;
                        break;
                    case RR_FERRON:
                        if (n < specialPlanetRate) return RRS_FERRON;
                        else if (rand.nextBoolean()) return RRS_DESERT;
                        break;
                    case RR_METAL_RICH:
                        if (n < specialPlanetRate) return RRS_METALLIC; //greater occurrence of barren planets, as those have metal. Too bad I didn't add a dedicated metal planet type that didn't have ferron lol
                        else if(rand.nextBoolean()) return RRS_BARREN;
                        break;
                    case RR_CORE:
                        if(n < specialPlanetRate) {
                            boolean a = rand.nextBoolean();
                            boolean b = rand.nextBoolean();
                            if(a == b) return RRS_FERRON;
                            else if (a) return RRS_ENERGETIC;
                            else return RRS_MIST;
                            //anbaric and the other resources don't have planet types of their own
                        }
                        break;
                    case RR_EXTRADIMENSIONAL:
                        return rand.nextBoolean() ? RRS_EXTRADIMENSIONAL : V_PURPLE;
                }
                //true default:
                if (cold) return rand.nextBoolean() ? RRS_COLD : V_ICE;
                else if (hot) return rand.nextBoolean() ? RRS_HOT : RRS_DESERT;
                else if (n < 0.5) return RRS_BARREN;
                else if (n < 0.7) return RRS_DESERT;
                else if (n < 0.9) return RRS_TEMPERATE;
                else return V_TERRAN;
            }
    }

    public GasGiantType pickGiantType(float temperature, long seed) {
        Random rand = new Random(seed);
        boolean cold = temperature <= ASTEROID_PLANET_FROSTLINE;
        boolean hot = temperature >= ASTEROID_PLANET_FIRELINE;
        float n = rand.nextFloat();
        switch(this.systemClass){
            case RR_ANBARIC:
                if(n < specialPlanetRate) return ANBARIC;
                break;
            case RR_MISTY:
                return PARSYNE;
            case RR_FRIGID:
                if(hot) return rand.nextBoolean() ? STORM : CALM;
                else return ICE;
            case RR_CORE:
                if(n < specialPlanetRate) return rand.nextBoolean() ? ANBARIC_FAINT : PARSYNE_FAINT;
                break;
        }
        if(cold) return ICE;
        else if(hot) return HOT_JUPITER;
        else{
            if(rand.nextFloat() < OFFTYPE_CHROMA_CHANCE) return ALGAE; //The jolly green giants are pretty much an Easter egg
            else return rand.nextBoolean() ? STORM : CALM;
        }
    }

    public RRAsteroidType pickRockType(float temperature, long seed) {
        Random rand = new Random(seed);
        boolean cold = temperature <= ASTEROID_PLANET_FROSTLINE;
        boolean hot = temperature >= ASTEROID_PLANET_FIRELINE;
        float n = rand.nextFloat();

        switch (this.systemClass) {
            case RR_FRIGID: //aegium
                if(n > offTypeAsteroidFrequency) {
                    if (cold)
                        return (rand.nextFloat() < resourceDensity) ? AEGIUM : COLD;
                    else if (hot) return rand.nextBoolean() ? COLD : TEMPERATE; //no hot rocks
                    else return COLD;
                }
                else if (hot) break;
                else {
                    if(rand.nextFloat() < OFFTYPE_CHROMA_CHANCE) return RRAsteroidType.CHROMA;
                    else return rand.nextBoolean()? COLD : (rand.nextBoolean()? METAL_DENSE : CRYSTAL_DENSE);
                }
                //don't go to offtype unless hot, as hot asteroids shouldn't exist in Frigid zones except <MAYBE> next to the star

            case RR_FERRON: //ferron
                if(n > offTypeAsteroidFrequency) {
                    if (hot) {
                        return (rand.nextFloat() < resourceDensity) ? FERRON : HOT;
                    } else return pickNormalRockType(temperature);
                }
                break;
                //go to offtype

            case RR_METAL_RICH: //dense metal
                if (n > offTypeAsteroidFrequency) {
                    return (rand.nextFloat() < resourceDensity) ? METAL_DENSE : pickNormalRockType(temperature);
                }
                break;
                //go to offtype

            case RR_CRYSTAL_RICH: //dense crystal
                if (n > offTypeAsteroidFrequency) {
                    return (rand.nextFloat() < resourceDensity) ? CRYSTAL_DENSE : pickNormalRockType(temperature);
                }
                break;
                //go to offtype

            case RR_CORE:
                if(n > offTypeAsteroidFrequency) {
                    boolean rare = rand.nextFloat() < 0.04f;
                    if (cold) return rare? AEGIUM : COLD;
                    else if (hot) return rare? FERRON : HOT;
                    else return TEMPERATE;
                }
                break;
                //TODO: Do we want fancy asteroids for the misty or flux systems? Maybe include red crystal and stuff to fit the theme
                // maybe blue crystal mantle for generic asteroids in thermyn systems
                // or white crystal spikes in misty systems

            default:
                //generic
                //TODO: might have to make a new wildcard cold/neutral/hot asteroid type with generic rock and whatever ores,
                // though that breaks consistency and "lore" of certain minerals only forming under certain conditions
                // otherwise have distinct cold offtypes, temperate offtypes, and hot offtypes.
                if(n > offTypeAsteroidFrequency) return pickNormalRockType(temperature);
        }
        //offtypes
        if(rand.nextFloat() < OFFTYPE_CHROMA_CHANCE) return RRAsteroidType.CHROMA;
        else return RRAsteroidType.values()[rand.nextInt(RRAsteroidType.values().length - 1)]; //anything but chroma
    }

    public static RRAsteroidType pickNormalRockType(float temperature){
        if (temperature >= ASTEROID_PLANET_FIRELINE) return HOT;
        else if (temperature <= ASTEROID_PLANET_FROSTLINE) return COLD;
        else return TEMPERATE; //Generic rock and crystal asteroids
    }

    public void generateGasGiants(Vector3i systemPos){
        Galaxy galaxy = zone.galaxy;
        if(!galaxy.isVoid(Galaxy.getRelPosInGalaxyFromAbsSystem(systemPos,new Vector3i()))) {
            Random lrand = new Random(galaxy.getSystemSeed(systemPos));
            List<Vector3i> orbitalSectors = new ArrayList<>();
            List<Float> distances = new ArrayList<>();
            int[] orbits = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
            Vector3f inclination = new Vector3f();
            Vector3i corner = new Vector3i(systemPos);
            corner.scale(SYSTEM_SIZE);
            Vector3i sectorLocal = new Vector3i();
            Vector3f unitSecVector = new Vector3f();

            System.out.print(""); //nop

            Vector3i locFromCorner = new Vector3i(systemPos); //corner-origin coordinates, the eternal source of galactic confusion
            locFromCorner.add(halfSize, halfSize, halfSize); //AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

            galaxy.getSystemOrbits(locFromCorner, orbits); //...Gives random seed numbers if there is an orbit at that band
            galaxy.getSystemAxis(locFromCorner, inclination); //anything approx. 90 deg off is on-plane (though with constant cube size, the accuracy gets worse the closer in you are... Maybe this is a good thing.)
            inclination = new Vector3f(inclination); //Just in case... was getting some strangeness with planet orbits
            inclination.normalize(); //dunno what's even going on in there but I'll do this just to be safe
            galaxy.getSunPositionOffset(locFromCorner, positionShift);
            positionShift.scale(-1); //shrug
            positionShift.add(8, 8, 8);

            //now, get a list of valid orbital sectors.
            for (int x = 0; x < SYSTEM_SIZE; x++)
                for (int y = 0; y < SYSTEM_SIZE; y++)
                    for (int z = 0; z < SYSTEM_SIZE; z++) {
                        sectorLocal.set(x, y, z);
                        unitSecVector.set(sectorLocal.x, sectorLocal.y, sectorLocal.z);
                        unitSecVector.sub(halfSizeVector.toVector3f()); //no offset here, as we're still in 'ideal' coords
                        unitSecVector.normalize();
                        float dot = unitSecVector.dot(inclination);
                        if (FastMath.abs(dot) < 0.2f) {
                            sectorLocal.sub(halfSizeVector); //now we want to compare against radii, again in ideal coords
                            float dist = sectorLocal.length();
                            for (int i = 0; i < orbits.length; i++)
                                if (Galaxy.isPlanetOrbit(orbits[i])) { //TODO: Verify that I didn't just kill all gas giant generation...
                                    if (FastMath.abs(dist - (i + 1)) < 1) {
                                        Vector3i trueSector = new Vector3i(sectorLocal);
                                        trueSector.add(corner);
                                        trueSector.add(positionShift); //includes both bringing back to center-origin and accounting for solar offset
                                        if (!orbitalSectors.contains(trueSector)) {
                                            orbitalSectors.add(trueSector);
                                            distances.add(dist);
                                            if (DEBUG_LOGGING) retrievedOrbitSectors.add(trueSector);
                                        }
                                    }
                                }
                        }
                    }

            //select some at random to add a gas giant to.
            if (orbitalSectors.size() > 0) {
                int giants = lrand.nextInt(systemClass.maxGiants() + 1);
                GasGiantSheet giant;
                Random textureSelector = new Random(systemPos.hashCode());
                for (int i = 0; i < giants; i++) {
                    int index = i;
                    index = lrand.nextInt(orbitalSectors.size());

                    Vector3i sector = orbitalSectors.get(index);
                    GasGiantType type;
                    long ggSeed = seeds.get(galaxy.galaxyPos) * i * orbitalSectors.get(index).hashCode();
                    type = pickGiantType(getTemperature(sector), ggSeed);

                    PassiveResourceSupplier[] minableResources = type.getGasGiantResourceSet(ggSeed, sector);
                    float rotation = GAS_GIANT_MIN_ROTATION + (lrand.nextFloat() * (GAS_GIANT_MAX_ROTATION - GAS_GIANT_MIN_ROTATION));
                    if (lrand.nextFloat() < 0.1) rotation = -rotation; //reverse spin
                    int textureID = 0;
                    if(!GameCommon.isDedicatedServer()) textureID = textureSelector.nextInt(gasGiantTextures.size());
                    //if it's a dedicated server, resources are probably not loaded, so this would error out and prevent gas giant generation on serverside. Yikes!

                    giant = new GasGiantSheet(type, GAS_GIANT_MIN_RADIUS + (lrand.nextFloat() * (GAS_GIANT_MAX_RADIUS - GAS_GIANT_MIN_RADIUS)),
                            rotation, (float) lrand.nextGaussian() * 30, (float) lrand.nextGaussian() * 30,
                            new Vector3f(0.0f, 0.0f, 0.0f), //this is where offset would go, if we wanted to actually implement that
                            minableResources, ggSeed, textureID);
                    float localModifier = getResourceModifier(sector);
                    if(zone.isMinor()) localModifier *= MINOR_ZONE_PLANET_RESOURCE_MODIFIER;
                    for(PassiveResourceSupplier w : giant.resourcePools){
                        w.passivePoolRegenRate *= localModifier;
                    }
                    //TODO: gas giant resource add event
                    if(ModStarter.justStartedServer) container.setPassiveSources(sector, giant.resourcePools);
                    GasPlanetPhysicsListener.addGasGiantLocation(sector);
                    gasGiants.put(orbitalSectors.get(index), giant);
                }
            }
        }
    }

    public SpaceGridMap<GasGiantSheet> getGiants(Vector3i systemPos){
        //Vector3i corner = new Vector3i(systemPos);
        //corner.add(64,64,64);

        try {
            Galaxy g;
            if(GameServerState.instance != null) g = getGalaxyFromSystemPosSafe(GameServer.getUniverse(),systemPos);
            //getGalaxyFromSystemPos() is a fucked method that can destroy universes if mods are involved. Don't use it.
            else g = GameClientState.instance.getCurrentGalaxy(); //eek
            if (g.getSystemTypeAt(systemPos) == VOID)
                return new SpaceGridMap<GasGiantSheet>();
        }
        catch(Exception ignore){return new SpaceGridMap<GasGiantSheet>();}

        if(!generatedGiants){
            //TODO: cache?
            generateGasGiants(systemPos);
            generatedGiants = true;
        }
        return gasGiants;
    }

    public void generateTerrestrials(Vector3i systemPos){ //ONLY DO THIS ON SERVER
        try {
            StellarSystem starSystem = getStellarSystemFromSecPosWithoutDangerousTmps(systemPos);
            Vector3i corner = new Vector3i(systemPos);
            corner.scale(SYSTEM_SIZE);
            Vector3i sector = new Vector3i();
            for (int z = 0, index = 0; z < VoidSystem.SYSTEM_SIZE; z++) {
                for (int y = 0; y < VoidSystem.SYSTEM_SIZE; y++) {
                    for (int x = 0; x < VoidSystem.SYSTEM_SIZE; x++) {
                        SectorInformation.SectorType sectorType = starSystem.getSectorType(index);
                        sector.set(corner);
                        sector.add(x, y, z);
                        if(sectorType == SectorInformation.SectorType.PLANET && !terrestrialPlanets.containsLocation(sector))
                            addTerrestrial(starSystem,new Vector3i(sector));
                        index++;
                    }
                }
            }
        generatedTerrestrialsOnServer = true;
        }
        catch(Exception ignore){
            System.err.println("[MOD][Resources ReSourced] Failed to generate terrestrial planets for " + systemPos);
            ignore.printStackTrace();
        }
    }

    public SpaceGridMap<TerrestrialSheet> getTerrestrials(Vector3i system){
        if(!generatedTerrestrialsOnServer && GameServer.getServerState() != null) generateTerrestrials(system);
        return terrestrialPlanets;
    }

    public void addTerrestrial(StellarSystem sys, Vector3i sectorPos){
        float temperature = sys.getTemperature(sectorPos);
        long galaxySeed = seeds.get(Galaxy.getContainingGalaxyFromSystemPos(sys.getPos(),new Vector3i()));
        RRSTerrestrialPlanetType type = pickTerrestrialType(temperature, sectorPos.hashCode() * galaxySeed, true, zone.isMinor());
        PassiveResourceSupplier[] sources = type.getResources(sectorPos.hashCode() * galaxySeed, sectorPos);
        float localModifier = getResourceModifier(sectorPos);
        if(zone.isMinor()) localModifier *= MINOR_ZONE_PLANET_RESOURCE_MODIFIER;
        for(PassiveResourceSupplier w : sources){
            w.passivePoolRegenRate *= localModifier;
        }
        container.setPassiveSources(sectorPos,sources);
        terrestrialPlanets.put(sectorPos,new TerrestrialSheet(type, sectorPos.hashCode(), resourceDensity));
    }

    public float getResourceModifier(Vector3i sector){
        Random rand = new Random(sector.hashCode());
        float gaussModifier = 1f + Math.min(GAUSSIAN_PASSIVE_RESOURCE_BONUS_CAP,(GAUSSIAN_PASSIVE_RESOURCE_BONUS_SIGMA * FastMath.abs((float) rand.nextGaussian())));
        return resourceDensity * gaussModifier;
    }

    public TerrestrialSheet getTerrestrial(Vector3i sectorPos) {
        if(!generatedTerrestrialsOnServer && GameServer.getServerState() != null) generateTerrestrials(StellarSystem.getPosFromSector(sectorPos, new Vector3i())); //TODO: Abs. pos? center origin? corner origin? who even knows at this point
        return terrestrialPlanets.get(sectorPos);
    }

    public boolean containsTerrestrialAt(Vector3i sectorCoordinates) {
        return terrestrialPlanets.containsLocation(sectorCoordinates);
    }

    public static float getTemperature(Vector3i sector) {
        //At the moment, all of the internal information here is static anyway...
        //So why wasn't the vanilla version of this method static? Was there a plan to have temperature radii vary per system?
        //Would make sense for different star colours/types I suppose but that should be in Galaxy anyway.
        //if so, for RRS purposes that functionality would be handled inside this class anyway, not VoidSystem... so no problem here :D

        int x = VoidSystem.localCoordinate(sector.x) - VoidSystem.SYSTEM_SIZE / 2;
        int y = VoidSystem.localCoordinate(sector.y) - VoidSystem.SYSTEM_SIZE / 2;
        int z = VoidSystem.localCoordinate(sector.z) - VoidSystem.SYSTEM_SIZE / 2;

        Vector3f max = new Vector3f(VoidSystem.SYSTEM_SIZE_HALF - 1, VoidSystem.SYSTEM_SIZE_HALF - 1, VoidSystem.SYSTEM_SIZE_HALF - 1);
        Vector3f actual = new Vector3f(x, y, z);

        float pc = Math.min(1, actual.length() / max.length());

        return 1.0f - pc;
    }

    @Override
    public String toString() {
        return "[System Sheet for " + systemClass.name() + " system, resource density " + resourceDensity + "]";
    }
}
