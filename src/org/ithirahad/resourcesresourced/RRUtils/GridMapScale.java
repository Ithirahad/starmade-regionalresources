package org.ithirahad.resourcesresourced.RRUtils;

import org.schema.game.server.data.Galaxy;

public enum GridMapScale {
    SYSTEM(16),
    GALAXY(Galaxy.size);

    private final int size;
    GridMapScale(int scale) {
        this.size = scale;
    }

    int GridMapScale(){
        return this.size;
    }
}
