package org.ithirahad.resourcesresourced.RRUtils;

import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

import java.util.Map;

/**
 Container class for utilities, such as bug workarounds, that should be removed at a later date.
 */
public class TemporaryUtils {
    public static void _fixInformationKeyData(){
        for (Map.Entry<Short, ElementInformation> next : ElementKeyMap.getInformationKeyMap().entrySet()) {
            Short keyId = next.getKey();
            ElementKeyMap.lodShapeArray[keyId] = next.getValue().hasLod();
            ElementKeyMap.factoryInfoArray[keyId] = ElementKeyMap.getFactorykeyset().contains(keyId);
        }
    }
}
