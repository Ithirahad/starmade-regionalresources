package org.ithirahad.resourcesresourced.RRUtils;

import api.DebugFile;
import api.mod.StarMod;
import api.network.packets.PacketUtil;
import api.utils.game.PlayerUtils;
import api.utils.game.chat.CommandInterface;
import org.ithirahad.resourcesresourced.RRSConfiguration;
import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.ithirahad.resourcesresourced.industry.Extractor;
import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import org.ithirahad.resourcesresourced.network.SystemScanInfo;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import javax.annotation.Nullable;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * STARMADE MOD
 * CREATOR: Max1M
 * DATE: 18.03.2022
 * TIME: 11:19
 */
public class DebugUI implements CommandInterface {
    public static Exception lastFactionBonusException = null;

    @Override
    public String getCommand() {
        return "RRS_debug";
    }

    @Override
    public String[] getAliases() {
        return new String[]{"rdb","resources_resourced_debugging_command_owo"};
    }

    @Override
    public String getDescription() {
        return "Debugging command for RRS\n" +
                "print wells : print all passiveresource suppliers\n" +
                "print exts : print all extractors\n" +
                "debug : toggle debug logging\n" +
                "recalc wells: recalculate extraction power for all wells\n" +
                "update well all: force update all suppliers\n" +
                "scan: run astromateric scanner with all addons for your position";
    }

    @Override
    public boolean isAdminOnly() {
        return true;
    }

    //TODO command for updating one or all extractors
    @Override
    public boolean onCommand(PlayerState playerState, String[] strings) {
        int l = strings.length;
        if (l == 1 &&strings[0].equalsIgnoreCase("debug")) {
            RRSConfiguration.DEBUG_LOGGING = !RRSConfiguration.DEBUG_LOGGING;
            echo("Set debug logging to: "+ RRSConfiguration.DEBUG_LOGGING,playerState);
            return true;
        }

        if (l==2 && strings[0].equalsIgnoreCase("recalc")) {
            int i = 0; int t = 0;
            StringBuilder b = new StringBuilder("Well recalc:\n");
            if (strings[1].equalsIgnoreCase("wells")) {
                for (PassiveResourceSupplier[] wellArr : ResourcesReSourced.container.getAllSources()) {
                    for (PassiveResourceSupplier p: wellArr) {
                        float before =p.getTotalExPower();
                        p.recalcExtractionPower();
                        float after = p.getTotalExPower();
                        if (before != after) {
                            b.append("changed: ").append(before).append(" -> ").append(after).append(" : ").append(p);
                            i++;
                        }
                        t++;
                    }
                }
                b.append("total changed: ").append(i).append(" of all: ").append(t);
                echo(b.toString(),playerState);
                return true;
            }
        }

        if (l == 2 && strings[0].equalsIgnoreCase("print")) {
            StringBuilder b = new StringBuilder();
            if (strings[1].equalsIgnoreCase("wells")) {

                b.append("All wells:########################################\n");
                for (PassiveResourceSupplier[] wellArr : ResourcesReSourced.container.getAllSources()) {
                    for (PassiveResourceSupplier p: wellArr) {
                        b.append(p).append("\n");
                    }
                    b.append("\n");
                }
                echo(b.toString(),playerState);
            }
            if (strings[1].equalsIgnoreCase("exts")) {
                b.append("All extractors:####################################\n");
                for (PassiveResourceSupplier[] wellArr : ResourcesReSourced.container.getAllSources()) {
                    for (PassiveResourceSupplier p: wellArr) {
                        if (p.getExtractorCount() >0) {
                            b.append(p).append("\n");
                            for (Extractor e : p.getAllExtractors())
                                b.append(e).append("\n");
                            b.append("----------\n");
                        }
                    }
                }
                echo(b.toString(),playerState);
            }
            return true;
        }
        if (l == 1 && strings[0].equalsIgnoreCase("scan")) {
            scanSystem(playerState.getCurrentSystem(),playerState.getCurrentSector(),playerState);
            echo("ran scanner for your position",playerState);
        }

        //update well all/x y z secondsAgo
        if (l>=3 && strings[0].equalsIgnoreCase("update")) {
            if (strings[1].equalsIgnoreCase("well")) {
                if (strings[2].equalsIgnoreCase("all")) {
                    long time = System.currentTimeMillis(); int t = 0;
                    if (l==4) {
                       t = (1000*Integer.parseInt(strings[3])); //t +x seconds
                    }
                    time += t;
                    for (PassiveResourceSupplier[] arr: ResourcesReSourced.container.getAllSources()) {
                        for (PassiveResourceSupplier well: arr) {
                            if (t < 0) {
                                well.setLastPassiveUpdate(time); //pretend extractor is running longer, than update to now againgit ba
                            } else {
                                well.updatePassiveResources(true, time);

                            }
                        }
                    }
                    echo("update all wells to "+(t/1000) + "sec from now",playerState );
                    return true;
                }
            }

        }
        return false;
    }

    @Override
    public void serverAction(@Nullable PlayerState playerState, String[] strings) {

    }

    @Override
    public StarMod getMod() {
        return ResourcesReSourced.modInstance;
    }

    private void echo(String mssg, PlayerState p) {
        DebugFile.log("ECHO "+p.getName()+": "+mssg);
        PlayerUtils.sendMessage(p,mssg);
    }

    /**
     *
     * @param millis
     * @param since true: since h,m  false: date format
     * @return
     */
    public static String millisToString(long millis, boolean since) {
        if (since) {
            long seconds = millis/1000;
            long h = seconds/(60*60);
            long m = (seconds%3600)/60;
            return String.format("  +%sh %smin",h,m);
        } else {
            if (millis > 0) {
                SimpleDateFormat sdf = new SimpleDateFormat("kk:mm - EEE, d. MMM");
                return sdf.format(new Date(millis));
            } else {
                return "NULL";
            }
        }
    }

    private void scanSystem(Vector3i system, Vector3i sector, PlayerState p) {
        SystemScanInfo response = new SystemScanInfo(
                system,
                sector,
                true,
                true);
        PacketUtil.sendPacket(p,response);
    }

    /**
     * will log message if debuglogging is on
     * @param message message, duh
     * @param force force log, ignore debug logging
     */
    public static void log(String message, boolean force) {
        if (force || RRSConfiguration.DEBUG_LOGGING) {
            message = "[RRS-DEBUG] "+message;
            DebugFile.log(message);
            System.out.println(message);
        }
    }
}
