package org.ithirahad.resourcesresourced.RRUtils;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import org.lwjgl.util.vector.Quaternion;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.ControlElementMapper;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.common.data.world.Universe;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.objects.Sendable;

import javax.annotation.Nullable;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import static org.schema.game.common.controller.database.DatabaseEntry.removePrefixWOException;

public class MiscUtils {
    public static void setPrivateField(String fieldName, Object targetInstance, Object newVal) {
        try {
            Class<?> cls = targetInstance.getClass();
            Field field = cls.getField(fieldName);
            field.setAccessible(true);
            field.set(targetInstance,newVal);
            field.setAccessible(false);
        } catch (NoSuchFieldException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Provided field name \"" + targetInstance.getClass().getName() + "." + fieldName + "\" does not correspond to any extant field in the target class.");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE GRANT AETHER"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Could not access target field \"" + targetInstance.getClass().getName() + "." + fieldName + "\".");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE GRANT COULOMB"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
        }
    }

    public static void setSuperclassPrivateField(Class cls, String fieldName, Object targetInstance, Object newVal) { //EdenCore compatibility measure, might be useful elsewhere
        try {
            Field field = cls.getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(targetInstance,newVal);
            field.setAccessible(false);
        } catch (NoSuchFieldException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Provided field name \"" + targetInstance.getClass().getName() + "." + fieldName + "\" does not correspond to any extant field in the target class.");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE GRANT AETHER"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Could not access target field \"" + targetInstance.getClass().getName() + "." + fieldName + "\".");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE GRANT COULOMB"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
        }
    }

    private static boolean containsField(Field[] declaredFields, String fieldName) {
        for(Field field : declaredFields) if(field.getName() == fieldName) return true;
        return false;
    }

    public static Object readPrivateField(String fieldName, Object targetInstance){
        try {
            Class<?> cls = targetInstance.getClass();
            Field field = cls.getDeclaredField(fieldName);
            field.setAccessible(true);
            Object rtn = field.get(targetInstance);
            field.setAccessible(false);
            return rtn;
        } catch (IllegalAccessException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Could not access target field \"" + targetInstance.getClass().getName() + "." + fieldName + "\".");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE ENIGMA COULOMB"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
            return null;
        } catch (NoSuchFieldException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Provided field name \"" + targetInstance.getClass().getName() + "." + fieldName + "\" does not correspond to any extant field in the target class.");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE ENIGMA AETHER"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
            return null;
        }
    }

    public static Object readSuperclassPrivateField(Class cls, String fieldName, Object targetInstance) {
        try {
            Field field = cls.getDeclaredField(fieldName);
            field.setAccessible(true);
            Object rtn = field.get(targetInstance);
            field.setAccessible(false);
            return rtn;
        } catch (IllegalAccessException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Could not access target field \"" + targetInstance.getClass().getName() + "." + fieldName + "\".");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE ENIGMA COULOMB"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
            return null;
        } catch (NoSuchFieldException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Provided field name \"" + targetInstance.getClass().getName() + "." + fieldName + "\" does not correspond to any extant field in the target's superclasses.");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE ENIGMA AETHER"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
            return null;
        }
    }

    public static Vector3i getGalaxyPosSafe(PlayerState player){
        Vector3i sysPos = new Vector3i();
        if (player != null) {
            if (player.isInTutorial() || player.isInPersonalSector() || player.isInTestSector()) {
                VoidSystem.getContainingSystem(new Vector3i(0, 0, 0), sysPos);
            } else {
                VoidSystem.getContainingSystem(player.getCurrentSector(), sysPos);
            }
        } //else let it be 0,0,0 for now
        return Galaxy.getContainingGalaxyFromSystemPos(sysPos, new Vector3i());
    }

    /**
    A version of Universe's internal method "getGalaxyFromSystemPos(vec3i sysPos)" that won't fuck your universe up if you call it during new galaxy gen! Yay!
     */
    public static Galaxy getGalaxyFromSystemPosSafe(Universe universe, Vector3i sysPos) {
        Vector3i galaxyPos = Galaxy.getContainingGalaxyFromSystemPos(sysPos, new Vector3i());
        return universe.getGalaxy(galaxyPos);
    }

    /**
     * Same deal here. Version of the Universe method that doesn't involve mutable tmp variables that can wreck the universe.
     */
    public static StellarSystem getStellarSystemFromSecPosWithoutDangerousTmps(Vector3i sectorPos) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Vector3i systemPos = VoidSystem.getContainingSystem(sectorPos, new Vector3i());
        Universe universe = GameServerState.instance.getUniverse();
        Object2ObjectOpenHashMap<Vector3i, VoidSystem> starSystemMap = (Object2ObjectOpenHashMap<Vector3i, VoidSystem>) readPrivateField("starSystemMap",universe);
        assert starSystemMap != null;
        VoidSystem starSystem = starSystemMap.get(systemPos);

        if (starSystem == null) {
            Vector3i galaxyCoords = Galaxy.getContainingGalaxyFromSystemPos(systemPos, new Vector3i());
            Object2ObjectOpenHashMap<Vector3i, Galaxy> galaxyMap = (Object2ObjectOpenHashMap<Vector3i, Galaxy>) readPrivateField("galaxyMap",universe);
            assert galaxyMap != null;
            Galaxy galaxyHere = galaxyMap.get(galaxyCoords);
            if(galaxyHere == null)
                galaxyHere = universe.getGalaxy(galaxyCoords);
            Method loadOrGenerateVoidSystem = Universe.class.getDeclaredMethod("loadOrGenerateVoidSystem", Vector3i.class, Galaxy.class);
            loadOrGenerateVoidSystem.setAccessible(true);
            starSystem = (VoidSystem) loadOrGenerateVoidSystem.invoke(universe, systemPos, galaxyHere);
            loadOrGenerateVoidSystem.setAccessible(false);
            starSystemMap.put(starSystem.getPos(), starSystem);
        }

        assert VoidSystem.getContainingSystem(sectorPos, new Vector3i()).equals(starSystem.getPos());

        return starSystem.getInternal(sectorPos);
    }

    public static final Vector3f forward = new Vector3f(0,0,-1);
    public static final Vector3f up = new Vector3f(0,1,0);

    public static Vector3f multiply(Vector3f vec, Matrix3f matrix) {
        Vector3f result = new Vector3f();
        Vector3f row = new Vector3f();
        matrix.getRow(1,row);
        result.x = vec.dot(row);
        matrix.getRow(2,row);
        result.y = vec.dot(row);
        matrix.getRow(3,row);
        result.z = vec.dot(row);
        return result;
    }

    public static Quat4f lookAt(Vector3f sourcePoint, Vector3f destPoint, boolean lookAway)
    {
        Vector3f forwardVector = new Vector3f(destPoint);
        forwardVector.sub(sourcePoint);
        if(forwardVector.lengthSquared() != 0) forwardVector.normalize();

        float dot = forward.dot(forwardVector);

        if (Math.abs(dot - (-1.0f)) < 0.000001f)
        {
            return new Quat4f(up.x, up.y, up.z, (float)Math.PI);
        }
        if (Math.abs(dot - (1.0f)) < 0.000001f)
        {
            return new Quat4f(0,0,0,1);
        }

        float rotAngle = (float)Math.acos(dot);
        Vector3f rotAxis = new Vector3f();
        rotAxis.cross(forward,forwardVector);
        if(rotAxis.lengthSquared() != 0) rotAxis.normalize();
        Quaternion qResult = quatFromAxisAngle(rotAxis, rotAngle);
        Quat4f result = new Quat4f(qResult.x, qResult.y, qResult.z, qResult.w);
        if(lookAway) result.inverse();
        return result;
    }

    public static Quaternion quatFromAxisAngle(Vector3f axis, float angle)
    {
        float halfAngle = angle * .5f;
        float s = (float)Math.sin(halfAngle);
        Quaternion q = new Quaternion();
        q.x = axis.x * s;
        q.y = axis.y * s;
        q.z = axis.z * s;
        q.w = (float)Math.cos(halfAngle);
        return q;
    }

    public static Quat4f quat4FromAxisAngle(Vector3f axis, float angle)
    {
        float halfAngle = angle * .5f;
        float s = (float)Math.sin(halfAngle);
        Quat4f q = new Quat4f();
        q.x = axis.x * s;
        q.y = axis.y * s;
        q.z = axis.z * s;
        q.w = (float)Math.cos(halfAngle);
        return q;
    }

    @Nullable
    public static Sendable getServerSendable(Sendable sendable) {
        int id = sendable.getId();
        boolean clientExists = GameClientState.instance != null;
        if(clientExists){
            return GameServerState.instance.getLocalAndRemoteObjectContainer().getLocalObjects().get(id);
        }
        return null;
    }

    public static <T extends Sendable> ArrayList<T> getServerSendables(ArrayList<T> sendables){
        ArrayList<T> result = new ArrayList<>();
        for(Sendable s : sendables){
            Sendable serverSendable = getServerSendable(s);
            if(serverSendable != null) result.add((T) serverSendable);
        }
        return result;
    }

    public static boolean existsInDBWithoutFluff(String uidWithPrefixAndLocation){
        String strippedID = removePrefixWOException(uidWithPrefixAndLocation);
        return existsInDB(strippedID);
    }

    public static boolean existsInDB(String uid){
        try {
            return GameServerState.instance.getDatabaseIndex().getTableManager().getEntityTable().getByUID(uid, 1).size() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    };

    public static float smoothstep(float from, float to, float x) {
        x = clamp((x - from) / (to - from), 0.0f, 1.0f);
        return x * x * (3 - 2 * x);
    }

    public static Vector3f smoothstep(Vector3f from, Vector3f to, float mix, Vector3f out) {
        out.set(smoothstep(from.x,to.x,mix),smoothstep(from.y,to.y,mix),smoothstep(from.z,to.z,mix));
        return out;
    }

    public static Vector4f smoothstep(Vector4f from, Vector4f to, float mix, Vector4f out) {
        out.set(smoothstep(from.x,to.x,mix),smoothstep(from.y,to.y,mix),smoothstep(from.z,to.z,mix),smoothstep(from.w,to.w,mix));
        return out;
    }

    public static float lerp(float from, float to, float mix) {
        return lerp(from,to,mix,true);
    }

    /**
     * Linearly interpolates between two values based on the third.
     * @param from The 'start' value
     * @param to The 'destination' value
     * @param mix The balance between the two that we are trying to find. Start is at 0.0; destination is at 1.0.
     * @param boundedAtTermini If true, do not allow overshooting/undershooting the end/start values. (i.e. clamp the mix to 0.0 and 1.0, inclusive)
     * @return
     */
    public static float lerp(float from, float to, float mix, boolean boundedAtTermini) {
        if(boundedAtTermini) mix = clamp(mix,0.0f,1.0f);
        return (from * (1f-mix)) + (to * mix);
    }

    public static Vector3f lerp(Vector3f from, Vector3f to, float mix){
        return new Vector3f(lerp(from.x,to.x,mix),lerp(from.y,to.y,mix),lerp(from.z,to.z,mix));
    }

    public static Vector4f lerp(Vector4f from, Vector4f to, float mix){
        return new Vector4f(lerp(from.x,to.x,mix),lerp(from.y,to.y,mix),lerp(from.z,to.z,mix),lerp(from.w,to.w,mix));
    }

    public static float clamp(float x, float lowerlimit, float upperlimit) {
        if (x < lowerlimit)
            x = lowerlimit;
        if (x > upperlimit)
            x = upperlimit;
        return x;
    }

    /**
     * Returns map of blocks linked to a specified block.
     * Use <code>SegmentController.getSegmentBuffer().getPointUnsave(absIndex)</code> to get the SegmentPiece at a given index.<br/><br/>
     *
     * <b>NOTE: StarMade can leave "ghost" entries in these hashmaps with empty hashsets, so in order to determine if a given block type is linked,
     * you must ensure that the hashmap not only contains a key, but that the stored <code>FastCopyLongOpenHashSet</code> is not empty.</b>
     * @param in The entity to look for linked blocks inside
     * @param linkedTo The abs. index of the "master" block
     * @return The map of absolute indices of blocks linked to the block at index "linkedTo", by type.
     */
    public static Short2ObjectOpenHashMap<FastCopyLongOpenHashSet> findLinkedBlocks(SegmentController in, long linkedTo){
        Short2ObjectOpenHashMap<FastCopyLongOpenHashSet> result = new Short2ObjectOpenHashMap<>();
        ControlElementMapper c = in.getControlElementMap().getControllingMap();
        if(c.containsKey(linkedTo)) result.putAll(c.get(linkedTo));
        return result;
    }

    /**
     * @param in The entity to look inside
     * @param absIndex The absolute index position of the storage block (cargo, factory, etc.)
     * @return the inventory held in this block position.
     */
    public static Inventory getInventoryAt(SegmentController in, long absIndex){
        if(in instanceof ManagedUsableSegmentController) {
            return ((ManagedUsableSegmentController<?>) in).getInventory(ElementCollection.getPosIndexFrom4(absIndex));
        } else return null;
    }
}
