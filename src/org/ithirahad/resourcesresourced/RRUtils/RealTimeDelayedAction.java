package org.ithirahad.resourcesresourced.RRUtils;

import api.utils.StarRunnable;

import static org.ithirahad.resourcesresourced.ResourcesReSourced.modInstance;

public abstract class RealTimeDelayedAction {
    private abstract static class IntervalRunnable extends StarRunnable{
        protected long interval = 0;
        protected void setInterval(long milliseconds){
            interval = milliseconds;
        }
    }

    public final IntervalRunnable runnable;
    private int repeats = -1;
    private boolean doFirstImmediately;

    public RealTimeDelayedAction(final long timeMs) {
        runnable = new IntervalRunnable() {
            boolean firstRun = true;
            long previousTime = System.nanoTime();
            long t;

            @Override
            public void run() {
                long currTime = System.nanoTime();
                int deltaT = (int) ((currTime - previousTime) / 1000000);
                previousTime = currTime;
                this.t += deltaT;
                if (this.t >= interval || doFirstImmediately && firstRun) {
                    doAction();
                    if(repeats > 0 || repeats == -1) {
                        if(repeats > 0) repeats--;
                        t = 0;
                    }
                    else if(repeats == 0){
                        this.cancel();
                    }
                }
                firstRun = false;
            }
        };
    }

    public RealTimeDelayedAction(final long timeMs, int repeats){
        this(timeMs);
        this.repeats = repeats;
    }

    public RealTimeDelayedAction(final long timeMs, int repeats, boolean skipFirstWait){
        this(timeMs, repeats);
        this.repeats = repeats;
        this.doFirstImmediately = skipFirstWait;
    }

    public void setInterval(long ms){
        runnable.setInterval(ms);
    }
    public abstract void doAction();

    public final void startCountdown(){
        runnable.runTimer(modInstance, 1);
    }

    public final void cancel(){
        repeats = 0;
        runnable.cancel();
    }
}
