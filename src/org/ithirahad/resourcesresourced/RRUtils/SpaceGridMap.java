package org.ithirahad.resourcesresourced.RRUtils;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.Galaxy;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

import static org.ithirahad.resourcesresourced.RRSConfiguration.DEBUG_LOGGING;

/**
 * STARMADE MOD
 * CREATOR: Ithirahad Ivrar'kiim
 * DATE: 01 Sept. 2020
 * TIME: an illusion
 *
 * This is a serializable, sparse 3D map that enforces a fixed dimensional size.
 * It can be used for adding special info to systems in galaxies, sectors in systems, or anywhere else it might be needed.
 *
 * Keep in mind that it is based on a simple LinkedHashMap, so I wouldn't recommend using it to store blockwise entity metadata
 * of any kind, unless you are CERTAIN that it will always be very sparse - and even then, exercise caution. StarMade itself uses
 * a more complicated tree structure to store blocks, and it does that for a very, very good reason. Entities get BIG.
 * Could be usable for (reasonably large) micro blocks inside one tile though, I suppose - e.g. logic mini tiles.
 */
public class SpaceGridMap<T> implements Serializable {
    private final LinkedHashMap<SerializableVector3i, T> map;
    private final SerializableVector3i maxCoord;
    private final SerializableVector3i minCoord;
    private final boolean useBounds;
    static int StellarSystemSize = 16;

    public SpaceGridMap()
    {
        this.map = new LinkedHashMap<>();
        maxCoord = new SerializableVector3i();
        minCoord = maxCoord;
        useBounds = false;
    }

    /**
     * Create an empty SpaceGridMap with defined 3D boundaries.
     * @param min The minimum key coordinates allowed by this SpaceGridMap.
     * @param max The maximum key coordinates allowed by this SpaceGridMap.
     */
    public SpaceGridMap(final Vector3i min, final Vector3i max){
        this.map = new LinkedHashMap<>();
        this.maxCoord = new SerializableVector3i(max);
        this.minCoord = new SerializableVector3i(min);
        useBounds = true;
    }

    /**
     * Create a SpaceGridMap with the same size and members as an existing SpaceGridMap.
     * @param map the source map to copy
     */
    public SpaceGridMap(SpaceGridMap<T> map){
        this.map = new LinkedHashMap<>(map.map);
        this.maxCoord = map.maxCoord;
        this.minCoord = map.minCoord;
        this.useBounds = map.useBounds;
    }

    /**
     * Create an empty, cubic SpaceGridMap with some dimension.
     * @param cubeSize the side length of the cube this map represents
     */
    public SpaceGridMap(int cubeSize, boolean centerOrigin){
        this(new Vector3i(), new Vector3i());
        if(centerOrigin){
            minCoord.set(-(cubeSize/2),-(cubeSize/2),-(cubeSize/2));
            maxCoord.set(cubeSize/2,cubeSize/2,cubeSize/2);
        }

        else {
            minCoord.set(0,0,0);
            maxCoord.set(cubeSize,cubeSize,cubeSize);
        }
    }

    /**
     * Create an empty SpaceGridMap with defined 3D boundaries.
     * @param x x-axis width of map
     * @param y y-axis height of map
     * @param z z-axis width of map
     */
    public SpaceGridMap(int x, int y, int z, boolean centerOrigin){
        this(new Vector3i(),new Vector3i());
        Vector3i dims = new Vector3i(x,y,z);
        if(centerOrigin){
            dims.div(2);
            maxCoord.set(dims);

            dims.scale(-1);
            minCoord.set(dims);
        }
        else {
            maxCoord.set(dims);
            minCoord.set(0,0,0);
        }
    }

    public SpaceGridMap(final GridMapScale mapScale, boolean centerOrigin){
        this.map = new LinkedHashMap<>();
        useBounds = true;
        if(mapScale == GridMapScale.GALAXY){
            if(centerOrigin){
                this.maxCoord = new SerializableVector3i(Galaxy.halfSize);
                this.minCoord = new SerializableVector3i(-Galaxy.halfSize);
            }
            else {
                this.maxCoord = new SerializableVector3i(Galaxy.size);
                this.minCoord = new SerializableVector3i(0);
            }
        }
        else /*if(mapScale == GridMapScale.SYSTEM)*/{
            if(centerOrigin){
                this.maxCoord = new SerializableVector3i(StellarSystemSize / 2);
                this.minCoord = new SerializableVector3i(StellarSystemSize / -2);
            }
            else {
                this.maxCoord = new SerializableVector3i(StellarSystemSize);
                this.minCoord = new SerializableVector3i(0);
            }
        }
    }

    public T get(Vector3i v) {
        return map.get(new SerializableVector3i(v));
    }

    public T get(int x, int y, int z) {
        return get(new Vector3i(x,y,z));
    }

    public void put(Vector3i location, T value) throws IllegalArgumentException {
        if(!useBounds || ((minCoord.x <= location.x) && (location.x <= maxCoord.x) && (minCoord.y <= location.y) && (location.y <= maxCoord.y) && (minCoord.z <= location.z) && (location.z <= maxCoord.z))){
            T previousValue = map.put(new SerializableVector3i(location), value);
            if(previousValue != null){
                if(DEBUG_LOGGING) System.err.println("[MOD][Resource ReSourced][WARNING] Replaced value " + previousValue.toString() + " in hashmap " + " at " + location + " with new value of " + value.toString() + "!");
            };
        }
        else
        {
            System.err.println("[MOD][Resource ReSourced][ERROR] Allowable coordinate range: " + this.minCoord.toString() + " to " + this.maxCoord.toString() + "; Attempted to place map entry at: " + location.toString() + "!");
            throw new IllegalArgumentException("[MOD][Resource ReSourced][ERROR] Attempting to add value outside of map bounds!");
        }
    }

    public void put(int x, int y, int z, T value) throws IllegalArgumentException {
        put(new Vector3i(x,y,z), value);
    }

    public void putIfAbsent(Vector3i location, T value){
        if(location != null && !map.containsKey(location)) put(location,value);
    }

    public void putIfAbsent(int x, int y, int z, T value){
        putIfAbsent(new Vector3i(x,y,z),value);
    }

    public void clear(){
            map.clear();
    }

    public void clear(int x) {
        for(SerializableVector3i key : map.keySet()) if(key.x == x) map.remove(key);
    }

    public void clear(int x, int y) {
        for(SerializableVector3i key : map.keySet()) if(key.x == x && key.y == y) map.remove(key);
    }

    public Set<SerializableVector3i> keySet() {
        return map.keySet();
    }

    public Collection<T> values() {
        return map.values();
    }

    public SerializableVector3i size() {
        return new SerializableVector3i(maxCoord);
    }

    public int count() {
        return map.size();
    }

    public boolean isEmpty(){
        return map.isEmpty();
    }

    public T remove(Vector3i key) {
        return map.remove(new SerializableVector3i(key));
    }

    public boolean containsXLayer(int x){
        for(SerializableVector3i key : map.keySet()) if(key.x == x) return true;
        return false;
    }

    public boolean containsYRow(int x, int y){
        for(SerializableVector3i key : map.keySet()) if(key.x == x && key.y == y) return true;
        return false;
    }

    public boolean containsLocation(Vector3i v){
        return map.containsKey(new SerializableVector3i(v));
    }

    public boolean containsLocation(int x, int y, int z){
        return containsLocation(new Vector3i(x,y,z));
    }

    public boolean containsValue(T value){
        return map.containsValue(value);
    }

    public HashMap<SerializableVector3i,T> toHashMap(){
        return map;
    }
}
