package org.ithirahad.resourcesresourced.listeners;

import api.common.GameCommon;
import api.listener.EventPriority;
import api.listener.Listener;
import api.listener.events.world.GalaxyFinishedGeneratingEvent;
import api.mod.StarLoader;
import api.mod.config.FileConfiguration;
import org.ithirahad.resourcesresourced.RRSConfiguration;
import org.ithirahad.resourcesresourced.RRUtils.SerializableVector3i;
import org.ithirahad.resourcesresourced.RRUtils.SpaceGridMap;
import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.ithirahad.resourcesresourced.events.RRSGalaxyGenCompleteEvent;
import org.ithirahad.resourcesresourced.universe.galaxy.ResourceZone;
import org.ithirahad.resourcesresourced.universe.galaxy.ZoneClass;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemClass;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemSheet;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.Galaxy;

import java.util.*;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static org.ithirahad.resourcesresourced.RRSConfiguration.*;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.*;
import static org.ithirahad.resourcesresourced.universe.galaxy.ZoneClass.RR_CORE;

public class RRSGalaxyGeneration extends Listener<GalaxyFinishedGeneratingEvent>{

    public RRSGalaxyGeneration(){
        super(EventPriority.MONITOR); //In reality, probably no mod should be trying to change the galaxy AFTER it generates, but just in case one does, we'll give it some time to do so.
    }

    @Override
    public void onEvent(GalaxyFinishedGeneratingEvent e) {

        synchronized(e.getGalaxy()) {
            if (warpSpaceIsPresentAndActive){
                Vector3i ws = new Vector3i(0,70,0);
                if(e.getGalaxy().galaxyPos.equals(ws)) {
                    ResourceZone warpSpace = new ResourceZone(e.getGalaxy(), "Warp Space", ZoneClass.WS_WARP, false, new ArrayList<Vector3i>());
                    //add nothing to this zone; always fallback to galactic void
                    SystemSheet voidPlaceholder = new SystemSheet(SystemClass.WARP_VOID, 0.0f, 0.0f, warpSpace, 0.0f);
                    GALACTIC_VOIDS.putIfAbsent(new Vector3i(ws), voidPlaceholder);
                    galaxiesDoneGenerating.add(ws);
                    System.err.println("[MOD][RRS][Interop] Successfully created WarpSpace placeholders.");
                    return;
                }
            }
        }

        if(GameCommon.isOnSinglePlayer()) {
            FileConfiguration config = modInstance.getConfig(RRSConfiguration.getConfigName());
            modInstance.updateInternalConfigValues(config,true);
        }

        if(galaxiesDoneGenerating.contains(e.getGalaxy().galaxyPos)){
            System.err.println("[MOD][RRS][WARNING] RRS information for galaxy at " + e.getGalaxy().galaxyPos + " already exists in this session! Aborting generation.");
            if(DEBUG_LOGGING) {
                System.err.println("[MOD][RRS]          (For those who may be curious why StarMade is generating the same galaxy over again, here's the stack trace for this generation round: )");
                StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
                for (int i = 0, stackTraceLength = stackTrace.length; i < stackTraceLength; i++) {
                    if (i >= 5) { //first six lines are always the same
                        StackTraceElement s = stackTrace[i];
                        System.err.print("  " + s.toString());
                    }
                }
            }
            return;
        }

        System.err.println("[MOD][RRS] Galaxy generation complete. Preparing to create galaxy regions...");
        final LinkedHashSet<Vector3i> ALL_GALAXY_STARS = new LinkedHashSet<>(ResourcesReSourced.availableStarsLists.get(e.getGalaxy().galaxyPos));
        Set<Vector3i> claimedVoids = new HashSet<>();
        List<Vector3i> unusedStarsOrig0 = filterVoids(ALL_GALAXY_STARS, e.getGalaxy()); //defensive programming against mysterious rogue voids

        long galaxySeed = seeds.get(e.getGalaxy().galaxyPos);
        final Random random_GALACTIC = new Random(galaxySeed);

        final Vector3i localCenterOrig64 = new Vector3i(Galaxy.halfSize, Galaxy.halfSize, Galaxy.halfSize);
        //TODO: check with non origin galaxies. Might need to add e.getGalaxy().galaxyPos * Galaxy.size depending on what some methods do w/ rel vs abs pos
        // (else this variable is pointless)
        Vector3i absCenterOrig0 = new Vector3i(localCenterOrig64); //first star in galaxy for region formation. reusable object.
        absCenterOrig0.sub(64,64,64);

        SerializableVector3i galaxyPos = new SerializableVector3i(e.getGalaxy().galaxyPos);

        SpaceGridMap<ResourceZone> zoneMap = new SpaceGridMap<>(); //map of zones to stars in this galaxy (by 'root'/principal star) - primarily just for the map.
        ArrayList<ResourceZone> zoneList = new ArrayList<>();

        //Generate zones to fill area
        System.err.println("[MOD][Resources ReSourced] Beginning zone generation for galaxy " + e.getGalaxy().galaxyPos.toString() + "; going for " + ((ZoneClass.RRClasses().length * MAJOR_RESOURCE_ZONES_PER_TYPE) + (USE_GALAXY_CORE_REGION? 1 : 0)) + " zones, using seed " + galaxySeed);

        Vector3i furthestStar = new Vector3i(); //inner/mid/outer zone band radii are computed based on this
        for(Vector3i star : unusedStarsOrig0) if(star.lengthSquared() > furthestStar.lengthSquared()) furthestStar.set(star);

        System.err.println("[MOD][Resources ReSourced] Located furthest star at " + furthestStar + ". Will use " + furthestStar.length() + " for reference maximum when building zone bands.");

        float galaxyCoreRadius;
        if(USE_GALAXY_CORE_REGION) galaxyCoreRadius = GALAXY_CORE_RADIUS;
        else galaxyCoreRadius = 0;
        float gc2 = galaxyCoreRadius*galaxyCoreRadius;

        float range = furthestStar.length() - galaxyCoreRadius;
        float midZoneTypeStart = (range*MID_ZONE_TYPE_START) + galaxyCoreRadius; //one-third mark
        int midZoneRadiusSquared = (int) (midZoneTypeStart * midZoneTypeStart);
        float outerZoneTypeStart = (range*FAR_ZONE_TYPE_START) + galaxyCoreRadius; //two-thirds mark
        int outerZoneRadiusSquared = (int) (outerZoneTypeStart * outerZoneTypeStart);
        //set up galaxy bands

        List<Vector3i> innerStars = new LinkedList<>(); //all center orig 0
        List<Vector3i> midStars = new LinkedList<>();
        List<Vector3i> outerStars = new LinkedList<>();

        for(Vector3i star : unusedStarsOrig0) {
            float dist2 = star.lengthSquared();
            if (dist2 > outerZoneRadiusSquared) outerStars.add(star);
            else if (dist2 > midZoneRadiusSquared) midStars.add(star);
            else if(!USE_GALAXY_CORE_REGION || dist2 > gc2) innerStars.add(star);
        }

        if(unusedStarsOrig0.size() > 0) {
            if (USE_GALAXY_CORE_REGION) {
                try {
                    ResourceZone core = new ResourceZone(e.getGalaxy(),galaxySeed, RR_CORE,false);
                    core.setPrincipalStar(absCenterOrig0);
                    ZoneSystemSet systems = doZoneStarCollectionPasses(core, unusedStarsOrig0, claimedVoids, 1, GALAXY_CORE_RADIUS, 0f); //steps, placement mode/stars to use, first look radius, subsequent look radius, trim
                    //maybe getPopulationFor zone pool should be a varargs lol

                    for (Vector3i star : systems.stars){
                        zoneMap.put(star, core);
                        unusedStarsOrig0.remove(star);
                        //don't need to worry about the other lists in this case
                    }
                    for (Vector3i voidBlock : systems.voids){
                        claimedVoids.add(voidBlock);
                        zoneMap.put(voidBlock, core);
                    }
                    core.add(systems.stars);
                    core.add(systems.voids);

                    System.err.println("[MOD][Resources ReSourced] Finished generating core zone successfully with " + core.getStarCount() + " system blocks.");
                    if (DEBUG_LOGGING) System.err.println("DEBUG: First star in core zone is of class: " + zoneMap.get(absCenterOrig0).zoneClass);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    System.err.println(); //no-op for breakpoint
                    System.exit(-1);
                }
            }

            ZoneClass[] rrClasses = ZoneClass.RRClasses();
            int zoneTypeQuantity = rrClasses.length;
            boolean generatingMinorZones = true;

            //This loop runs itself through twice: Once to generate minor zones, and once to generate major ones around them.

            for(int i = 1; i <= zoneTypeQuantity + 1; i++) {
                if(i>zoneTypeQuantity || !USE_MINOR_ZONES) { //skip right to major zone gen if not using the minor ones
                    if (generatingMinorZones) {
                        generatingMinorZones = false;
                        i = 0;
                        continue; //i++
                        //first pass done; commence second pass.
                        //this would be an arrayindexoutofboundsexception if allowed to continue -
                        // the "last index +1" loop is just a magic position for switching gen passes
                    } else break; //second pass done. this would also be an arrayindexoutofboundsexception if allowed to continue
                }

                ZoneClass zoneClass = rrClasses[i-1];
                System.err.println("[MOD][Resources ReSourced] Beginning generation of " + (generatingMinorZones? "minor ":"major ") + zoneClass.getDescriptor() + " zones.");

                int zoneCount;
                if(generatingMinorZones) zoneCount = Math.round(MINOR_RESOURCE_ZONES_PER_TYPE * zoneClass.getFrequencyMultiplier());
                else zoneCount = Math.round(MAJOR_RESOURCE_ZONES_PER_TYPE * zoneClass.getFrequencyMultiplier());

                for (int ii = 0; ii < zoneCount; ii++) {
                    int zoneLookSteps;
                    if(generatingMinorZones) zoneLookSteps = 1;
                    else zoneLookSteps = ZONE_SEARCH_BLOBS_MIN + random_GALACTIC.nextInt(ZONE_SEARCH_BLOBS_MAX - ZONE_SEARCH_BLOBS_MIN + 1);

                    List<Vector3i> allowedFirstStars;
                    float radiusModifier = 0f; //search dist. modifier based on search band
                    float baselineDist = furthestStar.length(); //TODO: might need to subtract galactic offset from furthest star.
                    //...abs vs rel pos is a whole other level of hell on top of orig0 vs orig64... I am not ready for this

                    if(generatingMinorZones) allowedFirstStars = unusedStarsOrig0;
                    else if (zoneClass.getPlacement() == ZoneClass.PlacementMode.INNER) allowedFirstStars = innerStars; //no radius mod
                    else if (zoneClass.getPlacement() == ZoneClass.PlacementMode.MID){
                        allowedFirstStars = midStars;
                        radiusModifier = FastMath.sqrt(midZoneRadiusSquared)/baselineDist;//avg. of bounds
                    }
                    else if (zoneClass.getPlacement() == ZoneClass.PlacementMode.OUTER){
                        allowedFirstStars = outerStars;
                        radiusModifier = (FastMath.sqrt(outerZoneRadiusSquared)/baselineDist);//outer bound
                    }
                    else allowedFirstStars = unusedStarsOrig0;

                    radiusModifier *= SEARCH_RADIUS_MODIFIER_FOR_DISTANCE_FROM_GALACTIC_CENTRE;
                    radiusModifier += 1;

                    ResourceZone zone = new ResourceZone(e.getGalaxy(),random_GALACTIC.nextLong(),zoneClass,generatingMinorZones);

                    System.err.println("[MOD][Resources ReSourced] Beginning generation of " + (generatingMinorZones? "minor ":"major ") + zoneClass.getDescriptor() + " zone \"" + zone.name + "\", with seed " + zone.seed);

                    ZoneSystemSet zoneSystems = getPopulationFor(
                            zone,
                    unusedStarsOrig0,
                    allowedFirstStars,
                            claimedVoids,
                    zoneLookSteps,
                generatingMinorZones? MINOR_ZONE_LOOK_RADIUS : ZONE_STAR_LOOK_RADIUS_INITIAL * radiusModifier * zoneClass.getRadiusMultiplier(),
            ZONE_STAR_LOOK_RADIUS_SECONDARY * radiusModifier * zoneClass.getRadiusMultiplier(),
                            radiusModifier,
                    generatingMinorZones? MINOR_ZONE_MIN_STAR_COUNT : MAJOR_ZONE_MIN_STAR_COUNT,
                    generatingMinorZones? MINOR_ZONE_MIN_STAR_COUNT + random_GALACTIC.nextInt(MINOR_ZONE_MAX_STAR_COUNT - MINOR_ZONE_MIN_STAR_COUNT) : MAJOR_ZONE_MAX_STAR_COUNT
                    );

                    for (Vector3i star : zoneSystems.stars){
                        zoneMap.put(star, zone);
                        allowedFirstStars.remove(star);
                        innerStars.remove(star);
                        midStars.remove(star);
                        outerStars.remove(star);
                        //"defensive" programming that doesn't even seem to work...?

                        unusedStarsOrig0.remove(star);
                    }

                    if(!generatingMinorZones)
                        for (Vector3i voidBlock : zoneSystems.voids) {
                            claimedVoids.add(voidBlock);
                            zoneMap.put(voidBlock, zone);
                        }

                    zoneList.add(zone);
                    System.err.println("[MOD][Resources ReSourced] Finished generating " + (generatingMinorZones? "minor":"major") + " zone \"" + zone.name + "\" of type " + zoneClass + " successfully with " + zoneSystems.stars.size() + " system blocks.");
                }
            }
            System.err.println("[MOD][Resources ReSourced] Finished generating resource zones.");
        }
        else System.err.println("[MOD][Resources ReSourced][WARNING] Galaxy " + galaxyPos + " is a completely empty galaxy. Unable to generate resource zones.");

        System.err.println("[MOD][Resources ReSourced] Creating unassociated space zone with " + unusedStarsOrig0.size() + " stars.");
        ResourceZone unassoc = new ResourceZone(e.getGalaxy(),"Galactic Barrens", ZoneClass.NONE, false, unusedStarsOrig0);
        zoneList.add(unassoc);
        for(Vector3i star : unusedStarsOrig0) zoneMap.put(star, unassoc);

        SystemSheet voidPlaceholder = new SystemSheet(SystemClass.NORMAL_VOID,0.0f, 0.0f, unassoc, 0.0f);
        GALACTIC_VOIDS.put(galaxyPos, voidPlaceholder);

        System.err.println("[MOD][Resources ReSourced] Finished creating unassociated space zone with " + unassoc.getStarCount() + " stars.");

        zoneMaps.put(galaxyPos, zoneMap);
        System.err.println("[MOD][Resources ReSourced] Finalized zone map for galaxy " + galaxyPos + " with " + zoneList.size() + " zones.");

        RRSGalaxyGenCompleteEvent ev = new RRSGalaxyGenCompleteEvent(e.getGalaxy(), zoneMap);
        StarLoader.fireEvent(ev, true);

        galaxiesDoneGenerating.add(new Vector3i(e.getGalaxy().galaxyPos));
    }

    /**
     * Attempt to get a population of stars fitting the parameters and zone type, starting from a given star.
     * @param zone The (preferably empty) resource zone to populate.
     * @param starPool Source of stars to pull from. If/when this routine succeeds, the claimed stars will be removed from this list.
     * @param rootPool Source of starting stars to pull from. If/when this routine succeeds, the claimed stars will be removed from this list.
     * @param usedVoids Voids already claimed by a different region. Ignore these when collecting voids for the new region itself.
     * @param lookSteps How many iterations to look for stars.
     * @param initialLookRadius Radius to carry out the first star search.
     * @param subsequentLookRadius Radius to carry out branching star searches.
     * @param maxStars The maximum number of stars. -1 for infinite. Any stars beyond this limit will be trimmed away.
     * @return The stars used to create the zone.
     */
    private ZoneSystemSet getPopulationFor(ResourceZone zone, List<Vector3i> starPool, List<Vector3i> rootPool,  Set<Vector3i> usedVoids, int lookSteps, float initialLookRadius, float subsequentLookRadius, float radiusModifier, int minStars, int maxStars) {
        boolean validZone;
        int tries = 0;
        ZoneSystemSet result;
        Random rng = new Random(zone.seed);
        if(maxStars == 1){
            Vector3i firstStar = rootPool.get(rng.nextInt(rootPool.size()));
            result = new ZoneSystemSet();
            result.addStar(firstStar);
            starPool.remove(firstStar);
            rootPool.remove(firstStar);
            zone.setPrincipalStar(firstStar);
            //special case for one-star zones. No need to go through extensive searches
        }
        else do {
            Vector3i firstStar = rootPool.get(rng.nextInt(rootPool.size()));
            zone.setPrincipalStar(firstStar);
            //Actually adding stars to a ResourceZone is a long procedure with lots of side effects, so...
            //we still have to use the zonesystemset container thing to be sure we don't waste ages adding and removing systemsheets and other superfluous nonsense
            result = doZoneStarCollectionPasses(zone, starPool, usedVoids, lookSteps, initialLookRadius, subsequentLookRadius);

            validZone = (result.stars.size() >= minStars);

            if (validZone) {
                if(maxStars > 0){
                    result.trim(maxStars,firstStar,starPool);
                }
                zone.add(result.stars);
                starPool.removeAll(result.stars);
                rootPool.removeAll(result.stars); //can't start from used stars

                if(!zone.isMinor()){
                    zone.add(result.voids);
                }
            }
            tries++; //incremented tries
            if (tries == 50) //TODO: configurable
                System.err.println("[MOD][Resources ReSourced] Unable to retrieve valid star collection for zone " + zone.name + " of type: " + zone.zoneClass.name());
        } while (!validZone && tries <= 50);
        if(result.stars.size() < minStars)
            System.err.println("[MOD][Resources ReSourced] Created undersized zone " + zone.name + " of type: " + zone.zoneClass.name());
        else System.err.println("[MOD][Resources ReSourced] Retrieved population of " + result.stars.size() + "stars.");
        return result;
    }

    /**
     *  Attempt to get a population of stars fitting the parameters and zone type, starting from a given star. If no stars are available, this will just return an empty system set.
     * @param zone The (preferably empty) resource zone to populate. Must have its principal star coordinates set already, as this will be the first star to search from.
     * @param starPool The stars available to draw from.
     * @param lookSteps How many iterations to look for stars.
     * @param initialLookRadius Radius to carry out the first star search.
     * @param subsequentLookRadius Radius to carry out branching star searches.
     * @return the stars used to create the zone
     */
    private ZoneSystemSet doZoneStarCollectionPasses(ResourceZone zone, Collection<Vector3i> starPool, Collection<Vector3i> usedVoids, int lookSteps, float initialLookRadius, float subsequentLookRadius) {
        ZoneSystemSet result = new ZoneSystemSet();
        if(zone.getPrincipalStar() == null){
            System.err.println("[MOD][Resources ReSourced][ERROR] Attempted to populate zone without setting a principal star first.");
            throw new UnsupportedOperationException();
        }
        Vector3i centerStarOrig0 = zone.getPrincipalStar();
        float lookRadius = initialLookRadius;
        Collection<Vector3i> starPoolTemp = new LinkedHashSet<>(starPool);
        ZoneSystemSet roundRetrieval;
        Random stepsRand = new Random(zone.seed);

        //TODO: known point of divergence
        System.err.println("[MOD][Resources ReSourced] Beginning star collection attempt from root location " + zone.getPrincipalStar() + ", with " + lookSteps + " search iterations.");

        for(int i = 0; i < lookSteps; i++){
            roundRetrieval = getZoneSystems(zone.galaxy,starPoolTemp,centerStarOrig0,lookRadius);
            result.addStars(roundRetrieval.stars);
            for(Vector3i v : roundRetrieval.voids){
                if(!usedVoids.contains(v) && !result.voids.contains(v)) result.addVoid(v);
            }
            starPoolTemp.removeAll(roundRetrieval.stars);

            lookRadius = subsequentLookRadius;
            if(i < lookSteps - 1) centerStarOrig0 = roundRetrieval.stars.get(stepsRand.nextInt(roundRetrieval.stars.size())); //somewhat costly operation, don't waste time


            System.err.println("[MOD][Resources ReSourced] Search iteration " + (i+1) + " turned up " + roundRetrieval.stars.size() + " stars.");
        }

        return result;
    }

    /**
     * @param g Galaxy within which to search
     * @param starSetToTakeFrom stars to pull from, in CENTER-ORIGIN.
     * @param centerStarOrig0 First star system, in CENTER-ORIGIN (origin 0,0,0) coordinates.
     * @param radius Radius within which to search for stars.
     * @return list of voids and non-void systems in radius, provided in CENTER-ORIGIN coordinates.
     */
    public ZoneSystemSet getZoneSystems(Galaxy g, Collection<Vector3i> starSetToTakeFrom, Vector3i centerStarOrig0, float radius){

        Vector3i centerStarOrig64 = new Vector3i(centerStarOrig0);
        centerStarOrig64.add(64,64,64);
        System.err.println("Attempting to find stars in " + radius + " radius of " + centerStarOrig0.toString() + ".");
        Vector3i probeOrig64 = new Vector3i();
        Set<Vector3i> stars = new LinkedHashSet<>();
        Set<Vector3i> voids = new LinkedHashSet<>();

        stars.add(centerStarOrig0);

        int boxdim = (int) Math.ceil(radius);
        int yLimitMin = max(0,centerStarOrig64.y - boxdim);
        int yLimitMax = min(Galaxy.size,centerStarOrig64.y + boxdim);

        int minY = centerStarOrig64.y;
        int maxY = centerStarOrig64.y; //ACTUAL min values

        for (int x = centerStarOrig64.x - boxdim; x <= centerStarOrig64.x + boxdim; x++) {
            for (int z = centerStarOrig64.z - boxdim; z <= centerStarOrig64.z + boxdim; z++) {
                for(int y = yLimitMin; y < yLimitMax; y++){
                    probeOrig64.set(x,y,z);
                    if(Vector3i.getDisatanceSquaredD(centerStarOrig64,probeOrig64) <= (radius * radius)){
                        Vector3i probeOrig0 = new Vector3i(probeOrig64.x - 64, probeOrig64.y - 64, probeOrig64.z - 64);
                        if(g.isVoid(probeOrig64)){
                            voids.add(probeOrig0);
                        }else {
                            if (starSetToTakeFrom.contains(probeOrig0)) {
                                starSetToTakeFrom.remove(probeOrig0);
                                Vector3i star = new Vector3i(probeOrig0);
                                stars.add(star);
                                if (y < minY) minY = y; //lower the floor
                                if (y > maxY) maxY = y; //raise the roof!
                            } //else System.err.println("Found a star, but not in pool!");
                        }
                    }
                }
            }
        }
        System.err.println("Found " + stars.size() + " stars and " + voids.size() + " voids in radius.");
        return new ZoneSystemSet(stars, voids);
    }

    List<Vector3i> filterVoids(Collection<Vector3i> origin, Galaxy galaxy){
        List<Vector3i> result = new LinkedList<>();
        Vector3i cposOrig64 = new Vector3i();
        for (Vector3i star : origin){
            cposOrig64.set(star);
            cposOrig64.add(64,64,64);
            if (!galaxy.isVoid(cposOrig64)) result.add(star);
        }
        return result;
    }

    private static class ZoneSystemSet {
        final LinkedList<Vector3i> stars;
        final LinkedList<Vector3i> voids;


        public ZoneSystemSet(Collection<Vector3i> stars, Collection<Vector3i> voids) {
            this.stars = new LinkedList<>(stars);
            this.voids = new LinkedList<>(voids);
        }

        public ZoneSystemSet() {
            this.stars = new LinkedList<>();
            this.voids = new LinkedList<>();
        }

        public void addStar(Vector3i star){stars.add(star);}
        public void addVoid(Vector3i pos){voids.add(pos);}
        public void add(ZoneSystemSet z){
            stars.addAll(z.stars);
            voids.addAll(z.voids);
        }
        /**
         * @param probeOrigin Place to measure distances from, in CENTER-ORIGIN coordinates. The closest stars to this point will be retained.
         * @param returnTo Any trimmed stars will be added to this list.
        Pares down the star count to the specified number (if lower than the star quantity we have to begin with), using those closest to the probeOrigin.
         */
        public void trim(int newSize, final Vector3i probeOrigin, Collection<Vector3i> returnTo) {
            if(newSize >= stars.size()) return;

            ArrayList<Vector3i> orderedStars = new ArrayList<>(stars);
            Collections.sort(orderedStars,new Comparator<Vector3i>() {
                @Override
                public int compare(Vector3i o1, Vector3i o2) {
                    return Float.compare(Vector3i.getDisatanceSquaredD(o1,probeOrigin),Vector3i.getDisatanceSquaredD(o2,probeOrigin));
                }
            });

            List<Vector3i> starsToEliminate = orderedStars.subList(newSize - 1, stars.size() - 1);
            if(returnTo != null) returnTo.addAll(starsToEliminate);
            //stars = Arrays.asList(Arrays.copyOf(stars.toArray(), newSize, Vector3i[].class));
            starsToEliminate.clear();
            stars.clear();
            stars.addAll(orderedStars);

            //this is probably inefficient but idk, those hashsets used to be arraylists and zones don't generate in realtime lol
        }

        public void clear() {
            stars.clear();
            voids.clear();
        }

        public void addStars(Collection<Vector3i> stars) {
            this.stars.addAll(stars);
        }

        public void addVoids(Collection<Vector3i> voids){
            this.voids.addAll(voids);
        }
    }
}
