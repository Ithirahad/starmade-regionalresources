package org.ithirahad.resourcesresourced.listeners;

import api.listener.Listener;
import api.listener.events.gui.HudCreateEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.GUITextOverlay;

/**
 * Created by Jake on 2/22/2021.
 * <insert description here>
 */
public class CustomAsteroidNameListener extends Listener<HudCreateEvent> {
    public CustomAsteroidNameListener() {
    }

    @Override
    public void onEvent(HudCreateEvent event) {
        GUITextOverlay textOverlay = event.getHud().getTargetPanel().getNameTextOverlay();
        textOverlay.setFont(FontLibrary.getBlenderProMedium20());
    }
}
