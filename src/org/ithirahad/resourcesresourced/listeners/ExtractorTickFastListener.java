package org.ithirahad.resourcesresourced.listeners;

import api.common.GameCommon;
import api.listener.fastevents.FactoryManufactureListener;
import api.mod.StarLoader;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import org.ithirahad.resourcesresourced.RRUtils.DebugUI;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.ithirahad.resourcesresourced.events.HarvesterStrengthUpdateEvent;
import org.ithirahad.resourcesresourced.industry.Extractor;
import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import org.ithirahad.resourcesresourced.industry.ResourceSourceType;
import org.ithirahad.resourcesresourced.vfx.ExtractorPulseEffect;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.controller.elements.UsableControllableElementManager;
import org.schema.game.common.controller.elements.cargo.CargoCollectionManager;
import org.schema.game.common.controller.elements.cargo.CargoElementManager;
import org.schema.game.common.controller.elements.cargo.CargoUnit;
import org.schema.game.common.controller.elements.factory.FactoryCollectionManager;
import org.schema.game.common.controller.elements.factory.FactoryProducerInterface;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.element.meta.RecipeInterface;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.movie.craterstudio.data.tuples.Pair;
import org.schema.schine.network.objects.Sendable;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.ithirahad.resourcesresourced.RRSConfiguration.DEBUG_LOGGING;
import static org.ithirahad.resourcesresourced.RRSConfiguration.EXTRACTION_RATE_PER_SECOND_PER_BLOCK;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.getServerSendables;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.container;
import static org.ithirahad.resourcesresourced.industry.ResourceSourceType.GROUND;
import static org.ithirahad.resourcesresourced.industry.ResourceSourceType.SPACE;
import static org.ithirahad.resourcesresourced.listeners.BlockRemovalLogic.isExtractor;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.*;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_ERROR;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class ExtractorTickFastListener implements FactoryManufactureListener {
    @Override
    public boolean onPreManufacture(FactoryCollectionManager factoryCollectionManager, Inventory inventory, LongOpenHashSet[] longOpenHashSets) {
        if (GameServerState.instance == null) return true;

        SegmentPiece block = inventory.getBlockIfPossible();
        if (block == null || !isExtractor(block.getType()))
            return true;

        ManagedUsableSegmentController<?> entity = (ManagedUsableSegmentController<?>) factoryCollectionManager.getSegmentController();
        if(entity.getType() == SHIP ||
        !entity.isFullyLoaded() ||
        entity.isMarkedForDeleteVolatile() ||
        entity.isWrittenForUnload() ||
        entity.isCoreOverheating()) return true;

        ManagerModuleCollection<CargoUnit, CargoCollectionManager, CargoElementManager> cargo = entity.getManagerContainer().getCargo();
        Boolean integrityChecked = (Boolean) MiscUtils.readSuperclassPrivateField(UsableControllableElementManager.class,"integrityChecked",cargo.getElementManager());
        if(integrityChecked == null || !integrityChecked) return true;
        //There is not a single flag anywhere in all of SM code that simply states if an entity or managermodulecollection
        // has updated at least once, so we have to use this bodge. integrityChecked happens to coincide with all the blocks having been counted up, so it works for our purposes.

        if ((block.getType() == elementEntries.get("Magmatic Extractor").id && (entity.getType() != PLANET_SEGMENT && entity.getType() != PLANET_ICO)))
            return false;
        //A magmatic extractor not placed on a planet is not (or should not be) doing anything. No need to waste any more time.

        boolean isPowered = factoryCollectionManager.consumePower(factoryCollectionManager.getElementManager());
        boolean hasCapacity = true;
        PassiveResourceSupplier[] localWells;
        Vector3i sector = entity.getSector(new Vector3i());
        ResourceSourceType type;

        //CRAPPY DEFENSIVE PROGRAMMING
        boolean wackError = false;
        String wackMessage = null;
        if(container == null){
            wackError = true;
            wackMessage = "[MOD][Resources ReSourced][ERROR]\r\nPassive Resource Provider Container Not Initialized!\r\n Please alert Ithirahad that his mod is broken.";
        }else if(sector == null){
            wackError = true;
            wackMessage = "[MOD][Resources ReSourced][ERROR]\r\nAttempted to extract resources from undetermined location!\r\n" +
                    "Entity does not know where it is.";
        }
        if(wackError){
            Collection<PlayerState> players = GameServerState.instance.getPlayerStatesByName().values();

            if (!GameCommon.isDedicatedServer()) players = getServerSendables(new ArrayList<>(players));

            for (PlayerState player : players) {
                player.sendServerMessage(Lng.astr(wackMessage), MESSAGE_TYPE_ERROR);
            }
            return false;
        }
        //END

        if (block.getType() == elementEntries.get("Magmatic Extractor").id){
            localWells = container.getPassiveSources(sector);
            type = GROUND;
        }
        else {
            localWells = container.getAllAvailableSourcesInArea(entity);
            type = SPACE;
        }

        String uid = entity.getUniqueIdentifier();

        if(DEBUG_LOGGING) System.out.println("################Ticking extractor-factory "+uid);


        for (PassiveResourceSupplier well : localWells) { //update all wells: collect stuff and create harvesters for unloaded colletion
            if(DEBUG_LOGGING) System.out.println("[MOD][Resources ReSourced] Extractor block on "+uid+" is beginning resource collection pass for " + ElementKeyMap.getInfo(well.resourceTypeId).name + ".");
            //ABORT IF: remove harvester if unpowered or not matching farmed ore type
            if(!isPowered) {
                if(DEBUG_LOGGING) System.out.println("[MOD][Resources ReSourced] Removing/ignoring extractor for " + ElementKeyMap.getInfo(well.resourceTypeId).name + " on cycle due to ground/space mismatch or lack of power to the extractor.");
                //TODO: Also on any other conditions for removal
                well.removeExtractor(uid);
                continue;
            } else if(well.type != type) continue;

            //defensive stuff
            if (uid == null || inventory == null || factoryCollectionManager == null || factoryCollectionManager.getSegmentController() == null || !factoryCollectionManager.getSegmentController().isFullyLoadedWithDock()) {
                DebugUI.log("Something with this resource well is horribly messed-up or null: "+ well,true);
                continue;
            }

            Extractor harvester;
            //add harvester if it doesn't exist yet
            if(!well.existsExtractor(uid)) {
                harvester = well.addExtractor(uid, type, sector);
                harvester.setCapacityVolume(inventory.getCapacity());
                harvester.setStrength(EXTRACTION_RATE_PER_SECOND_PER_BLOCK); // TEMP INITIALIZATION VALUE - overwritten at end of pass.
                // TODO: can we move the actual value update back up here? If this change fixes the harvest issue don't touch it, else move it back up
                if(DEBUG_LOGGING) System.out.println("[MOD][Resources ReSourced] This extractor was not registered! Newly registering extractor to resource well.");
            } else  //get existing
                harvester = well.getExtractor(uid);

            harvester.setFaction(entity.getFactionId());
            well.updatePassiveResources(true, System.currentTimeMillis());

            //collected farmed stuff from the harvester, put into physical factory
            double capacity = inventory.getCapacity();
            double volumeUsed = inventory.getVolume();
            double availableCapacity = Math.max(0,capacity - volumeUsed);

            float volPerUnit = Math.max(ElementKeyMap.getInfo(well.resourceTypeId).getVolume(),0.00000000001f); //no dividing by zero, even if something's misconfigured...

            int materials = well.claimPassiveResources(uid, availableCapacity);
            double volumeOfMaterials = materials * volPerUnit;
            if(DEBUG_LOGGING)System.out.println("Receiving " + materials + " materials, into inventory capacity of " + inventory.getCapacity() + "...");

            if(materials > 0) {
                int inventoryChange = inventory.incExistingOrNextFreeSlotWithoutException(well.resourceTypeId, materials);
                inventory.sendInventoryModification(inventoryChange);
            }
            else if(materials < 0) System.err.println("[MOD][Resources ReSourced][WARNING] Attempted to send negative inventory modification to extractor. Operation cancelled.");

            //update remaining inventory size on harvester
            harvester.setCapacityVolume(availableCapacity);
            //System.out.println(well);
            if(DEBUG_LOGGING) {
                System.out.println("[MOD][Resources ReSourced] Updated extractor storage capacity to " + capacity + " ("+inventory.getCapacity()+" - "+inventory.getVolume()+").");
                System.out.println(harvester);
                System.out.println("[MOD][Resources ReSourced] Finished extractor collection pass.");
                System.out.println("[MOD][Resources ReSourced] Final material value obtained: " + materials);
            }

            //Finally, update info.
            // We have to do this AFTER running the resource collection,
            // so that malformed info (e.g. not-yet-loaded chamber) won't nuke a bunch of resources

            float harvesterStrength;

            HarvesterStrengthUpdateEvent hse = new HarvesterStrengthUpdateEvent(factoryCollectionManager.getFactoryCapability(), EXTRACTION_RATE_PER_SECOND_PER_BLOCK, entity);
            StarLoader.fireEvent(hse, true);
            harvesterStrength = hse.getPower();

            harvester.setStrength(harvesterStrength);
            // Save the new harvester strength

            int entityMiningBonus = entity.getConfigManager().apply(StatusEffectType.MINING_BONUS_ACTIVE, 1);
            harvester.setEntityMiningBonus(entityMiningBonus);

            // Faction bonus calculation is run on virtual collection, as it is reliable no matter what state the entity is in
            // and can even be simulated while unloaded later on.
            // Entity bonus has to be updated AFTER every pass, however.
            /* We have to do this last because apparently, the factory can tick during loading
             * and we see no reliable way to be sure that the chamber system has already initialized
             * by the time the factory runs.
             * For instance, if the chamber can't be found, or the station's native mining bonus is 1,
             * is it actually not there or has the chamber tree just not built itself yet?
             */
        }

        if(block.getType() == elementEntries.get("Vapor Siphon").id && isPowered && hasCapacity) { //do vfx
            //Vector3f endpoint = new Vector3f(entity.getWorldTransform().origin);
            Vector3f endpoint = block.getWorldPos(new Vector3f(), entity.getSectorId());
            Vector3f startpoint;
            String currentSectorSize = ServerConfig.SECTOR_SIZE.getCurrentState().toString();
            //Vector3f blockOffset = new Vector3i(block.x,block.y,block.z).toVector3f();
            //endpoint.add(blockOffset);

            List<Pair<Vector3i, Vector4f>> fx = container.getSpaceSourceColorsAndLocationsForDisplay(entity);
            for (Pair<Vector3i, Vector4f> entry : fx){
                if(entry.first() == sector) startpoint = new Vector3f(0,0,0); //same sector, so assume centre of sector for now
                else{
                    startpoint = new Vector3f(entry.first().toVector3f());
                    startpoint.scale(Float.parseFloat(currentSectorSize));
                }
                ExtractorPulseEffect.fireEffectServer(entity.getSectorId(), sector, startpoint, endpoint, entry.second());
            }
        } //TODO: else magma extraction effect, whatever that would look like
        return false;
    }

    @Override
    public void onProduceItem(RecipeInterface recipeInterface, Inventory inventory, FactoryProducerInterface factoryProducerInterface, FactoryResource factoryResource, int i, IntCollection intCollection) {
        //nothing ever happens
        //and i wonder.
    }
}