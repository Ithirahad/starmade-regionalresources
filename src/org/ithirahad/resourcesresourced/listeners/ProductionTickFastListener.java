package org.ithirahad.resourcesresourced.listeners;

import api.listener.fastevents.FactoryManufactureListener;
import api.mod.StarLoader;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import org.ithirahad.resourcesresourced.events.MacetineRefineListCompileEvent;
import org.schema.game.common.controller.elements.factory.FactoryCollectionManager;
import org.schema.game.common.controller.elements.factory.FactoryProducerInterface;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.element.meta.RecipeInterface;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.server.data.GameServerState;

import java.util.Arrays;
import java.util.Random;

import static org.ithirahad.resourcesresourced.RRSConfiguration.COMPONENT_RECYCLE_FAIL_RATE;
import static org.ithirahad.resourcesresourced.RRSConfiguration.REFINING_MACETINE_BYPRODUCT_RATE;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;

public class ProductionTickFastListener implements FactoryManufactureListener {
    Random rng = new Random();
    private static short[] macetineSideproductWhiteList = null;

    @Override
    public boolean onPreManufacture(FactoryCollectionManager factoryCollectionManager, Inventory inventory, LongOpenHashSet[] longOpenHashSets) {
        return true;
    }

    @Override
    public void onProduceItem(RecipeInterface recipeInterface, Inventory inventory, FactoryProducerInterface producer, FactoryResource product, int quantity, IntCollection changeSet) {
        if(GameServerState.instance == null) return;
        if(macetineSideproductWhiteList == null){
            macetineSideproductWhiteList = new short[]{
                    elementEntries.get("Threns Capsule").id,
                    elementEntries.get("Aegium Capsule").id,
                    elementEntries.get("Ferron Capsule").id,
                    elementEntries.get("Parsyne Capsule").id,
                    elementEntries.get("Anbaric Capsule").id,
                    elementEntries.get("Thermyn Capsule").id,
                    //no macetine, basic metal/crystal, or mysterious back reactions >:(
            }; //I'm fully aware that this sort of on-demand initialization is terrible practice, but probably fine in this context
            ShortArrayList entries = ShortArrayList.wrap(macetineSideproductWhiteList);
            MacetineRefineListCompileEvent ev = new MacetineRefineListCompileEvent(entries);
            StarLoader.fireEvent(ev, true);
            ev.getRefineList().toArray(macetineSideproductWhiteList);
        }


        SegmentPiece factoryBlock = inventory.getBlockIfPossible();
        if(factoryBlock != null) {
            short factoryElementID = factoryBlock.getType();
            if (factoryElementID == elementEntries.get("Component Recycler").id) {
                if (rng.nextFloat() <= COMPONENT_RECYCLE_FAIL_RATE){
                    changeSet.add(inventory.incExistingOrNextFreeSlotWithoutException(product.type, -product.count));
                    if(product.type == ElementKeyMap.METAL_MESH || product.type == elementEntries.get("Ferron Capsule").id || product.type == elementEntries.get("Threns Capsule").id) changeSet.add(inventory.incExistingOrNextFreeSlotWithoutException(ElementKeyMap.SCRAP_ALLOYS, producer.getFactoryCapability()));
                    else if(product.type == ElementKeyMap.CRYSTAL_CRIRCUITS || product.type == elementEntries.get("Aegium Capsule").id) changeSet.add(inventory.incExistingOrNextFreeSlotWithoutException(ElementKeyMap.SCRAP_COMPOSITE, producer.getFactoryCapability()));
                    //else produce generic gas? Either way, component recycle fails should have their own event tbh
                }
            } else { //TODO: ...really, it's "else if is refinery" - not that macetine-whitelisted things can be made anywhere else ideally - but there's an event for other mods to hook into, so who knows?
                boolean whitelistedProduction = false;
                for(short id : macetineSideproductWhiteList)
                    if (id == product.type) {
                        whitelistedProduction = true;
                        break;
                    }
                if (factoryElementID == ElementKeyMap.FACTORY_CAPSULE_ASSEMBLER_ID && whitelistedProduction) {
                    int inventoryChange;
                    float idealMacetineYield = quantity * REFINING_MACETINE_BYPRODUCT_RATE; //TODO: uhm, this might not be working as intended
                    if (idealMacetineYield > 1) { //we can just add a steady byproduct yield every time
                        inventoryChange = inventory.incExistingOrNextFreeSlotWithoutException(elementEntries.get("Macetine Aggregate").id, (int) idealMacetineYield);
                        changeSet.add(inventoryChange);
                    } else if (rng.nextFloat() <= idealMacetineYield) { //randomly produce a single unit of macetine
                        inventoryChange = inventory.incExistingOrNextFreeSlotWithoutException(elementEntries.get("Macetine Aggregate").id, 1);
                        changeSet.add(inventoryChange);
                    }
                }
            }
        }
    }
}
