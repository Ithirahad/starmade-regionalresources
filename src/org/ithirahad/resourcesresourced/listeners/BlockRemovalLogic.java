package org.ithirahad.resourcesresourced.listeners;

import api.listener.Listener;
import api.listener.events.block.SegmentPieceAddByMetadataEvent;
import api.listener.events.block.SegmentPieceAddEvent;
import api.listener.events.block.SegmentPieceRemoveEvent;
import api.listener.events.entity.SegmentControllerOverheatEvent;
import api.listener.events.world.ServerSendableRemoveEvent;
import api.listener.fastevents.FastListenerCommon;
import api.listener.fastevents.segmentpiece.SegmentPieceKilledListener;
import api.listener.fastevents.segmentpiece.SegmentPieceRemoveListener;
import api.mod.StarLoader;
import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import org.ithirahad.resourcesresourced.industry.ResourceSourceType;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.movie.craterstudio.data.tuples.Pair;

import javax.annotation.Nullable;

import static api.mod.StarLoader.registerListener;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.container;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.getSystemSheet;
import static org.ithirahad.resourcesresourced.industry.ResourceSourceType.GROUND;
import static org.ithirahad.resourcesresourced.industry.ResourceSourceType.SPACE;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.*;

/**
 * Created by Jake on 2/24/2021.
 * Modified by Ithirahad in Mar-Apr 2021.
 */
public class BlockRemovalLogic {
    public static void onRemove(SegmentController entity, short blockType){
        if((GameServerState.instance != null) && isExtractor(blockType)) //no need to do this stuff on dedicated clients
        {
            if(blockType == vaporSiphon) clearExtractors(entity,SPACE);
            else if(blockType == magmaExtractor) clearExtractors(entity,GROUND);
            else clearExtractors(entity);
        }
    }

    private static void clearExtractors(SegmentController entity,ResourceSourceType type) {
        String thisEntityUID = entity.getUniqueIdentifier();
        PassiveResourceSupplier[] suppliers = container.getAllAvailableSourcesInArea(entity);

        for (PassiveResourceSupplier well : suppliers) if (well != null && (type == null || well.type == type)) {
            well.removeExtractor(thisEntityUID);
        }
    }

    private static void clearExtractors(SegmentController entity) {
        clearExtractors(entity,null);
    }

    public static void initListeners(){
        StarLoader.registerListener(SegmentPieceRemoveEvent.class, new Listener<SegmentPieceRemoveEvent>() {
            @Override
            public void onEvent(SegmentPieceRemoveEvent event) {
                if(event.isServer()) onRemove(event.getSegment().getSegmentController(),event.getType());
            }
        }, ResourcesReSourced.modInstance);

        FastListenerCommon.segmentPieceKilledListeners.add(new SegmentPieceKilledListener(){
            @Override
            public void onBlockKilled(SegmentPiece segmentPiece, SendableSegmentController c, @Nullable Damager damager, boolean b) {
                if(c.isOnServer()) onRemove(segmentPiece.getSegmentController(),segmentPiece.getType());
            }
        });

        FastListenerCommon.segmentPieceRemoveListeners.add(new SegmentPieceRemoveListener() {
            @Override
            public void onBlockRemove(short type, int segmentSize, byte x, byte y, byte z, byte orientation, Segment segment, boolean preserveControl, boolean isServer) {
                if(isServer) onRemove(segment.getSegmentController(),type);
            }
        });

        StarLoader.registerListener(SegmentControllerOverheatEvent.class, new Listener<SegmentControllerOverheatEvent>() {
            @Override
            public void onEvent(SegmentControllerOverheatEvent event) {
                if(event.isServer() && event.getEntity().getType() != SHIP) clearExtractors(event.getEntity());
            }
        }, ResourcesReSourced.modInstance);

        StarLoader.registerListener(ServerSendableRemoveEvent.class, new Listener<ServerSendableRemoveEvent>() {
            @Override
            public void onEvent(ServerSendableRemoveEvent event) {
                if(event.isServer() && event.getSendable() instanceof SegmentController && ((SegmentController)event.getSendable()).getType() != SHIP){
                    SegmentController entity = (SegmentController) event.getSendable();
                    Sector sec = entity.getRemoteSector().getServerSector();
                    if(sec.isActive() || entity.isCoreOverheating())
                        clearExtractors(entity,null);
                }
            }
        }, ResourcesReSourced.modInstance);

        registerListener(SegmentPieceAddEvent.class, new Listener<SegmentPieceAddEvent>() {
            @Override
            public void onEvent(SegmentPieceAddEvent e) {
                short b = e.getNewType();
                SimpleTransformableSendableObject.EntityType etype = e.getSegmentController().getType();
                if (GameServerState.instance != null && ((ElementKeyMap.getInfo(b).factory != null && etype == ASTEROID_MANAGED) || (isExtractor(b) && etype == SHIP))) {
                    SegmentPieceCleanupRunner.cleanupQueue.enqueue(new Pair<>(new Vector3b(e.getX(), e.getY(), e.getZ()), e.getSegment()));
                }
            }
        }, ResourcesReSourced.modInstance);

        registerListener(SegmentPieceAddByMetadataEvent.class, new Listener<SegmentPieceAddByMetadataEvent>() {
            @Override
            public void onEvent(SegmentPieceAddByMetadataEvent e) {
                short b = e.getType();
                SimpleTransformableSendableObject.EntityType etype = e.getSegment().getSegmentController().getType();
                if (GameServerState.instance != null && ((ElementKeyMap.getInfo(b).factory != null && etype == ASTEROID_MANAGED) || (isExtractor(b) && etype == SHIP))) {
                    SegmentPieceCleanupRunner.cleanupQueue.enqueue(new Pair<>(new Vector3b(e.getX(), e.getY(), e.getZ()), e.getSegment()));
                }
            }
        }, ResourcesReSourced.modInstance);
    }
    /*
    public static void tryPlaceExtractor(SegmentPieceAddInfo info, boolean playerPlacement) {
        if(GameServerState.instance != null && info.segment.getSegmentController().getType() != SHIP) {
            SegmentController controller = info.segment.getSegmentController();
            ResourceSourceType thisExtractorType = info.type == elementEntries.get("Magmatic Extractor").id ? GROUND : SPACE;
            if (!(controller.getType() == PLANET_SEGMENT || controller.getType() == PLANET_ICO) && thisExtractorType == GROUND)
                return;
            //magma extractor not on a planet; can't possibly do anything.

            long absIndex = info.absIndex;
            Vector3i sys = controller.getSystem(new Vector3i());
            SystemSheet sheet = getSystemSheet(Galaxy.getContainingGalaxyFromSystemPos(sys, new Vector3i()), sys);
            assert sheet != null;

            addExtractorIfAbsent(controller.getSector(new Vector3i()), controller, thisExtractorType, absIndex);
            //There was a second type check here to filter out magmatic extractors on stations/asteroids/etc., but it was kind of redundant.
        }
    }
     */

    /**
     * Checks for the presence of extractor blocks of a given type.
     * @param entity The entity to check
     * @param type Whether to look for ground or space extractors
     */
    static int numberOfExtractors(SegmentController entity, ResourceSourceType type) {
        ElementCountMap allBlocksOnEntity = entity.getElementClassCountMap();
        int extractorCount;
        if(type == SPACE) extractorCount = allBlocksOnEntity.get(elementEntries.get("Vapor Siphon").id);
        else extractorCount = allBlocksOnEntity.get(elementEntries.get("Magmatic Extractor").id);
        return extractorCount;
    }

    public static Short vaporSiphon = null;
    public static Short magmaExtractor = null;
    public static boolean isExtractor(short id){
        if(vaporSiphon == null) vaporSiphon = elementEntries.get("Vapor Siphon").id;
        if(magmaExtractor == null) magmaExtractor = elementEntries.get("Magmatic Extractor").id;
        return (id == vaporSiphon || id == magmaExtractor);
    }

    public static SegmentPiece getBlockFromSegment(Segment segment, Vector3b tileLoc, @Nullable SegmentPiece segmentPieceTmp) {
        if (segmentPieceTmp == null) {
            segmentPieceTmp = new SegmentPiece();
        }

        segmentPieceTmp.setByReference(segment, tileLoc.x, tileLoc.y, tileLoc.z);
        return segmentPieceTmp;
    }

    public static Vector3i getStarResourceSector(Vector3i system){
        Vector3i sector = new Vector3i(system);
        sector.scale(VoidSystem.SYSTEM_SIZE);
        sector.add(8,8,8); //center of system(?) Doesn't really matter tbh; could use the corner
        return sector;
    }

    public static class SegmentPieceAddInfo {
        //public PlayerState playerState;
        public short type;
        public Segment segment;
        public byte x;
        public byte y;
        public byte z;
        public long absIndex;

        public SegmentPieceAddInfo(){}

        public SegmentPieceAddInfo(short type, long absIndex, byte x, byte y, byte z, Segment segment) {
            this.type = type;
            this.absIndex = absIndex;
            this.x = x;
            this.y = y;
            this.z = z;
            this.segment = segment;
        }

        public SegmentPieceAddInfo(SegmentPieceAddEvent e) {
            this(e.getNewType(), e.getAbsIndex(), e.getX(), e.getY(), e.getZ(), e.getSegment());
        }
    }
}

