package org.ithirahad.resourcesresourced.listeners;

import api.common.GameCommon;
import api.common.GameServer;
import api.utils.StarRunnable;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.objects.Sendable;

import java.util.Iterator;
import java.util.LinkedList;

import static org.ithirahad.resourcesresourced.industry.ShopPricingManager.setNPShopPricesIfApplicable;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_ERROR;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_WARNING;

public class ShopPriceUpdateRunner extends StarRunnable {
    public static final LinkedList<SegmentController> shopsToUpdate = new LinkedList<>();
    private static int timer = 0;

    @Override
    public void run() {
        if (!shopsToUpdate.isEmpty()) {
            Iterator<SegmentController> it = shopsToUpdate.iterator();
            while(it.hasNext()){
                SegmentController entity = it.next();
                Sendable entAsSendable = (Sendable) entity.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(entity.getSectorId());
                if (entAsSendable instanceof RemoteSector /*this is a relevant null check AND a typecheck*/)
                //This strange incantation is a copy of some SimpleTransformableSendableObject code used to verify that an object doesn't have "null" position
                // which is rather important given that a shop needs a position in order to get position-based pricing :P :P
                {
                    setNPShopPricesIfApplicable(entity);
                    it.remove();
                }
                else if(entity.isMarkedForDeleteVolatile() || entity.isMarkedForPermanentDelete() || entity.isWrittenForUnload()){
                    it.remove(); //honestly IDK what these features do, but presumably if it's about to be unloaded or removed, the queue shouldn't be holding it in memory
                }
            }

            //TODO: This can snowball and hurt performance
            if(GameServerState.instance != null){
                if(shopsToUpdate.size() > 7 * GameServer.getServerState().getClients().size()) //more than 1 per loaded sector set for a player
                    if(timer > 10000){
                        for (RegisteredClientOnServer client : GameServer.getServerState().getClients().values()) {
                            try {
                                PlayerState player =  GameServer.getServerState().getPlayerFromName(client.getPlayerName());
                                if(player.isAdmin()) player.sendServerMessage(Lng.astr("[SERVER][WARNING] RRS shop queue is beginning to pile up disproportionately to player count. Performance may be affected. If this message persists, please report this to Ithirahad."), MESSAGE_TYPE_WARNING);
                            } catch (PlayerNotFountException ignore) {}
                        }
                        timer = 0;
                    }
                    else timer++;
                }
        }
    }
}
