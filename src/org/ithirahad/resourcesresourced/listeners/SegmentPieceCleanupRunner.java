package org.ithirahad.resourcesresourced.listeners;

import api.utils.StarRunnable;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.game.common.data.world.Segment;
import org.schema.schine.graphicsengine.movie.craterstudio.data.tuples.Pair;

public class SegmentPieceCleanupRunner extends StarRunnable {
    public static final ObjectArrayFIFOQueue<Pair<Vector3b, Segment>> cleanupQueue = new ObjectArrayFIFOQueue<>();

    @Override
    public void run() {
        while(!cleanupQueue.isEmpty()){
            Pair<Vector3b, Segment> block = cleanupQueue.dequeue();
            Segment seg = block.second();
            Vector3b index = block.first();
            seg.removeElement(index.x,index.y,index.z,true);
            //TODO: Yell at everyone in sector
        }
    }
}
