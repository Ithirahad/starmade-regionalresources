package org.ithirahad.resourcesresourced.listeners;

import api.common.GameCommon;
import api.listener.fastevents.SectorUpdateListener;
import api.mod.ModStarter;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import org.ithirahad.resourcesresourced.RRUtils.SpaceGridMap;
import org.ithirahad.resourcesresourced.universe.gasplanet.GasGiantSheet;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemSheet;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.Sector;
import org.schema.game.server.data.Galaxy;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.NetworkStateContainer;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.objects.Sendable;

import javax.vecmath.Vector3f;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import static java.lang.Math.*;
import static org.ithirahad.resourcesresourced.RRSConfiguration.*;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.galaxiesDoneGenerating;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.getSystemSheet;
import static org.ithirahad.resourcesresourced.universe.gasplanet.GasGiantSheet.BASE_GIANT_SIZE;
import static org.ithirahad.resourcesresourced.universe.gasplanet.GasPlanetDrawer.GIANT_LOWER_SURFACE;
import static org.ithirahad.resourcesresourced.universe.gasplanet.GasPlanetDrawer.NEAR_FORCED_PERSPECTIVE_HORIZON;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.SHIP;

public class GasPlanetPhysicsListener implements SectorUpdateListener {
    private static HashSet<Vector3i> ggSectorCache = new HashSet<>();

    public static void addGasGiantLocation(Vector3i loc){
        ggSectorCache.add(new Vector3i(loc));
    }

    @Override
    public void local_preUpdate(Sector sector, Timer timer) {

    }

    void applyGravity(StateInterface state, int sectorId, Timer timer){
        List<SegmentController> validSegmentControllers = new LinkedList<>();

        synchronized (state.getLocalAndRemoteObjectContainer().getLocalObjects()) {
            SegmentController entity;
            for (Sendable s : state.getLocalAndRemoteObjectContainer().getLocalObjects().values()) {
                if (s instanceof SegmentController && ((SegmentController) s).getSectorId() == sectorId) {
                    entity = (SegmentController) s;
                    if(entity.getType() == SHIP && !entity.railController.isDocked()) validSegmentControllers.add(entity);
                }
            }
        }

        Vector3i galaxyPos;
        Vector3i tmp3i;
        Vector3f tmp3f;
        SystemSheet sysinfo;
        for(SegmentController entity : validSegmentControllers){
            try {
                galaxyPos = new Vector3i();
                tmp3i = new Vector3i(); //just exists for the various methods that insist on mutating a Vector3i and THEN returning it for some reason, so we don't create pointless Vector3i's every cycle
                tmp3f = new Vector3f(); //ditto
                sysinfo = null;

                Vector3i systemPos = entity.getSystem(new Vector3i());
                Vector3i sectorPos = entity.getSector(new Vector3i());
                if(systemPos == null){
                    if(DEBUG_LOGGING) {
                        System.err.println("[MOD][RRS] No available location for the entity we're trying to accelerate");
                        System.err.println("[MOD][RRS] Entity: " + entity);
                    }
                    continue;
                }
                Galaxy.getContainingGalaxyFromSystemPos(systemPos, galaxyPos);
                if(galaxiesDoneGenerating.contains(galaxyPos)) try {
                    sysinfo = getSystemSheet(galaxyPos, systemPos);
                }
                catch(Exception ignore){
                    //sysinfo = null;
                }
                if(sysinfo == null) continue; //guess there's nothing here (yet)

                SpaceGridMap<GasGiantSheet> giants = sysinfo.getGiants(systemPos);
                if (entity.getMass() > 0 &&
                        entity.getPhysicsDataContainer().getObject() != null &&
                        entity.getSectorId() == sectorId &&
                        giants != null &&
                        giants.keySet().contains(sectorPos)
                ) {
                    GasGiantSheet giant = giants.get(entity.getSector(tmp3i));
                    RigidBody body = (RigidBody) entity.getPhysicsDataContainer().getObject();
                    Transform transform = new Transform();
                    entity.getWorldTransformCenterOfMass(transform);
                    Vector3f force = new Vector3f(transform.origin);
                    Vector3f localPosOriginal = new Vector3f(transform.origin); //same initial value, different uses
                    force.normalize();

                    float r_e = localPosOriginal.length();
                    float r_gc = GIANT_LOWER_SURFACE * BASE_GIANT_SIZE * GameClientState.instance.getSectorSize() * giant.size; //radius of giant cloud tops
                    float r_gk = (1+NEAR_FORCED_PERSPECTIVE_HORIZON) * BASE_GIANT_SIZE * GameClientState.instance.getSectorSize() * giant.size; //radius of giant 'Kármán line' where atmosphere begins to phase in
                    float m = entity.getMassWithDocks();
                    if(r_e > r_gc) {
                        //GMm/(r*r)
                        force.scale(-3F * m);
                        force.scale(GIANT_GRAVITY_FACTOR*giant.size);
                        float gravityMultiplier = 1.0f;
                        gravityMultiplier = entity.getConfigManager().apply(StatusEffectType.THRUSTER_ANTI_GRAVITY, gravityMultiplier);
                        force.scale(gravityMultiplier);
                    }


                    if(r_e < r_gk){
                        float altitude = (r_e-r_gc)/(r_gk-r_gc); //height above "surface" radius
                        float boundedAltitude = min(max(altitude,0),1);
                        Vector3f velocity = body.getLinearVelocity(new Vector3f());
                        Vector3f universalDrag = new Vector3f(velocity); //Air drag on ships regardless of orientation/etc
                        universalDrag.scale(m * -DRAG_FORCE_MULTIPLIER * (1 - boundedAltitude));
                        force.add(universalDrag);

                        //TODO: aero drag (and lift) dependent on AoA. Remove or reduce universal drag, or set to only exist above a certain speed.
                        //Vector3f orientation = body.getWorldTransform(new Transform()).origin;
                        //float angleOfAttack = org.schema.common.util.linAlg.Vector3fTools.getFullRange2DAngleFast(velocity, orientation); //should be equivalent to a "transform.forward"
                        //lift = trailing vertical axis * (1 - diff. from 45 deg) * depth * area of presented bottom face of boxdim or -area of presented top face of boxdim
                        //aero drag = planetwise up or down depending on positive or negative pitch * diff. from 0 deg * depth * area of boxdim presented cross section in direction of travel

                        if(r_e < r_gc-1f){
                            Vector3f repulsion = transform.origin;
                            repulsion.normalize();
                            repulsion.scale(FLOOR_REPEL_FORCE_FACTOR * m * min(abs(altitude),1)); //'abs' is weird here and should not be necessary but with so much weird behaviour, I'm covering all bases.
                            force.add(repulsion);
                            //reject people from going too far into the planet
                        }
                    }

                    body.applyCentralForce(force);
                    float e_v2 = body.getLinearVelocity(tmp3f).lengthSquared();
                    if(e_v2 > entity.getMaxServerSpeed() * entity.getMaxServerSpeed() * 2.25){
                        System.err.print(""); //no-op
                        Vector3f newVel = body.getAngularVelocity(tmp3f);
                        newVel.normalize();
                        newVel.scale(entity.getMaxServerSpeed() * 1.5f);
                        body.setAngularVelocity(body.getAngularVelocity(tmp3f));
                        //*why* should we ever have to do this
                        //System.err.println("[MOD][RRS] WARNING: Excessive speed detected after gas giant force application.");

                        //TO-DO: Set zero speed, if all else fails.
                        // .
                        // ***This cannot be allowed to stop us.***
                    };

                    //TODO: support gas-giant offset from center if we bother with using that

                    //if (r_e < 30) { //TODO: (kill radius as fraction of GG radius) squared
                    //  ((SegmentController) s).startCoreOverheating(null); //caused an NPC faction nullpointer somehow by killing an NPC faction mob...
                    //}
                }
            } catch (Exception ignore) {
//                        ignore.printStackTrace();
                /*Likely just means this thing isn't somewhere with a corresponding remote sector*/
            }
        }
    }
    @Override
    public void local_activeUpdate(Sector sector, Timer timer) {
        int sectorId = sector.getSectorId();
        //TODO: CACHE GG SECTORS in IntOpenHashSet and check against cache before doing all this other bs. if(!ggCache.contains(sectorId)) return;
        if(GameCommon.isClientConnectedToServer() || GameCommon.isOnSinglePlayer()){
            GameClientState clientState = GameClientState.instance;
            if(getCurrentRemoteSector(clientState) != null &&
                    (getCurrentSectorId(clientState) == sectorId || isNeighborToClientSector(sectorId,clientState)))
                if(ggSectorCache.contains(sector.pos))
                    applyGravity(sector.getState(), sector.getSectorId(), timer);//TODO: see if desynch avoided somehow - should resolve on load but technically some synch shenanigans could happen
        }
        //TODO: check gg's by sectorid before checking for segcons (register gg sector indices in a hashset somewhere), also minimize synchronized time by building a list of relevant segcons
    }

    @Override
    public void local_postUpdate(Sector sector, Timer timer) {

    }

    @Override
    public void remote_preUpdate(RemoteSector remoteSector, Timer timer) {

    }

    @Override
    public void remote_postUpdate(RemoteSector sector, Timer timer) {
        int sectorId = sector.getId();
        //TODO: CACHE GG SECTORS in IntOpenHashSet and check against cache before doing all this other bs. if(!ggCache.contains(sectorId)) return;
        if(GameCommon.isClientConnectedToServer() || GameCommon.isOnSinglePlayer()){
            GameClientState clientState = GameClientState.instance;
            if(getCurrentRemoteSector(clientState) != null &&
                    (getCurrentSectorId(clientState) == sectorId || isNeighborToClientSector(sectorId,clientState)))
                if(ggSectorCache.contains(sector.clientPos()))
                    applyGravity(sector.getState(), sector.getId(), timer);//TODO: see if desynch avoided somehow - should resolve on load but technically some synch shenanigans could happen
        }
        //TODO: check gg's by sectorid before checking for segcons (register gg sector indices in a hashset somewhere), also minimize synchronized time by building a list of relevant segcons
    }

    public boolean isNeighborToClientSector(int sec, GameClientState cs) {
        NetworkStateContainer netStateBox = cs.getLocalAndRemoteObjectContainer();
        RemoteSector objSector = (RemoteSector) netStateBox.getLocalObjects().get(sec);
        return objSector != null &&
                getCurrentRemoteSector(cs) != null &&
                Sector.isNeighbor(objSector.clientPos(),
                        getCurrentRemoteSector(cs).clientPos());
    }

    private RemoteSector getCurrentRemoteSector(GameClientState cs) {
        Sendable sendable = cs.getLocalAndRemoteObjectContainer().getLocalObjects().get(getCurrentSectorId(cs));
        if (sendable != null && sendable instanceof RemoteSector) {
            return (RemoteSector) sendable;
        }
        return null;
    }

    private int getCurrentSectorId(GameClientState cs) {
        if (cs.getPlayer() == null) {
            assert (cs.getInitialSectorId() != Sector.SECTOR_INITIAL);
            return cs.getInitialSectorId();
        }
        assert (cs.getPlayer().getCurrentSectorId() != Sector.SECTOR_INITIAL);
        return cs.getPlayer().getCurrentSectorId();
    }

    //why the hell did I have to make this stuff client-authoritative anyway? This game's physics handling is really something special.
}
