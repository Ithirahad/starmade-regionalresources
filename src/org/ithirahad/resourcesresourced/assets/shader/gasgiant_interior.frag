#version 120

uniform float time;
uniform float ambientLevel;
uniform float depth;
uniform sampler2D lavaTex;
uniform vec4 color1;
uniform vec4 color2;
uniform vec4 viewPos;

varying vec4 col;
varying vec3 normal;
varying vec3 vert;
varying vec3 vpos;
varying vec3 view;

/*
float brightness(in vec4 color){
    return (color.r+color.g+color.b)/3.0; //very visually inaccurate as a brightness function, but does not matter because we are expecting greyscale input
}
*/

void main()
{
    /*
    ///UNLIT COLOUR

    //mostly duplicated from lava/planet core shader
    float mx = 0.25;
    float my = 0.25;

    mx += sin(time + cos(gl_TexCoord[0].y*1.0)*0.1)/166.0;
    my += cos(time + sin(gl_TexCoord[0].x*1.0)*0.1)/166.0;

    float dx = mx;
    float dy = my;

    vec4 tex = texture2D(lavaTex,gl_TexCoord[0].xy*7.0);
    vec4 distort = texture2D(lavaTex,(gl_TexCoord[0].xy * 0.2) + vec2(dx,dy));

    dx += time/60.0;
    dy -= time/60.0;

    vec4 distort2 = texture2D(lavaTex,(gl_TexCoord[0].xy * 0.8) + vec2(dx,dy));

    float omx = sin(time/80.0 + (distort.r + distort2.g)/5.0)/2.0;
    float omy = cos(time/80.0 + (distort.g + distort2.r)/5.0)/2.0;

    vec4 tex2 = texture2D(lavaTex,(gl_TexCoord[0].xy * 2.0) + vec2(mx+omx,my+omy));

    gl_FragColor = mix(tex2 * col,tex,0.1);

    gl_FragColor.r = mix(gl_FragColor.r,1.0,1.0 - tex.a);
    gl_FragColor.g = mix(gl_FragColor.g,1.0,1.0 - tex.a);
    gl_FragColor.b = mix(gl_FragColor.b,1.0,1.0 - tex.a);
    //gl_FragColor.a = mix(gl_FragColor.a,1.0,1.0 - tex.a);

    gl_FragColor.rgb -= mx*0.6 *(distort.r+distort.g)*0.5;
    gl_FragColor.rgb -= my*0.6 *(distort.r+distort.b)*0.5;
    //gl_FragColor.rgb += pow(gl_FragColor.r, 1.1);
    gl_FragColor.a = tex.a;

    gl_FragColor *= col;

    //gl_FragColor.r += 1.0 - tex.a*1.2;
    //gl_FragColor.g += 1.0 - tex.a*1.2;
    //gl_FragColor.b += 1.0 - tex.a*1.2;
    gl_FragColor.r = mix(color1.r,color2.r,brightness(gl_FragColor));
    gl_FragColor.g = mix(color1.g,color2.g,brightness(gl_FragColor));
    gl_FragColor.b = mix(color1.b,color2.b,brightness(gl_FragColor));
    gl_FragColor = mix(gl_FragColor, color2, 0.75f); //fade the texture down to mostly the bright color on the inside
    ///

    vec3 light = gl_LightSource[0].position.xyz;
    vec3 lightAngle = normalize(light-vert);
    vec3 n_normal = normalize(normal);
    float diffuseFactor = (max(dot(lightAngle, n_normal), 0.0)/(1-ambientLevel)) + ambientLevel;

    vec3 VertexToEye = normalize(viewPos.xyz - vpos); //world-space view minus vert position //TODO: probably will have to invert a bunch of stuff for being inside a ball
    vec3 LightReflect = normalize(reflect(lightAngle, n_normal));
    float specularFactor = dot(VertexToEye, LightReflect);
    vec4 specularColor;
    if (specularFactor > 0) {
        specularColor = vec4(specularFactor, specularFactor, specularFactor, 1.0f);
    }

    //TODO: probably invert light position
    //TODO: specular sun (bright color -> white)
    gl_FragColor *= diffuseFactor;
    gl_FragColor.xyz *= specularColor.xyz;
    */

    vec3 light = gl_LightSource[0].position.xyz;
    vec3 lightAngle = normalize(light-vert);
    vec3 n_normal = normalize(-normal);
    float diffuseFactor = (1-(max(dot(lightAngle, n_normal), 0.0)/(1-ambientLevel))) + ambientLevel; //double ambient at max depth, idk. Would probably have to multiply that last term by day/night coefficient
    //1- because this is intended for use on inside-out surfaces
    diffuseFactor = mix(diffuseFactor,ambientLevel,depth); //the deeper we go, the less the diffuse lighting should apply.
    //TODO: switch the mix order if necessary?

    gl_FragColor = (color2 + (0.25*color1)) / 1.25;
    gl_FragColor.rgb -= gl_FragColor.rgb * 0.2 * depth; //for some reason it washes out to pure white at a certain point; this is to try and prevent that
    gl_FragColor.rgb *= diffuseFactor;
    //gl_FragColor *= diffuseFactor;
    gl_FragColor.a = min(1,2*depth); //max opacity at half depth to floor
}
