#version 120

uniform float time;

varying vec4 col;
varying vec3 normal;
varying vec3 vert;
varying vec3 vpos;


void main()
{
	vec4 vpos = gl_Vertex;
	//vec3 nm = gl_NormalMatrix * gl_Normal;
	
	float cgt = time * -210.0;
	float tsin = sin((cgt + vpos.x*150.0)/100.0)*1.0;
	float tcos = cos((cgt + vpos.y*150.0)/100.0)*1.0;
	
	gl_Position = (gl_ModelViewProjectionMatrix * vpos);
	
	col = vec4(1.0,1.0,1.0,1.0);

	vert = vec3(gl_ModelViewMatrix * gl_Vertex);

	normal = normalize(gl_NormalMatrix * gl_Vertex.xyz); //This is incredibly janky, but I mean, it's a ball... //TODO: Why did I have to do this?
	//normal = normalize(gl_NormalMatrix * gl_Normal); //was giving bad normals w/ light and darkness on wrong sides of faces
	
	gl_TexCoord[0] = gl_MultiTexCoord0;
}