package org.ithirahad.resourcesresourced.events;

import api.listener.events.Event;
import org.ithirahad.resourcesresourced.RRUtils.SpaceGridMap;
import org.ithirahad.resourcesresourced.universe.galaxy.ResourceZone;
import org.schema.game.server.data.Galaxy;

public class RRSGalaxyGenCompleteEvent extends Event {
    private final Galaxy galaxy;
    private final SpaceGridMap<ResourceZone> zones;

    public RRSGalaxyGenCompleteEvent(Galaxy g, SpaceGridMap<ResourceZone> zones) {
        this.zones = zones;
        this.galaxy = g;
    }

    public Galaxy getGalaxy(){
        return galaxy;
    }

    public SpaceGridMap<ResourceZone> getZoneLayout(){
        return zones;
    }
}
