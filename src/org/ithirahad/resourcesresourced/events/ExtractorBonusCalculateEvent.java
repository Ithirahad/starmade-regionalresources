package org.ithirahad.resourcesresourced.events;

import api.listener.events.Event;
import org.ithirahad.resourcesresourced.industry.ResourceSourceType;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.ServerConfig;

public class ExtractorBonusCalculateEvent extends Event {
    private final String entityUID;
    private final int faction;
    private final Vector3i sector;
    private final short resourceType;
    private final ResourceSourceType rst;
    private int bonus;

    public ExtractorBonusCalculateEvent(String uid, int factionId, Vector3i sector, short resourceType, ResourceSourceType sourceType, int bonus) {
        this.entityUID = uid;
        this.faction = factionId;
        this.sector = sector;
        this.resourceType = resourceType;
        this.rst = sourceType;
        this.bonus = bonus;
    }

    public String getExtractingEntityUID(){
        return entityUID;
    }

    public int getGlobalBonus(){
        return (Integer) ServerConfig.MINING_BONUS.getCurrentState();
    }

    public int getFactionID(){
        return faction;
    }

    public Vector3i getSector() {
        return sector;
    }

    public short getResourceType() {
        return resourceType;
    }

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }
}
