package org.ithirahad.resourcesresourced.events;

import api.listener.events.Event;
import org.schema.game.common.controller.SegmentController;

public class HarvesterStrengthUpdateEvent extends Event {
    private final float factoryEnhanceLevel;
    private final float configuredBaseRate;
    private final SegmentController entity;
    private float power;

    public HarvesterStrengthUpdateEvent(int factoryCapability, float configuredBaseRate, SegmentController entity) {
        factoryEnhanceLevel = factoryCapability;
        this.configuredBaseRate = configuredBaseRate;
        this.entity = entity;
        power = factoryEnhanceLevel * this.configuredBaseRate; //normal power calc - maybe shouldn't be in an event
    }

    public float getFactoryEnhancedScale() {
        return factoryEnhanceLevel;
    }

    public float getConfigStrengthMultiplier(){
        return configuredBaseRate;
    }

    public void setPower(float val){
        power = val;
    }

    public float getPower(){
        return power;
    }

    public SegmentController getExtractingEntity() {
        return entity;
    }
}
