package org.ithirahad.resourcesresourced.events;

import api.listener.events.Event;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;

public class MacetineRefineListCompileEvent extends Event{
    private final ShortArrayList list;
    public MacetineRefineListCompileEvent(ShortArrayList v) {
        list = v;
    }

    public ShortArrayList getRefineList() {
        return list;
    }
}
